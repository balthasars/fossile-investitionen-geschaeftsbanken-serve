---
site: bookdown::bookdown_site
output:
   html_document: default
   pdf_document: default
editor_options: 
  chunk_output_type: console
---

# 13F-Filings mit Klimadaten

## Datenverfügbarkeit

Hier werden die Firmenindustrien- und CO~2~-Daten hinzugefügt.

Von den Portfolios nur American Depository Receipts und Ordinary Shares, ohne ETF. Für die ETFs hat Eikon keine aussagekräftigen GICS-Daten.







**Wertpapierarten, die in die Analyse eingeflossen sind:**

* American Depository Receipt
* Depository Receipt
* Convertible Preference Share
* Fully Paid Ordinary Share
* Cumulative Preference Share
* Global Depository Receipt
* Depository Share
* Ordinary Share
* Preferred Share
* Unit

Wie viele, die keine asset_category_description haben, haben aber eine ISIN?


```
## [1] FALSE  TRUE  TRUE  TRUE  TRUE
```


```{=html}
<div id="jsyknmdgjq" style="overflow-x:auto;overflow-y:auto;width:auto;height:auto;">
<style>html {
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Helvetica Neue', 'Fira Sans', 'Droid Sans', Arial, sans-serif;
}

#jsyknmdgjq .gt_table {
  display: table;
  border-collapse: collapse;
  margin-left: auto;
  margin-right: auto;
  color: #333333;
  font-size: 16px;
  font-weight: normal;
  font-style: normal;
  background-color: #FFFFFF;
  width: auto;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #A8A8A8;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #A8A8A8;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
}

#jsyknmdgjq .gt_heading {
  background-color: #FFFFFF;
  text-align: center;
  border-bottom-color: #FFFFFF;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#jsyknmdgjq .gt_title {
  color: #333333;
  font-size: 125%;
  font-weight: initial;
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 5px;
  padding-right: 5px;
  border-bottom-color: #FFFFFF;
  border-bottom-width: 0;
}

#jsyknmdgjq .gt_subtitle {
  color: #333333;
  font-size: 85%;
  font-weight: initial;
  padding-top: 0;
  padding-bottom: 6px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-color: #FFFFFF;
  border-top-width: 0;
}

#jsyknmdgjq .gt_bottom_border {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#jsyknmdgjq .gt_col_headings {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#jsyknmdgjq .gt_col_heading {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 6px;
  padding-left: 5px;
  padding-right: 5px;
  overflow-x: hidden;
}

#jsyknmdgjq .gt_column_spanner_outer {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  padding-top: 0;
  padding-bottom: 0;
  padding-left: 4px;
  padding-right: 4px;
}

#jsyknmdgjq .gt_column_spanner_outer:first-child {
  padding-left: 0;
}

#jsyknmdgjq .gt_column_spanner_outer:last-child {
  padding-right: 0;
}

#jsyknmdgjq .gt_column_spanner {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 5px;
  overflow-x: hidden;
  display: inline-block;
  width: 100%;
}

#jsyknmdgjq .gt_group_heading {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
}

#jsyknmdgjq .gt_empty_group_heading {
  padding: 0.5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: middle;
}

#jsyknmdgjq .gt_from_md > :first-child {
  margin-top: 0;
}

#jsyknmdgjq .gt_from_md > :last-child {
  margin-bottom: 0;
}

#jsyknmdgjq .gt_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  margin: 10px;
  border-top-style: solid;
  border-top-width: 1px;
  border-top-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
  overflow-x: hidden;
}

#jsyknmdgjq .gt_stub {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 5px;
  padding-right: 5px;
}

#jsyknmdgjq .gt_stub_row_group {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 5px;
  padding-right: 5px;
  vertical-align: top;
}

#jsyknmdgjq .gt_row_group_first td {
  border-top-width: 2px;
}

#jsyknmdgjq .gt_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#jsyknmdgjq .gt_first_summary_row {
  border-top-style: solid;
  border-top-color: #D3D3D3;
}

#jsyknmdgjq .gt_first_summary_row.thick {
  border-top-width: 2px;
}

#jsyknmdgjq .gt_last_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#jsyknmdgjq .gt_grand_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#jsyknmdgjq .gt_first_grand_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-style: double;
  border-top-width: 6px;
  border-top-color: #D3D3D3;
}

#jsyknmdgjq .gt_striped {
  background-color: rgba(128, 128, 128, 0.05);
}

#jsyknmdgjq .gt_table_body {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#jsyknmdgjq .gt_footnotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#jsyknmdgjq .gt_footnote {
  margin: 0px;
  font-size: 90%;
  padding-left: 4px;
  padding-right: 4px;
  padding-left: 5px;
  padding-right: 5px;
}

#jsyknmdgjq .gt_sourcenotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#jsyknmdgjq .gt_sourcenote {
  font-size: 90%;
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 5px;
  padding-right: 5px;
}

#jsyknmdgjq .gt_left {
  text-align: left;
}

#jsyknmdgjq .gt_center {
  text-align: center;
}

#jsyknmdgjq .gt_right {
  text-align: right;
  font-variant-numeric: tabular-nums;
}

#jsyknmdgjq .gt_font_normal {
  font-weight: normal;
}

#jsyknmdgjq .gt_font_bold {
  font-weight: bold;
}

#jsyknmdgjq .gt_font_italic {
  font-style: italic;
}

#jsyknmdgjq .gt_super {
  font-size: 65%;
}

#jsyknmdgjq .gt_footnote_marks {
  font-style: italic;
  font-weight: normal;
  font-size: 75%;
  vertical-align: 0.4em;
}

#jsyknmdgjq .gt_asterisk {
  font-size: 100%;
  vertical-align: 0;
}

#jsyknmdgjq .gt_slash_mark {
  font-size: 0.7em;
  line-height: 0.7em;
  vertical-align: 0.15em;
}

#jsyknmdgjq .gt_fraction_numerator {
  font-size: 0.6em;
  line-height: 0.6em;
  vertical-align: 0.45em;
}

#jsyknmdgjq .gt_fraction_denominator {
  font-size: 0.6em;
  line-height: 0.6em;
  vertical-align: -0.05em;
}
</style>
<table class="gt_table">
  <caption>(#tab:unnamed-chunk-5)Verfügbarkeit `asset_category_description`, in Proz. der Wertpapiere, nach Ergänzungen durch OpenFIGI und Regex</caption>
  
  <thead class="gt_col_headings">
    <tr>
      <th class="gt_col_heading gt_columns_bottom_border gt_center" rowspan="1" colspan="1">not_available</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1">n</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1">perc</th>
    </tr>
  </thead>
  <tbody class="gt_table_body">
    <tr><td class="gt_row gt_center">FALSE</td>
<td class="gt_row gt_right">16859</td>
<td class="gt_row gt_right">99.1297701</td></tr>
    <tr><td class="gt_row gt_center">TRUE</td>
<td class="gt_row gt_right">148</td>
<td class="gt_row gt_right">0.8702299</td></tr>
  </tbody>
  
  
</table>
</div>
```










<!-- Wie viele Wertpapiere mit CO2-Emissionen, aber keinen Angaben zur Asset Category? -->

<!-- ```{r} -->
<!-- filings_pre_filter %>% -->
<!--   distinct(tot_co2_by_revenue, asset_category_description, company_common_name) %>% -->
<!--   count(co2_intensity_available = !is.na(tot_co2_by_revenue), no_asset_cat = is.na(asset_category_description)) %>%  -->
<!--   gt(caption = "") -->
<!-- ``` -->

Wert der Aktien nach Bank:

<div class="figure">
<p class="caption">(\#fig:unnamed-chunk-10)Summe der analysierten Wertpapiere nach Bank, Q2/2021</p><img src="03-filings-mit-meta-ts_files/figure-html/unnamed-chunk-10-1.png" alt="Summe der analysierten Wertpapiere nach Bank, Q2/2021" width="672" /></div>

<!-- Anteil verfügbar für Analyse (CO2-Daten): -->

<!-- ```{r, fig.cap="Summe der analysierten Wertpapiere nach Bank, nach Verfügbarkeit der Emissionsdaten, Q2/2021"} -->
<!-- filings_pre_filter %>% -->
<!--   filter(asset_category_description %in% included_assset_categories) %>% -->
<!--   filter(period_of_report == "2021-06-30") %>% -->
<!--   mutate(co2_intensity_available = !is.na(tot_co2_by_revenue)) %>% -->
<!--   group_by(bank_name, co2_intensity_available) %>% -->
<!--   summarize( -->
<!--     value_tot = sum(value_usd_thousands, na.rm = TRUE) -->
<!--   ) %>% -->
<!--   ungroup() %>% -->
<!--   group_by(bank_name) %>% -->
<!--   mutate(perc = (value_tot / sum(value_tot, na.rm = TRUE)) * 100) %>% -->
<!--   ggplot(aes(forcats::fct_reorder(bank_name, -perc), perc, fill = co2_intensity_available)) + -->
<!--   geom_col(position = "stack") + -->
<!--   # geom_text(aes(y = (value_tot * 10.6), label = paste(round(value_tot * 1e-6, 1), "Mia. USD"))) + -->
<!--   coord_flip() + -->
<!--   scale_y_continuous( -->
<!--     expand = c(0, 1), -->
<!--     # trans = "sqrt", -->
<!--     label = scales::label_comma(big.mark = "'") -->
<!--     # breaks = c(1e6, 5e6, 10e6, 15e6, 50e6, 100e6, 200e6, 500e6) -->
<!--   ) + -->
<!--   labs( -->
<!--     title = "Summen der offengelegten Werte nach Bank, nach Datenkorrektur", -->
<!--     subtitle = "Q2/2021, Summe Spalte 'value'", -->
<!--     y = "Verfügbarkeit der Daten", x = "", -->
<!--     caption = "X-Achse Log10-transformiert" -->
<!--   ) + -->
<!--   theme(plot.title.position = "plot", legend.position = "top") -->
<!-- ``` -->


```{=html}
<div id="olmysuktgz" style="overflow-x:auto;overflow-y:auto;width:auto;height:auto;">
<style>html {
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Helvetica Neue', 'Fira Sans', 'Droid Sans', Arial, sans-serif;
}

#olmysuktgz .gt_table {
  display: table;
  border-collapse: collapse;
  margin-left: auto;
  margin-right: auto;
  color: #333333;
  font-size: 16px;
  font-weight: normal;
  font-style: normal;
  background-color: #FFFFFF;
  width: auto;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #A8A8A8;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #A8A8A8;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
}

#olmysuktgz .gt_heading {
  background-color: #FFFFFF;
  text-align: center;
  border-bottom-color: #FFFFFF;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#olmysuktgz .gt_title {
  color: #333333;
  font-size: 125%;
  font-weight: initial;
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 5px;
  padding-right: 5px;
  border-bottom-color: #FFFFFF;
  border-bottom-width: 0;
}

#olmysuktgz .gt_subtitle {
  color: #333333;
  font-size: 85%;
  font-weight: initial;
  padding-top: 0;
  padding-bottom: 6px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-color: #FFFFFF;
  border-top-width: 0;
}

#olmysuktgz .gt_bottom_border {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#olmysuktgz .gt_col_headings {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#olmysuktgz .gt_col_heading {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 6px;
  padding-left: 5px;
  padding-right: 5px;
  overflow-x: hidden;
}

#olmysuktgz .gt_column_spanner_outer {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  padding-top: 0;
  padding-bottom: 0;
  padding-left: 4px;
  padding-right: 4px;
}

#olmysuktgz .gt_column_spanner_outer:first-child {
  padding-left: 0;
}

#olmysuktgz .gt_column_spanner_outer:last-child {
  padding-right: 0;
}

#olmysuktgz .gt_column_spanner {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 5px;
  overflow-x: hidden;
  display: inline-block;
  width: 100%;
}

#olmysuktgz .gt_group_heading {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
}

#olmysuktgz .gt_empty_group_heading {
  padding: 0.5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: middle;
}

#olmysuktgz .gt_from_md > :first-child {
  margin-top: 0;
}

#olmysuktgz .gt_from_md > :last-child {
  margin-bottom: 0;
}

#olmysuktgz .gt_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  margin: 10px;
  border-top-style: solid;
  border-top-width: 1px;
  border-top-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
  overflow-x: hidden;
}

#olmysuktgz .gt_stub {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 5px;
  padding-right: 5px;
}

#olmysuktgz .gt_stub_row_group {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 5px;
  padding-right: 5px;
  vertical-align: top;
}

#olmysuktgz .gt_row_group_first td {
  border-top-width: 2px;
}

#olmysuktgz .gt_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#olmysuktgz .gt_first_summary_row {
  border-top-style: solid;
  border-top-color: #D3D3D3;
}

#olmysuktgz .gt_first_summary_row.thick {
  border-top-width: 2px;
}

#olmysuktgz .gt_last_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#olmysuktgz .gt_grand_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#olmysuktgz .gt_first_grand_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-style: double;
  border-top-width: 6px;
  border-top-color: #D3D3D3;
}

#olmysuktgz .gt_striped {
  background-color: rgba(128, 128, 128, 0.05);
}

#olmysuktgz .gt_table_body {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#olmysuktgz .gt_footnotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#olmysuktgz .gt_footnote {
  margin: 0px;
  font-size: 90%;
  padding-left: 4px;
  padding-right: 4px;
  padding-left: 5px;
  padding-right: 5px;
}

#olmysuktgz .gt_sourcenotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#olmysuktgz .gt_sourcenote {
  font-size: 90%;
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 5px;
  padding-right: 5px;
}

#olmysuktgz .gt_left {
  text-align: left;
}

#olmysuktgz .gt_center {
  text-align: center;
}

#olmysuktgz .gt_right {
  text-align: right;
  font-variant-numeric: tabular-nums;
}

#olmysuktgz .gt_font_normal {
  font-weight: normal;
}

#olmysuktgz .gt_font_bold {
  font-weight: bold;
}

#olmysuktgz .gt_font_italic {
  font-style: italic;
}

#olmysuktgz .gt_super {
  font-size: 65%;
}

#olmysuktgz .gt_footnote_marks {
  font-style: italic;
  font-weight: normal;
  font-size: 75%;
  vertical-align: 0.4em;
}

#olmysuktgz .gt_asterisk {
  font-size: 100%;
  vertical-align: 0;
}

#olmysuktgz .gt_slash_mark {
  font-size: 0.7em;
  line-height: 0.7em;
  vertical-align: 0.15em;
}

#olmysuktgz .gt_fraction_numerator {
  font-size: 0.6em;
  line-height: 0.6em;
  vertical-align: 0.45em;
}

#olmysuktgz .gt_fraction_denominator {
  font-size: 0.6em;
  line-height: 0.6em;
  vertical-align: -0.05em;
}
</style>
<table class="gt_table">
  <caption>(#tab:unnamed-chunk-11)Verfügbarkeit CO2-Intensitätsdaten, nach Bank, Q2/2021</caption>
  
  <thead class="gt_col_headings">
    <tr>
      <th class="gt_col_heading gt_columns_bottom_border gt_left" rowspan="1" colspan="1">Bank</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_center" rowspan="1" colspan="1"><span style='color:blue'><b>Nicht Verfügbar</b></span>||<span style='color:black'><b>Verfügbar</b></span></th>
    </tr>
  </thead>
  <tbody class="gt_table_body">
    <tr><td class="gt_row gt_left">BCV</td>
<td class="gt_row gt_center"><?xml version='1.0' encoding='UTF-8' ?><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' class='svglite' width='198.43pt' height='14.17pt' viewBox='0 0 198.43 14.17'><defs>  <style type='text/css'><![CDATA[    .svglite line, .svglite polyline, .svglite polygon, .svglite path, .svglite rect, .svglite circle {      fill: none;      stroke: #000000;      stroke-linecap: round;      stroke-linejoin: round;      stroke-miterlimit: 10.00;    }    .svglite text {      white-space: pre;    }  ]]></style></defs><rect width='100%' height='100%' style='stroke: none; fill: none;'/><defs>  <clipPath id='cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw=='>    <rect x='0.00' y='0.00' width='198.43' height='14.17' />  </clipPath></defs><g clip-path='url(#cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw==)'><rect x='0.00' y='0.0000000000000018' width='27.02' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #0000FF;' /><rect x='27.02' y='0.0000000000000018' width='153.36' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #000000;' /><text x='13.51' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='15.36px' lengthAdjust='spacingAndGlyphs'>15%</text><text x='103.71' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='15.36px' lengthAdjust='spacingAndGlyphs'>85%</text></g></svg></td></tr>
    <tr><td class="gt_row gt_left">Credit Suisse</td>
<td class="gt_row gt_center"><?xml version='1.0' encoding='UTF-8' ?><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' class='svglite' width='198.43pt' height='14.17pt' viewBox='0 0 198.43 14.17'><defs>  <style type='text/css'><![CDATA[    .svglite line, .svglite polyline, .svglite polygon, .svglite path, .svglite rect, .svglite circle {      fill: none;      stroke: #000000;      stroke-linecap: round;      stroke-linejoin: round;      stroke-miterlimit: 10.00;    }    .svglite text {      white-space: pre;    }  ]]></style></defs><rect width='100%' height='100%' style='stroke: none; fill: none;'/><defs>  <clipPath id='cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw=='>    <rect x='0.00' y='0.00' width='198.43' height='14.17' />  </clipPath></defs><g clip-path='url(#cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw==)'><rect x='0.00' y='0.0000000000000018' width='15.60' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #0000FF;' /><rect x='15.60' y='0.0000000000000018' width='164.79' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #000000;' /><text x='7.80' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='10.24px' lengthAdjust='spacingAndGlyphs'>9%</text><text x='97.99' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='15.36px' lengthAdjust='spacingAndGlyphs'>91%</text></g></svg></td></tr>
    <tr><td class="gt_row gt_left">Edmond de Rothschild</td>
<td class="gt_row gt_center"><?xml version='1.0' encoding='UTF-8' ?><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' class='svglite' width='198.43pt' height='14.17pt' viewBox='0 0 198.43 14.17'><defs>  <style type='text/css'><![CDATA[    .svglite line, .svglite polyline, .svglite polygon, .svglite path, .svglite rect, .svglite circle {      fill: none;      stroke: #000000;      stroke-linecap: round;      stroke-linejoin: round;      stroke-miterlimit: 10.00;    }    .svglite text {      white-space: pre;    }  ]]></style></defs><rect width='100%' height='100%' style='stroke: none; fill: none;'/><defs>  <clipPath id='cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw=='>    <rect x='0.00' y='0.00' width='198.43' height='14.17' />  </clipPath></defs><g clip-path='url(#cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw==)'><rect x='0.00' y='0.0000000000000018' width='18.34' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #0000FF;' /><rect x='18.34' y='0.0000000000000018' width='162.05' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #000000;' /><text x='9.17' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='15.36px' lengthAdjust='spacingAndGlyphs'>10%</text><text x='99.36' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='15.36px' lengthAdjust='spacingAndGlyphs'>90%</text></g></svg></td></tr>
    <tr><td class="gt_row gt_left">Julius Bär</td>
<td class="gt_row gt_center"><?xml version='1.0' encoding='UTF-8' ?><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' class='svglite' width='198.43pt' height='14.17pt' viewBox='0 0 198.43 14.17'><defs>  <style type='text/css'><![CDATA[    .svglite line, .svglite polyline, .svglite polygon, .svglite path, .svglite rect, .svglite circle {      fill: none;      stroke: #000000;      stroke-linecap: round;      stroke-linejoin: round;      stroke-miterlimit: 10.00;    }    .svglite text {      white-space: pre;    }  ]]></style></defs><rect width='100%' height='100%' style='stroke: none; fill: none;'/><defs>  <clipPath id='cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw=='>    <rect x='0.00' y='0.00' width='198.43' height='14.17' />  </clipPath></defs><g clip-path='url(#cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw==)'><rect x='0.00' y='0.0000000000000018' width='3.68' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #0000FF;' /><rect x='3.68' y='0.0000000000000018' width='176.71' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #000000;' /><text x='1.84' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='10.24px' lengthAdjust='spacingAndGlyphs'>2%</text><text x='92.03' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='15.36px' lengthAdjust='spacingAndGlyphs'>98%</text></g></svg></td></tr>
    <tr><td class="gt_row gt_left">LGT</td>
<td class="gt_row gt_center"><?xml version='1.0' encoding='UTF-8' ?><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' class='svglite' width='198.43pt' height='14.17pt' viewBox='0 0 198.43 14.17'><defs>  <style type='text/css'><![CDATA[    .svglite line, .svglite polyline, .svglite polygon, .svglite path, .svglite rect, .svglite circle {      fill: none;      stroke: #000000;      stroke-linecap: round;      stroke-linejoin: round;      stroke-miterlimit: 10.00;    }    .svglite text {      white-space: pre;    }  ]]></style></defs><rect width='100%' height='100%' style='stroke: none; fill: none;'/><defs>  <clipPath id='cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw=='>    <rect x='0.00' y='0.00' width='198.43' height='14.17' />  </clipPath></defs><g clip-path='url(#cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw==)'><rect x='0.00' y='0.0000000000000018' width='5.46' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #0000FF;' /><rect x='5.46' y='0.0000000000000018' width='174.92' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #000000;' /><text x='2.73' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='10.24px' lengthAdjust='spacingAndGlyphs'>3%</text><text x='92.92' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='15.36px' lengthAdjust='spacingAndGlyphs'>97%</text></g></svg></td></tr>
    <tr><td class="gt_row gt_left">Lombard Odier</td>
<td class="gt_row gt_center"><?xml version='1.0' encoding='UTF-8' ?><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' class='svglite' width='198.43pt' height='14.17pt' viewBox='0 0 198.43 14.17'><defs>  <style type='text/css'><![CDATA[    .svglite line, .svglite polyline, .svglite polygon, .svglite path, .svglite rect, .svglite circle {      fill: none;      stroke: #000000;      stroke-linecap: round;      stroke-linejoin: round;      stroke-miterlimit: 10.00;    }    .svglite text {      white-space: pre;    }  ]]></style></defs><rect width='100%' height='100%' style='stroke: none; fill: none;'/><defs>  <clipPath id='cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw=='>    <rect x='0.00' y='0.00' width='198.43' height='14.17' />  </clipPath></defs><g clip-path='url(#cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw==)'><rect x='0.00' y='0.0000000000000018' width='22.20' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #0000FF;' /><rect x='22.20' y='0.0000000000000018' width='158.19' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #000000;' /><text x='11.10' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='15.36px' lengthAdjust='spacingAndGlyphs'>12%</text><text x='101.29' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='15.36px' lengthAdjust='spacingAndGlyphs'>88%</text></g></svg></td></tr>
    <tr><td class="gt_row gt_left">Pictet</td>
<td class="gt_row gt_center"><?xml version='1.0' encoding='UTF-8' ?><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' class='svglite' width='198.43pt' height='14.17pt' viewBox='0 0 198.43 14.17'><defs>  <style type='text/css'><![CDATA[    .svglite line, .svglite polyline, .svglite polygon, .svglite path, .svglite rect, .svglite circle {      fill: none;      stroke: #000000;      stroke-linecap: round;      stroke-linejoin: round;      stroke-miterlimit: 10.00;    }    .svglite text {      white-space: pre;    }  ]]></style></defs><rect width='100%' height='100%' style='stroke: none; fill: none;'/><defs>  <clipPath id='cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw=='>    <rect x='0.00' y='0.00' width='198.43' height='14.17' />  </clipPath></defs><g clip-path='url(#cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw==)'><rect x='0.00' y='0.0000000000000018' width='18.69' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #0000FF;' /><rect x='18.69' y='0.0000000000000018' width='161.70' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #000000;' /><text x='9.34' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='15.36px' lengthAdjust='spacingAndGlyphs'>10%</text><text x='99.54' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='15.36px' lengthAdjust='spacingAndGlyphs'>90%</text></g></svg></td></tr>
    <tr><td class="gt_row gt_left">UBS</td>
<td class="gt_row gt_center"><?xml version='1.0' encoding='UTF-8' ?><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' class='svglite' width='198.43pt' height='14.17pt' viewBox='0 0 198.43 14.17'><defs>  <style type='text/css'><![CDATA[    .svglite line, .svglite polyline, .svglite polygon, .svglite path, .svglite rect, .svglite circle {      fill: none;      stroke: #000000;      stroke-linecap: round;      stroke-linejoin: round;      stroke-miterlimit: 10.00;    }    .svglite text {      white-space: pre;    }  ]]></style></defs><rect width='100%' height='100%' style='stroke: none; fill: none;'/><defs>  <clipPath id='cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw=='>    <rect x='0.00' y='0.00' width='198.43' height='14.17' />  </clipPath></defs><g clip-path='url(#cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw==)'><rect x='0.00' y='0.0000000000000018' width='12.57' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #0000FF;' /><rect x='12.57' y='0.0000000000000018' width='167.82' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #000000;' /><text x='6.28' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='10.24px' lengthAdjust='spacingAndGlyphs'>7%</text><text x='96.48' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='15.36px' lengthAdjust='spacingAndGlyphs'>93%</text></g></svg></td></tr>
    <tr><td class="gt_row gt_left">Union Bancaire Privée</td>
<td class="gt_row gt_center"><?xml version='1.0' encoding='UTF-8' ?><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' class='svglite' width='198.43pt' height='14.17pt' viewBox='0 0 198.43 14.17'><defs>  <style type='text/css'><![CDATA[    .svglite line, .svglite polyline, .svglite polygon, .svglite path, .svglite rect, .svglite circle {      fill: none;      stroke: #000000;      stroke-linecap: round;      stroke-linejoin: round;      stroke-miterlimit: 10.00;    }    .svglite text {      white-space: pre;    }  ]]></style></defs><rect width='100%' height='100%' style='stroke: none; fill: none;'/><defs>  <clipPath id='cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw=='>    <rect x='0.00' y='0.00' width='198.43' height='14.17' />  </clipPath></defs><g clip-path='url(#cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw==)'><rect x='0.00' y='0.0000000000000018' width='1.61' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #0000FF;' /><rect x='1.61' y='0.0000000000000018' width='178.78' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #000000;' /><text x='0.80' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='10.24px' lengthAdjust='spacingAndGlyphs'>1%</text><text x='91.00' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='15.36px' lengthAdjust='spacingAndGlyphs'>99%</text></g></svg></td></tr>
    <tr><td class="gt_row gt_left">Vontobel</td>
<td class="gt_row gt_center"><?xml version='1.0' encoding='UTF-8' ?><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' class='svglite' width='198.43pt' height='14.17pt' viewBox='0 0 198.43 14.17'><defs>  <style type='text/css'><![CDATA[    .svglite line, .svglite polyline, .svglite polygon, .svglite path, .svglite rect, .svglite circle {      fill: none;      stroke: #000000;      stroke-linecap: round;      stroke-linejoin: round;      stroke-miterlimit: 10.00;    }    .svglite text {      white-space: pre;    }  ]]></style></defs><rect width='100%' height='100%' style='stroke: none; fill: none;'/><defs>  <clipPath id='cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw=='>    <rect x='0.00' y='0.00' width='198.43' height='14.17' />  </clipPath></defs><g clip-path='url(#cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw==)'><rect x='0.00' y='0.0000000000000018' width='12.31' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #0000FF;' /><rect x='12.31' y='0.0000000000000018' width='168.08' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #000000;' /><text x='6.15' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='10.24px' lengthAdjust='spacingAndGlyphs'>7%</text><text x='96.35' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='15.36px' lengthAdjust='spacingAndGlyphs'>93%</text></g></svg></td></tr>
    <tr><td class="gt_row gt_left">ZKB</td>
<td class="gt_row gt_center"><?xml version='1.0' encoding='UTF-8' ?><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' class='svglite' width='198.43pt' height='14.17pt' viewBox='0 0 198.43 14.17'><defs>  <style type='text/css'><![CDATA[    .svglite line, .svglite polyline, .svglite polygon, .svglite path, .svglite rect, .svglite circle {      fill: none;      stroke: #000000;      stroke-linecap: round;      stroke-linejoin: round;      stroke-miterlimit: 10.00;    }    .svglite text {      white-space: pre;    }  ]]></style></defs><rect width='100%' height='100%' style='stroke: none; fill: none;'/><defs>  <clipPath id='cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw=='>    <rect x='0.00' y='0.00' width='198.43' height='14.17' />  </clipPath></defs><g clip-path='url(#cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw==)'><rect x='0.00' y='0.0000000000000018' width='24.19' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #0000FF;' /><rect x='24.19' y='0.0000000000000018' width='156.20' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #000000;' /><text x='12.09' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='15.36px' lengthAdjust='spacingAndGlyphs'>13%</text><text x='102.29' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='15.36px' lengthAdjust='spacingAndGlyphs'>87%</text></g></svg></td></tr>
  </tbody>
  
  
</table>
</div>
```

Datenverfügbarkeit über Zeit:

<div class="figure">
<p class="caption">(\#fig:unnamed-chunk-12)Verfügbarkeit Emissionsintensitätsdaten, nach Bank, Anteil des Portfoliowerts, Q2/2014 - Q2/2021</p><img src="03-filings-mit-meta-ts_files/figure-html/unnamed-chunk-12-1.png" alt="Verfügbarkeit Emissionsintensitätsdaten, nach Bank, Anteil des Portfoliowerts, Q2/2014 - Q2/2021" width="672" /></div>







Verfügbarkeit GICS-Subindustriedaten:


```{=html}
<div id="jsqlpfjjpn" style="overflow-x:auto;overflow-y:auto;width:auto;height:auto;">
<style>html {
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Helvetica Neue', 'Fira Sans', 'Droid Sans', Arial, sans-serif;
}

#jsqlpfjjpn .gt_table {
  display: table;
  border-collapse: collapse;
  margin-left: auto;
  margin-right: auto;
  color: #333333;
  font-size: 16px;
  font-weight: normal;
  font-style: normal;
  background-color: #FFFFFF;
  width: auto;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #A8A8A8;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #A8A8A8;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
}

#jsqlpfjjpn .gt_heading {
  background-color: #FFFFFF;
  text-align: center;
  border-bottom-color: #FFFFFF;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#jsqlpfjjpn .gt_title {
  color: #333333;
  font-size: 125%;
  font-weight: initial;
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 5px;
  padding-right: 5px;
  border-bottom-color: #FFFFFF;
  border-bottom-width: 0;
}

#jsqlpfjjpn .gt_subtitle {
  color: #333333;
  font-size: 85%;
  font-weight: initial;
  padding-top: 0;
  padding-bottom: 6px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-color: #FFFFFF;
  border-top-width: 0;
}

#jsqlpfjjpn .gt_bottom_border {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#jsqlpfjjpn .gt_col_headings {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#jsqlpfjjpn .gt_col_heading {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 6px;
  padding-left: 5px;
  padding-right: 5px;
  overflow-x: hidden;
}

#jsqlpfjjpn .gt_column_spanner_outer {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  padding-top: 0;
  padding-bottom: 0;
  padding-left: 4px;
  padding-right: 4px;
}

#jsqlpfjjpn .gt_column_spanner_outer:first-child {
  padding-left: 0;
}

#jsqlpfjjpn .gt_column_spanner_outer:last-child {
  padding-right: 0;
}

#jsqlpfjjpn .gt_column_spanner {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 5px;
  overflow-x: hidden;
  display: inline-block;
  width: 100%;
}

#jsqlpfjjpn .gt_group_heading {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
}

#jsqlpfjjpn .gt_empty_group_heading {
  padding: 0.5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: middle;
}

#jsqlpfjjpn .gt_from_md > :first-child {
  margin-top: 0;
}

#jsqlpfjjpn .gt_from_md > :last-child {
  margin-bottom: 0;
}

#jsqlpfjjpn .gt_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  margin: 10px;
  border-top-style: solid;
  border-top-width: 1px;
  border-top-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
  overflow-x: hidden;
}

#jsqlpfjjpn .gt_stub {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 5px;
  padding-right: 5px;
}

#jsqlpfjjpn .gt_stub_row_group {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 5px;
  padding-right: 5px;
  vertical-align: top;
}

#jsqlpfjjpn .gt_row_group_first td {
  border-top-width: 2px;
}

#jsqlpfjjpn .gt_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#jsqlpfjjpn .gt_first_summary_row {
  border-top-style: solid;
  border-top-color: #D3D3D3;
}

#jsqlpfjjpn .gt_first_summary_row.thick {
  border-top-width: 2px;
}

#jsqlpfjjpn .gt_last_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#jsqlpfjjpn .gt_grand_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#jsqlpfjjpn .gt_first_grand_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-style: double;
  border-top-width: 6px;
  border-top-color: #D3D3D3;
}

#jsqlpfjjpn .gt_striped {
  background-color: rgba(128, 128, 128, 0.05);
}

#jsqlpfjjpn .gt_table_body {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#jsqlpfjjpn .gt_footnotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#jsqlpfjjpn .gt_footnote {
  margin: 0px;
  font-size: 90%;
  padding-left: 4px;
  padding-right: 4px;
  padding-left: 5px;
  padding-right: 5px;
}

#jsqlpfjjpn .gt_sourcenotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#jsqlpfjjpn .gt_sourcenote {
  font-size: 90%;
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 5px;
  padding-right: 5px;
}

#jsqlpfjjpn .gt_left {
  text-align: left;
}

#jsqlpfjjpn .gt_center {
  text-align: center;
}

#jsqlpfjjpn .gt_right {
  text-align: right;
  font-variant-numeric: tabular-nums;
}

#jsqlpfjjpn .gt_font_normal {
  font-weight: normal;
}

#jsqlpfjjpn .gt_font_bold {
  font-weight: bold;
}

#jsqlpfjjpn .gt_font_italic {
  font-style: italic;
}

#jsqlpfjjpn .gt_super {
  font-size: 65%;
}

#jsqlpfjjpn .gt_footnote_marks {
  font-style: italic;
  font-weight: normal;
  font-size: 75%;
  vertical-align: 0.4em;
}

#jsqlpfjjpn .gt_asterisk {
  font-size: 100%;
  vertical-align: 0;
}

#jsqlpfjjpn .gt_slash_mark {
  font-size: 0.7em;
  line-height: 0.7em;
  vertical-align: 0.15em;
}

#jsqlpfjjpn .gt_fraction_numerator {
  font-size: 0.6em;
  line-height: 0.6em;
  vertical-align: 0.45em;
}

#jsqlpfjjpn .gt_fraction_denominator {
  font-size: 0.6em;
  line-height: 0.6em;
  vertical-align: -0.05em;
}
</style>
<table class="gt_table">
  <caption>(#tab:unnamed-chunk-16)Verfügbarkeit GICS-Subindustrie nach Bank per Q2/2021</caption>
  
  <thead class="gt_col_headings">
    <tr>
      <th class="gt_col_heading gt_columns_bottom_border gt_left" rowspan="1" colspan="1">Bank</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_center" rowspan="1" colspan="1"><span style='color:blue'><b>Nicht Verfügbar</b></span>||<span style='color:black'><b>Verfügbar</b></span></th>
    </tr>
  </thead>
  <tbody class="gt_table_body">
    <tr><td class="gt_row gt_left">BCV</td>
<td class="gt_row gt_center"><?xml version='1.0' encoding='UTF-8' ?><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' class='svglite' width='198.43pt' height='14.17pt' viewBox='0 0 198.43 14.17'><defs>  <style type='text/css'><![CDATA[    .svglite line, .svglite polyline, .svglite polygon, .svglite path, .svglite rect, .svglite circle {      fill: none;      stroke: #000000;      stroke-linecap: round;      stroke-linejoin: round;      stroke-miterlimit: 10.00;    }    .svglite text {      white-space: pre;    }  ]]></style></defs><rect width='100%' height='100%' style='stroke: none; fill: none;'/><defs>  <clipPath id='cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw=='>    <rect x='0.00' y='0.00' width='198.43' height='14.17' />  </clipPath></defs><g clip-path='url(#cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw==)'><rect x='0.00' y='0.0000000000000018' width='24.14' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #0000FF;' /><rect x='24.14' y='0.0000000000000018' width='156.24' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #000000;' /><text x='12.07' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='15.36px' lengthAdjust='spacingAndGlyphs'>13%</text><text x='102.26' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='15.36px' lengthAdjust='spacingAndGlyphs'>87%</text></g></svg></td></tr>
    <tr><td class="gt_row gt_left">Credit Suisse</td>
<td class="gt_row gt_center"><?xml version='1.0' encoding='UTF-8' ?><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' class='svglite' width='198.43pt' height='14.17pt' viewBox='0 0 198.43 14.17'><defs>  <style type='text/css'><![CDATA[    .svglite line, .svglite polyline, .svglite polygon, .svglite path, .svglite rect, .svglite circle {      fill: none;      stroke: #000000;      stroke-linecap: round;      stroke-linejoin: round;      stroke-miterlimit: 10.00;    }    .svglite text {      white-space: pre;    }  ]]></style></defs><rect width='100%' height='100%' style='stroke: none; fill: none;'/><defs>  <clipPath id='cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw=='>    <rect x='0.00' y='0.00' width='198.43' height='14.17' />  </clipPath></defs><g clip-path='url(#cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw==)'><rect x='0.00' y='0.0000000000000018' width='11.83' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #0000FF;' /><rect x='11.83' y='0.0000000000000018' width='168.55' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #000000;' /><text x='5.92' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='10.24px' lengthAdjust='spacingAndGlyphs'>7%</text><text x='96.11' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='15.36px' lengthAdjust='spacingAndGlyphs'>93%</text></g></svg></td></tr>
    <tr><td class="gt_row gt_left">Edmond de Rothschild</td>
<td class="gt_row gt_center"><?xml version='1.0' encoding='UTF-8' ?><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' class='svglite' width='198.43pt' height='14.17pt' viewBox='0 0 198.43 14.17'><defs>  <style type='text/css'><![CDATA[    .svglite line, .svglite polyline, .svglite polygon, .svglite path, .svglite rect, .svglite circle {      fill: none;      stroke: #000000;      stroke-linecap: round;      stroke-linejoin: round;      stroke-miterlimit: 10.00;    }    .svglite text {      white-space: pre;    }  ]]></style></defs><rect width='100%' height='100%' style='stroke: none; fill: none;'/><defs>  <clipPath id='cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw=='>    <rect x='0.00' y='0.00' width='198.43' height='14.17' />  </clipPath></defs><g clip-path='url(#cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw==)'><rect x='0.00' y='0.0000000000000018' width='16.90' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #0000FF;' /><rect x='16.90' y='0.0000000000000018' width='163.48' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #000000;' /><text x='8.45' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='10.24px' lengthAdjust='spacingAndGlyphs'>9%</text><text x='98.64' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='15.36px' lengthAdjust='spacingAndGlyphs'>91%</text></g></svg></td></tr>
    <tr><td class="gt_row gt_left">Julius Bär</td>
<td class="gt_row gt_center"><?xml version='1.0' encoding='UTF-8' ?><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' class='svglite' width='198.43pt' height='14.17pt' viewBox='0 0 198.43 14.17'><defs>  <style type='text/css'><![CDATA[    .svglite line, .svglite polyline, .svglite polygon, .svglite path, .svglite rect, .svglite circle {      fill: none;      stroke: #000000;      stroke-linecap: round;      stroke-linejoin: round;      stroke-miterlimit: 10.00;    }    .svglite text {      white-space: pre;    }  ]]></style></defs><rect width='100%' height='100%' style='stroke: none; fill: none;'/><defs>  <clipPath id='cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw=='>    <rect x='0.00' y='0.00' width='198.43' height='14.17' />  </clipPath></defs><g clip-path='url(#cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw==)'><rect x='0.00' y='0.0000000000000018' width='0.78' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #0000FF;' /><rect x='0.78' y='0.0000000000000018' width='179.61' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #000000;' /><text x='0.39' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='10.24px' lengthAdjust='spacingAndGlyphs'>0%</text><text x='90.58' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='20.48px' lengthAdjust='spacingAndGlyphs'>100%</text></g></svg></td></tr>
    <tr><td class="gt_row gt_left">LGT</td>
<td class="gt_row gt_center"><?xml version='1.0' encoding='UTF-8' ?><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' class='svglite' width='198.43pt' height='14.17pt' viewBox='0 0 198.43 14.17'><defs>  <style type='text/css'><![CDATA[    .svglite line, .svglite polyline, .svglite polygon, .svglite path, .svglite rect, .svglite circle {      fill: none;      stroke: #000000;      stroke-linecap: round;      stroke-linejoin: round;      stroke-miterlimit: 10.00;    }    .svglite text {      white-space: pre;    }  ]]></style></defs><rect width='100%' height='100%' style='stroke: none; fill: none;'/><defs>  <clipPath id='cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw=='>    <rect x='0.00' y='0.00' width='198.43' height='14.17' />  </clipPath></defs><g clip-path='url(#cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw==)'><rect x='0.00' y='0.0000000000000018' width='0.00' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #0000FF;' /><rect x='0.00' y='0.0000000000000018' width='180.39' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #000000;' /><text x='0.00' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='10.24px' lengthAdjust='spacingAndGlyphs'>0%</text><text x='90.19' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='20.48px' lengthAdjust='spacingAndGlyphs'>100%</text></g></svg></td></tr>
    <tr><td class="gt_row gt_left">Lombard Odier</td>
<td class="gt_row gt_center"><?xml version='1.0' encoding='UTF-8' ?><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' class='svglite' width='198.43pt' height='14.17pt' viewBox='0 0 198.43 14.17'><defs>  <style type='text/css'><![CDATA[    .svglite line, .svglite polyline, .svglite polygon, .svglite path, .svglite rect, .svglite circle {      fill: none;      stroke: #000000;      stroke-linecap: round;      stroke-linejoin: round;      stroke-miterlimit: 10.00;    }    .svglite text {      white-space: pre;    }  ]]></style></defs><rect width='100%' height='100%' style='stroke: none; fill: none;'/><defs>  <clipPath id='cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw=='>    <rect x='0.00' y='0.00' width='198.43' height='14.17' />  </clipPath></defs><g clip-path='url(#cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw==)'><rect x='0.00' y='0.0000000000000018' width='17.67' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #0000FF;' /><rect x='17.67' y='0.0000000000000018' width='162.71' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #000000;' /><text x='8.84' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='15.36px' lengthAdjust='spacingAndGlyphs'>10%</text><text x='99.03' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='15.36px' lengthAdjust='spacingAndGlyphs'>90%</text></g></svg></td></tr>
    <tr><td class="gt_row gt_left">Pictet</td>
<td class="gt_row gt_center"><?xml version='1.0' encoding='UTF-8' ?><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' class='svglite' width='198.43pt' height='14.17pt' viewBox='0 0 198.43 14.17'><defs>  <style type='text/css'><![CDATA[    .svglite line, .svglite polyline, .svglite polygon, .svglite path, .svglite rect, .svglite circle {      fill: none;      stroke: #000000;      stroke-linecap: round;      stroke-linejoin: round;      stroke-miterlimit: 10.00;    }    .svglite text {      white-space: pre;    }  ]]></style></defs><rect width='100%' height='100%' style='stroke: none; fill: none;'/><defs>  <clipPath id='cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw=='>    <rect x='0.00' y='0.00' width='198.43' height='14.17' />  </clipPath></defs><g clip-path='url(#cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw==)'><rect x='0.00' y='0.0000000000000018' width='15.07' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #0000FF;' /><rect x='15.07' y='0.0000000000000018' width='165.31' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #000000;' /><text x='7.54' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='10.24px' lengthAdjust='spacingAndGlyphs'>8%</text><text x='97.73' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='15.36px' lengthAdjust='spacingAndGlyphs'>92%</text></g></svg></td></tr>
    <tr><td class="gt_row gt_left">UBS</td>
<td class="gt_row gt_center"><?xml version='1.0' encoding='UTF-8' ?><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' class='svglite' width='198.43pt' height='14.17pt' viewBox='0 0 198.43 14.17'><defs>  <style type='text/css'><![CDATA[    .svglite line, .svglite polyline, .svglite polygon, .svglite path, .svglite rect, .svglite circle {      fill: none;      stroke: #000000;      stroke-linecap: round;      stroke-linejoin: round;      stroke-miterlimit: 10.00;    }    .svglite text {      white-space: pre;    }  ]]></style></defs><rect width='100%' height='100%' style='stroke: none; fill: none;'/><defs>  <clipPath id='cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw=='>    <rect x='0.00' y='0.00' width='198.43' height='14.17' />  </clipPath></defs><g clip-path='url(#cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw==)'><rect x='0.00' y='0.0000000000000018' width='8.90' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #0000FF;' /><rect x='8.90' y='0.0000000000000018' width='171.48' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #000000;' /><text x='4.45' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='10.24px' lengthAdjust='spacingAndGlyphs'>5%</text><text x='94.64' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='15.36px' lengthAdjust='spacingAndGlyphs'>95%</text></g></svg></td></tr>
    <tr><td class="gt_row gt_left">Union Bancaire Privée</td>
<td class="gt_row gt_center"><?xml version='1.0' encoding='UTF-8' ?><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' class='svglite' width='198.43pt' height='14.17pt' viewBox='0 0 198.43 14.17'><defs>  <style type='text/css'><![CDATA[    .svglite line, .svglite polyline, .svglite polygon, .svglite path, .svglite rect, .svglite circle {      fill: none;      stroke: #000000;      stroke-linecap: round;      stroke-linejoin: round;      stroke-miterlimit: 10.00;    }    .svglite text {      white-space: pre;    }  ]]></style></defs><rect width='100%' height='100%' style='stroke: none; fill: none;'/><defs>  <clipPath id='cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw=='>    <rect x='0.00' y='0.00' width='198.43' height='14.17' />  </clipPath></defs><g clip-path='url(#cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw==)'><rect x='0.00' y='0.0000000000000018' width='2.98' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #0000FF;' /><rect x='2.98' y='0.0000000000000018' width='177.41' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #000000;' /><text x='1.49' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='10.24px' lengthAdjust='spacingAndGlyphs'>2%</text><text x='91.68' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='15.36px' lengthAdjust='spacingAndGlyphs'>98%</text></g></svg></td></tr>
    <tr><td class="gt_row gt_left">Vontobel</td>
<td class="gt_row gt_center"><?xml version='1.0' encoding='UTF-8' ?><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' class='svglite' width='198.43pt' height='14.17pt' viewBox='0 0 198.43 14.17'><defs>  <style type='text/css'><![CDATA[    .svglite line, .svglite polyline, .svglite polygon, .svglite path, .svglite rect, .svglite circle {      fill: none;      stroke: #000000;      stroke-linecap: round;      stroke-linejoin: round;      stroke-miterlimit: 10.00;    }    .svglite text {      white-space: pre;    }  ]]></style></defs><rect width='100%' height='100%' style='stroke: none; fill: none;'/><defs>  <clipPath id='cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw=='>    <rect x='0.00' y='0.00' width='198.43' height='14.17' />  </clipPath></defs><g clip-path='url(#cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw==)'><rect x='0.00' y='0.0000000000000018' width='11.13' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #0000FF;' /><rect x='11.13' y='0.0000000000000018' width='169.25' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #000000;' /><text x='5.57' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='10.24px' lengthAdjust='spacingAndGlyphs'>6%</text><text x='95.76' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='15.36px' lengthAdjust='spacingAndGlyphs'>94%</text></g></svg></td></tr>
    <tr><td class="gt_row gt_left">ZKB</td>
<td class="gt_row gt_center"><?xml version='1.0' encoding='UTF-8' ?><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' class='svglite' width='198.43pt' height='14.17pt' viewBox='0 0 198.43 14.17'><defs>  <style type='text/css'><![CDATA[    .svglite line, .svglite polyline, .svglite polygon, .svglite path, .svglite rect, .svglite circle {      fill: none;      stroke: #000000;      stroke-linecap: round;      stroke-linejoin: round;      stroke-miterlimit: 10.00;    }    .svglite text {      white-space: pre;    }  ]]></style></defs><rect width='100%' height='100%' style='stroke: none; fill: none;'/><defs>  <clipPath id='cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw=='>    <rect x='0.00' y='0.00' width='198.43' height='14.17' />  </clipPath></defs><g clip-path='url(#cpMC4wMHwxOTguNDN8MC4wMHwxNC4xNw==)'><rect x='0.00' y='0.0000000000000018' width='22.53' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #0000FF;' /><rect x='22.53' y='0.0000000000000018' width='157.85' height='14.17' style='stroke-width: 2.13; stroke: #FFFFFF; stroke-linecap: square; stroke-linejoin: miter; fill: #000000;' /><text x='11.27' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='15.36px' lengthAdjust='spacingAndGlyphs'>12%</text><text x='101.46' y='9.56' text-anchor='middle' style='font-size: 8.54px; font-weight: 0;fill: #FFFFFF; font-family: "Courier";' textLength='15.36px' lengthAdjust='spacingAndGlyphs'>88%</text></g></svg></td></tr>
  </tbody>
  
  
</table>
</div>
```

Portfolios nach Asset-Type und Bank:

<div class="figure">
<p class="caption">(\#fig:unnamed-chunk-17)Portfoliowert, nach relevanter Anlageklasse und Bank, Q2/2014 - Q2/2021</p><img src="03-filings-mit-meta-ts_files/figure-html/unnamed-chunk-17-1.png" alt="Portfoliowert, nach relevanter Anlageklasse und Bank, Q2/2014 - Q2/2021" width="672" /></div>



<!-- Vor dem Rausfiltern von allen anderen Assets überprüfen: -->

<!-- Komplettheitstabelle für alle CO2-Variablen für Aktien: -->





<!-- ## Verfügbarkeit der Metadaten -->

<!-- -   NA-Kategorie nochmals überprüfen! -->

<!--     -   sinnvoll hier schon zu filtern? (ev. Firmeninfos vorhanden ohne Asset Category?) -->



<!-- Welcher Anteil GICS vorhanden innerhalb der Kategorie American Depository Receipt + Ordinary Share? -->



<!-- ### Variablenkomplettheit -->



<!-- ### CO2-Ausstoss -->

<!-- `estimated_co2_equivalents_emission_total`: -->



<!-- Verfügbarkeit von `co2_equivalent_emissions_indirect_scope_3_to_revenues_usd_in_million`: -->



<!-- ## Portfolios nach Bank und Industrie -->



Fehlender Anteil des Datensets (Anteile der Asset Class der Wertpapiere, der nicht bestimmt werden kann):

<div class="figure">
<p class="caption">(\#fig:unnamed-chunk-27)Anteil der Asset Class der Wertpapiere, die nicht bestimmt werden kann</p><img src="03-filings-mit-meta-ts_files/figure-html/unnamed-chunk-27-1.png" alt="Anteil der Asset Class der Wertpapiere, die nicht bestimmt werden kann" width="672" /></div>

Fehlender Anteil des Datensets (Anteile der Asset Class der Wertpapiere, der nicht bestimmt werden kann):

<div class="figure">
<p class="caption">(\#fig:unnamed-chunk-28)Anteil der CO2-Werte der Wertpapiere, der nicht bestimmt werden kann, nach Bank</p><img src="03-filings-mit-meta-ts_files/figure-html/unnamed-chunk-28-1.png" alt="Anteil der CO2-Werte der Wertpapiere, der nicht bestimmt werden kann, nach Bank" width="672" /></div>

### Mögliche Fehler?

Wie diversifiziert sind die Investitionen? Gibt es irgendwelche Fehler?



<div class="figure">
<p class="caption">(\#fig:unnamed-chunk-30)Diversifikation Portfolios, nach Bank, Q2/2014 - Q2/2021</p><img src="03-filings-mit-meta-ts_files/figure-html/unnamed-chunk-30-1.png" alt="Diversifikation Portfolios, nach Bank, Q2/2014 - Q2/2021" width="672" /></div>


<div class="figure">
<p class="caption">(\#fig:unnamed-chunk-31)Portfoliogrössen, nach Bank, Q2/2014 - Q2/2021, ohne UBS und Credit Suisse</p><img src="03-filings-mit-meta-ts_files/figure-html/unnamed-chunk-31-1.png" alt="Portfoliogrössen, nach Bank, Q2/2014 - Q2/2021, ohne UBS und Credit Suisse" width="672" /></div>


-   Seltsamer Anstieg für Vontobel (Anzahl der Subindustrien und weiteren Metrics gegen Ende!)
-   LGT und Vontobel haben laut diesen Zahlen sehr grosse Positionen in wenigen Industrien --- plausibel, oder fehlt da etwas?
  -   könnte auch sein, dass Vontobel gegen Ende sehr viel neues Geld (das sie ev. auch in ihrer grossen Fixed Income-Abteilung in Equities gesteckt haben), diversifizierter anlegte als zuvor? --\> wenig plausibel
  - Vontobels Portfoliowert vervielfacht sich: vermutlich fehlt da was  
- deshalb: Vontobel und LGT keine Kandidaten für Inklusion in der Analyse

## Klimaanalyse {#methodology}

### Herangehensweisen

Wie quantifiziert man ein Portfolio nach Umweltverträglichkeit?

Folgende Vorgehensweisen bieten sich an:

- **Green/Brown-Share Approach**: Aufteilen der Portfolios in «grüne» und «braune» Industrien: Ist die durchschnittliche CO2-Intensität einer Industrie per Q4/2021 besonders hoch (grösser als die Emissionen von 85 % aller Unternehmen), ist diese «CO2-intensiv». Der wertmässige, relative Anteil der CO2-intensivsten Industrien am Gesamtportfolio wird danach berechnet.
- **Emissionsintensität**: Anteil des Portfolios der Firmen, die mehr als 90 %/85 %/80 % aller anderen Unternehmen ausstossen
- Kapitalgewichtete CO2-Intensität des Gesamtportfolios, gewichtet nach Wert der Anlagen

Die ersten beiden Herangehensweisen werden für den Artikel verwendet, um die Risikokomponente besser zu betonen. 



Welches sind die «schmutzigsten» Industrien?

Das finden wir heraus, in dem wir die Variable «Estimated CO2 Equivalents Emission Total» (`TR.AnalyticEstimatedCO2Total`, in Tonnen ([S.3](https://www.refinitiv.com/content/dam/marketing/en_us/documents/fact-sheets/esg-carbon-data-estimate-models-fact-sheet.pdf)) durch den «Total Revenue from Business Activity» (`TR.TotalRevenue` (in Mio. US-Dollar, eigens transformiert)) von Eikon Refinitiv teilen.

Für den Umsatz gäbe grundsätzlich die Felder

-   `TR.TotalRevenue`
    -   "Revenue based on the last reported year from all of a company's operating activities after deducting any sales adjustments and their equivalents"
-   `TR.RevenueMean`
-   `TR.RevenueActValue`
-   `TR.PCTotRevenueFromBizActv`

Folgendes ist enthalten in «Estimated CO2 Equivalents Emission Total» (*«Total Carbon dioxide (CO2) and CO2 equivalents emission in tonnes»)* ([s. hier S. 27](https://www.eifr.eu/uploads/eventdocs/60db1a075b141577568155.pdf)) :

-   carbon dioxide (CO2)

-   methane (CH4)

-   nitrous oxide (N2O)

-   hydrofluorocarbons (HFCS)

-   perfluorinated compound (PFCS)

-   sulfur hexafluoride (SF6)

-   nitrogen trifluoride (NF3)

**Berechnung**: total CO2 emission = direct (scope1) + indirect (scope 2)

Pro Umsatz in US-Dollar wird dann für jede Industrie (nach GICS 6-Kategorie) ihr Durchschnitt berechnet.

Das bedeutet dass wir dann sehen welche Industrien relativ zum Umsatz am meisten Grünhausgase gemessen in CO2-Äquivalenten ausstösst.

Diese Methodik hat eine Limitation: Refinitiv schätzt diesen Wert für die Firmen, die ihre Emissionen nicht veröffentlichen, nach eigenen Methoden ([siehe Details hier](https://www.refinitiv.com/content/dam/marketing/en_us/documents/fact-sheets/esg-carbon-data-estimate-models-fact-sheet.pdf)). 
Ich verwende diese Variable dennoch, da so für rund zwei Drittel aller Firmen ein Emissionswert vorhanden ist --- bei den anderen Variablen, die nicht künstlich ergänzt werden ist dieser Wert beachtlich tiefer (rund ein Fünftel).

-   Erklärung für CO2-Äquivalente: <https://ecometrica.com/assets/GHGs-CO2-CO2e-and-Carbon-What-Do-These-Mean-v2.1.pdf>

> CO2-equivalent: "amount of CO2 which would warm the earth as much as that amount of that gas. Thus it provides a common scale for measuring the climate effects of different gases"







Tabelle \@ref(tab:emissionenquantile) zeigt wie gross die typischen Emissionen von Firmen sind, in die die Schweizer Banken investiert haben.


```{=html}
<div id="hznmbqcalg" style="overflow-x:auto;overflow-y:auto;width:auto;height:auto;">
<style>html {
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Helvetica Neue', 'Fira Sans', 'Droid Sans', Arial, sans-serif;
}

#hznmbqcalg .gt_table {
  display: table;
  border-collapse: collapse;
  margin-left: auto;
  margin-right: auto;
  color: #333333;
  font-size: small;
  font-weight: normal;
  font-style: normal;
  background-color: #FFFFFF;
  width: auto;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #A8A8A8;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #A8A8A8;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
}

#hznmbqcalg .gt_heading {
  background-color: #FFFFFF;
  text-align: center;
  border-bottom-color: #FFFFFF;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#hznmbqcalg .gt_title {
  color: #333333;
  font-size: 125%;
  font-weight: initial;
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 5px;
  padding-right: 5px;
  border-bottom-color: #FFFFFF;
  border-bottom-width: 0;
}

#hznmbqcalg .gt_subtitle {
  color: #333333;
  font-size: 85%;
  font-weight: initial;
  padding-top: 0;
  padding-bottom: 6px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-color: #FFFFFF;
  border-top-width: 0;
}

#hznmbqcalg .gt_bottom_border {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#hznmbqcalg .gt_col_headings {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#hznmbqcalg .gt_col_heading {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 6px;
  padding-left: 5px;
  padding-right: 5px;
  overflow-x: hidden;
}

#hznmbqcalg .gt_column_spanner_outer {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  padding-top: 0;
  padding-bottom: 0;
  padding-left: 4px;
  padding-right: 4px;
}

#hznmbqcalg .gt_column_spanner_outer:first-child {
  padding-left: 0;
}

#hznmbqcalg .gt_column_spanner_outer:last-child {
  padding-right: 0;
}

#hznmbqcalg .gt_column_spanner {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 5px;
  overflow-x: hidden;
  display: inline-block;
  width: 100%;
}

#hznmbqcalg .gt_group_heading {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
}

#hznmbqcalg .gt_empty_group_heading {
  padding: 0.5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: middle;
}

#hznmbqcalg .gt_from_md > :first-child {
  margin-top: 0;
}

#hznmbqcalg .gt_from_md > :last-child {
  margin-bottom: 0;
}

#hznmbqcalg .gt_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  margin: 10px;
  border-top-style: solid;
  border-top-width: 1px;
  border-top-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
  overflow-x: hidden;
}

#hznmbqcalg .gt_stub {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 5px;
  padding-right: 5px;
}

#hznmbqcalg .gt_stub_row_group {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 5px;
  padding-right: 5px;
  vertical-align: top;
}

#hznmbqcalg .gt_row_group_first td {
  border-top-width: 2px;
}

#hznmbqcalg .gt_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#hznmbqcalg .gt_first_summary_row {
  border-top-style: solid;
  border-top-color: #D3D3D3;
}

#hznmbqcalg .gt_first_summary_row.thick {
  border-top-width: 2px;
}

#hznmbqcalg .gt_last_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#hznmbqcalg .gt_grand_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#hznmbqcalg .gt_first_grand_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-style: double;
  border-top-width: 6px;
  border-top-color: #D3D3D3;
}

#hznmbqcalg .gt_striped {
  background-color: rgba(128, 128, 128, 0.05);
}

#hznmbqcalg .gt_table_body {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#hznmbqcalg .gt_footnotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#hznmbqcalg .gt_footnote {
  margin: 0px;
  font-size: 90%;
  padding-left: 4px;
  padding-right: 4px;
  padding-left: 5px;
  padding-right: 5px;
}

#hznmbqcalg .gt_sourcenotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#hznmbqcalg .gt_sourcenote {
  font-size: 90%;
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 5px;
  padding-right: 5px;
}

#hznmbqcalg .gt_left {
  text-align: left;
}

#hznmbqcalg .gt_center {
  text-align: center;
}

#hznmbqcalg .gt_right {
  text-align: right;
  font-variant-numeric: tabular-nums;
}

#hznmbqcalg .gt_font_normal {
  font-weight: normal;
}

#hznmbqcalg .gt_font_bold {
  font-weight: bold;
}

#hznmbqcalg .gt_font_italic {
  font-style: italic;
}

#hznmbqcalg .gt_super {
  font-size: 65%;
}

#hznmbqcalg .gt_footnote_marks {
  font-style: italic;
  font-weight: normal;
  font-size: 75%;
  vertical-align: 0.4em;
}

#hznmbqcalg .gt_asterisk {
  font-size: 100%;
  vertical-align: 0;
}

#hznmbqcalg .gt_slash_mark {
  font-size: 0.7em;
  line-height: 0.7em;
  vertical-align: 0.15em;
}

#hznmbqcalg .gt_fraction_numerator {
  font-size: 0.6em;
  line-height: 0.6em;
  vertical-align: 0.45em;
}

#hznmbqcalg .gt_fraction_denominator {
  font-size: 0.6em;
  line-height: 0.6em;
  vertical-align: -0.05em;
}
</style>
<table class="gt_table">
  
  <thead class="gt_col_headings">
    <tr>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1">Quantil</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1">CO2-Äq./Mio. USD Umsatz<sup class="gt_footnote_marks">1</sup></th>
    </tr>
  </thead>
  <tbody class="gt_table_body">
    <tr><td class="gt_row gt_right">0</td>
<td class="gt_row gt_right">0.0</td></tr>
    <tr><td class="gt_row gt_right">5</td>
<td class="gt_row gt_right">1.3</td></tr>
    <tr><td class="gt_row gt_right">10</td>
<td class="gt_row gt_right">3.1</td></tr>
    <tr><td class="gt_row gt_right">15</td>
<td class="gt_row gt_right">4.2</td></tr>
    <tr><td class="gt_row gt_right">20</td>
<td class="gt_row gt_right">5.1</td></tr>
    <tr><td class="gt_row gt_right">25</td>
<td class="gt_row gt_right">6.0</td></tr>
    <tr><td class="gt_row gt_right">30</td>
<td class="gt_row gt_right">7.4</td></tr>
    <tr><td class="gt_row gt_right">35</td>
<td class="gt_row gt_right">9.6</td></tr>
    <tr><td class="gt_row gt_right">40</td>
<td class="gt_row gt_right">12.0</td></tr>
    <tr><td class="gt_row gt_right">45</td>
<td class="gt_row gt_right">14.8</td></tr>
    <tr><td class="gt_row gt_right">50</td>
<td class="gt_row gt_right">18.1</td></tr>
    <tr><td class="gt_row gt_right">55</td>
<td class="gt_row gt_right">22.6</td></tr>
    <tr><td class="gt_row gt_right">60</td>
<td class="gt_row gt_right">28.3</td></tr>
    <tr><td class="gt_row gt_right">65</td>
<td class="gt_row gt_right">36.0</td></tr>
    <tr><td class="gt_row gt_right">70</td>
<td class="gt_row gt_right">47.4</td></tr>
    <tr><td class="gt_row gt_right">75</td>
<td class="gt_row gt_right">65.3</td></tr>
    <tr><td class="gt_row gt_right">80</td>
<td class="gt_row gt_right">99.0</td></tr>
    <tr><td class="gt_row gt_right">85</td>
<td class="gt_row gt_right">189.9</td></tr>
    <tr><td class="gt_row gt_right">90</td>
<td class="gt_row gt_right">411.9</td></tr>
    <tr><td class="gt_row gt_right">95</td>
<td class="gt_row gt_right">1'005.2</td></tr>
    <tr><td class="gt_row gt_right">100</td>
<td class="gt_row gt_right">32'373.6</td></tr>
  </tbody>
  <tfoot class="gt_sourcenotes">
    <tr>
      <td class="gt_sourcenote" colspan="2">Daten: Refinitiv (Industrie- und GHG-Daten). Q4/2021, US Securities and Exchange Commission (13F-Filings)</td>
    </tr>
  </tfoot>
  <tfoot class="gt_footnotes">
    <tr>
      <td class="gt_footnote" colspan="2"><sup class="gt_footnote_marks">1</sup> Emissionen auf eine Dezimalstelle gerundet.</td>
    </tr>
  </tfoot>
</table>
</div>
```

Im Median (50%-Quantil) stiessen die Firmen 18 Tonnen CO2-Äquivalente aus. Nur 10% der Firmen stiessen 412 Tonnen CO2-Äquivalente oder mehr aus. Das zeigt auch Abbildung \@ref(fig:ggemissionenquantile).

Diese Grenze nehmen wir für die Abgrenzung, ob ein Unternehmen CO2-intensiv ist oder nicht.

<div class="figure">
<p class="caption">(\#fig:ggemissionenquantile)Geschätzte Emissionen, CO2-Äquivalente nach Umsatz, Quantile</p><img src="03-filings-mit-meta-ts_files/figure-html/ggemissionenquantile-1.png" alt="Geschätzte Emissionen, CO2-Äquivalente nach Umsatz, Quantile" width="672" /></div>

Tabelle \@ref(tab:emissionennachindustriegics) zeigt die durchschnittlichen Unternehmensemissionen nach Industrie der Haupttätigkeit (gemäss GICS6-Schema).


```{=html}
<div id="ypamcigfrw" style="overflow-x:auto;overflow-y:auto;width:auto;height:auto;">
<style>html {
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Helvetica Neue', 'Fira Sans', 'Droid Sans', Arial, sans-serif;
}

#ypamcigfrw .gt_table {
  display: table;
  border-collapse: collapse;
  margin-left: auto;
  margin-right: auto;
  color: #333333;
  font-size: small;
  font-weight: normal;
  font-style: normal;
  background-color: #FFFFFF;
  width: auto;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #A8A8A8;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #A8A8A8;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
}

#ypamcigfrw .gt_heading {
  background-color: #FFFFFF;
  text-align: center;
  border-bottom-color: #FFFFFF;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#ypamcigfrw .gt_title {
  color: #333333;
  font-size: 125%;
  font-weight: initial;
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 5px;
  padding-right: 5px;
  border-bottom-color: #FFFFFF;
  border-bottom-width: 0;
}

#ypamcigfrw .gt_subtitle {
  color: #333333;
  font-size: 85%;
  font-weight: initial;
  padding-top: 0;
  padding-bottom: 6px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-color: #FFFFFF;
  border-top-width: 0;
}

#ypamcigfrw .gt_bottom_border {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#ypamcigfrw .gt_col_headings {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#ypamcigfrw .gt_col_heading {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 6px;
  padding-left: 5px;
  padding-right: 5px;
  overflow-x: hidden;
}

#ypamcigfrw .gt_column_spanner_outer {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  padding-top: 0;
  padding-bottom: 0;
  padding-left: 4px;
  padding-right: 4px;
}

#ypamcigfrw .gt_column_spanner_outer:first-child {
  padding-left: 0;
}

#ypamcigfrw .gt_column_spanner_outer:last-child {
  padding-right: 0;
}

#ypamcigfrw .gt_column_spanner {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 5px;
  overflow-x: hidden;
  display: inline-block;
  width: 100%;
}

#ypamcigfrw .gt_group_heading {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
}

#ypamcigfrw .gt_empty_group_heading {
  padding: 0.5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: middle;
}

#ypamcigfrw .gt_from_md > :first-child {
  margin-top: 0;
}

#ypamcigfrw .gt_from_md > :last-child {
  margin-bottom: 0;
}

#ypamcigfrw .gt_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  margin: 10px;
  border-top-style: solid;
  border-top-width: 1px;
  border-top-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
  overflow-x: hidden;
}

#ypamcigfrw .gt_stub {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 5px;
  padding-right: 5px;
}

#ypamcigfrw .gt_stub_row_group {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 5px;
  padding-right: 5px;
  vertical-align: top;
}

#ypamcigfrw .gt_row_group_first td {
  border-top-width: 2px;
}

#ypamcigfrw .gt_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#ypamcigfrw .gt_first_summary_row {
  border-top-style: solid;
  border-top-color: #D3D3D3;
}

#ypamcigfrw .gt_first_summary_row.thick {
  border-top-width: 2px;
}

#ypamcigfrw .gt_last_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#ypamcigfrw .gt_grand_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#ypamcigfrw .gt_first_grand_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-style: double;
  border-top-width: 6px;
  border-top-color: #D3D3D3;
}

#ypamcigfrw .gt_striped {
  background-color: rgba(128, 128, 128, 0.05);
}

#ypamcigfrw .gt_table_body {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#ypamcigfrw .gt_footnotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#ypamcigfrw .gt_footnote {
  margin: 0px;
  font-size: 90%;
  padding-left: 4px;
  padding-right: 4px;
  padding-left: 5px;
  padding-right: 5px;
}

#ypamcigfrw .gt_sourcenotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#ypamcigfrw .gt_sourcenote {
  font-size: 90%;
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 5px;
  padding-right: 5px;
}

#ypamcigfrw .gt_left {
  text-align: left;
}

#ypamcigfrw .gt_center {
  text-align: center;
}

#ypamcigfrw .gt_right {
  text-align: right;
  font-variant-numeric: tabular-nums;
}

#ypamcigfrw .gt_font_normal {
  font-weight: normal;
}

#ypamcigfrw .gt_font_bold {
  font-weight: bold;
}

#ypamcigfrw .gt_font_italic {
  font-style: italic;
}

#ypamcigfrw .gt_super {
  font-size: 65%;
}

#ypamcigfrw .gt_footnote_marks {
  font-style: italic;
  font-weight: normal;
  font-size: 75%;
  vertical-align: 0.4em;
}

#ypamcigfrw .gt_asterisk {
  font-size: 100%;
  vertical-align: 0;
}

#ypamcigfrw .gt_slash_mark {
  font-size: 0.7em;
  line-height: 0.7em;
  vertical-align: 0.15em;
}

#ypamcigfrw .gt_fraction_numerator {
  font-size: 0.6em;
  line-height: 0.6em;
  vertical-align: 0.45em;
}

#ypamcigfrw .gt_fraction_denominator {
  font-size: 0.6em;
  line-height: 0.6em;
  vertical-align: -0.05em;
}
</style>
<table class="gt_table">
  <caption>(#tab:emissionennachindustriegics)Durchschnittliche Emissionen nach GICS-Industrie, Tabelle</caption>
  
  <thead class="gt_col_headings">
    <tr>
      <th class="gt_col_heading gt_columns_bottom_border gt_left" rowspan="1" colspan="1">Industrie</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1">Industrie Code</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_left" rowspan="1" colspan="1">Sektor</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1">CO2-Äq./Mio. USD Umsatz<sup class="gt_footnote_marks">1</sup></th>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1">0</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1">25</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1">50</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1">75</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1">100</th>
    </tr>
  </thead>
  <tbody class="gt_table_body">
    <tr><td class="gt_row gt_left">Energie-Agenturen</td>
<td class="gt_row gt_right">55105010</td>
<td class="gt_row gt_left">Versorgungsunternehmen</td>
<td class="gt_row gt_right" style="background-color: #808080; color: #000000;">7'126.1</td>
<td class="gt_row gt_right">47.8</td>
<td class="gt_row gt_right">2'778.9</td>
<td class="gt_row gt_right">7'796.3</td>
<td class="gt_row gt_right">10'995.4</td>
<td class="gt_row gt_right">14'489.8</td></tr>
    <tr><td class="gt_row gt_left">Kohle und nicht-erneuerbare Brennstoffe</td>
<td class="gt_row gt_right">10102050</td>
<td class="gt_row gt_left">Energie</td>
<td class="gt_row gt_right" style="background-color: #FE1100; color: #FFFFFF;">2'974.7</td>
<td class="gt_row gt_right">155.3</td>
<td class="gt_row gt_right">852.8</td>
<td class="gt_row gt_right">1'459.6</td>
<td class="gt_row gt_right">3'413.8</td>
<td class="gt_row gt_right">14'475.4</td></tr>
    <tr><td class="gt_row gt_left">Industriegase</td>
<td class="gt_row gt_right">15101040</td>
<td class="gt_row gt_left">Material: Roh- und Grundstoffe</td>
<td class="gt_row gt_right" style="background-color: #EB6F00; color: #000000;">2'344.3</td>
<td class="gt_row gt_right">2'344.3</td>
<td class="gt_row gt_right">2'344.3</td>
<td class="gt_row gt_right">2'344.3</td>
<td class="gt_row gt_right">2'344.3</td>
<td class="gt_row gt_right">2'344.3</td></tr>
    <tr><td class="gt_row gt_left">Multi-Versorger</td>
<td class="gt_row gt_right">55103010</td>
<td class="gt_row gt_left">Versorgungsunternehmen</td>
<td class="gt_row gt_right" style="background-color: #E77900; color: #000000;">2'213.3</td>
<td class="gt_row gt_right">299.0</td>
<td class="gt_row gt_right">831.1</td>
<td class="gt_row gt_right">1'560.0</td>
<td class="gt_row gt_right">3'172.9</td>
<td class="gt_row gt_right">6'074.6</td></tr>
    <tr><td class="gt_row gt_left">Stromversorgungsunternehmen</td>
<td class="gt_row gt_right">55101010</td>
<td class="gt_row gt_left">Versorgungsunternehmen</td>
<td class="gt_row gt_right" style="background-color: #E18600; color: #000000;">2'053.9</td>
<td class="gt_row gt_right">13.6</td>
<td class="gt_row gt_right">315.4</td>
<td class="gt_row gt_right">1'448.2</td>
<td class="gt_row gt_right">3'264.9</td>
<td class="gt_row gt_right">8'587.1</td></tr>
    <tr><td class="gt_row gt_left">Aluminium</td>
<td class="gt_row gt_right">15104010</td>
<td class="gt_row gt_left">Material: Roh- und Grundstoffe</td>
<td class="gt_row gt_right" style="background-color: #D79700; color: #000000;">1'803.1</td>
<td class="gt_row gt_right">287.2</td>
<td class="gt_row gt_right">1'043.7</td>
<td class="gt_row gt_right">2'018.0</td>
<td class="gt_row gt_right">2'225.1</td>
<td class="gt_row gt_right">3'482.5</td></tr>
    <tr><td class="gt_row gt_left">Baustoffe</td>
<td class="gt_row gt_right">15102010</td>
<td class="gt_row gt_left">Material: Roh- und Grundstoffe</td>
<td class="gt_row gt_right" style="background-color: #CCA700; color: #000000;">1'567.7</td>
<td class="gt_row gt_right">0.0</td>
<td class="gt_row gt_right">198.7</td>
<td class="gt_row gt_right">1'268.7</td>
<td class="gt_row gt_right">2'293.7</td>
<td class="gt_row gt_right">4'302.0</td></tr>
    <tr><td class="gt_row gt_left">diverse Chemikalien</td>
<td class="gt_row gt_right">15101020</td>
<td class="gt_row gt_left">Material: Roh- und Grundstoffe</td>
<td class="gt_row gt_right" style="background-color: #C2B300; color: #000000;">1'370.2</td>
<td class="gt_row gt_right">182.9</td>
<td class="gt_row gt_right">417.1</td>
<td class="gt_row gt_right">702.3</td>
<td class="gt_row gt_right">1'242.1</td>
<td class="gt_row gt_right">4'889.9</td></tr>
    <tr><td class="gt_row gt_left">Erdöl u. Erdgas: Lagerung u. Transport</td>
<td class="gt_row gt_right">10102040</td>
<td class="gt_row gt_left">Energie</td>
<td class="gt_row gt_right" style="background-color: #BDB800; color: #000000;">1'282.8</td>
<td class="gt_row gt_right">16.2</td>
<td class="gt_row gt_right">526.1</td>
<td class="gt_row gt_right">1'203.6</td>
<td class="gt_row gt_right">1'546.8</td>
<td class="gt_row gt_right">5'880.1</td></tr>
    <tr><td class="gt_row gt_left">Bau- und Schwerlastfahrzeuge [inkl. ziviler Schiffbau]</td>
<td class="gt_row gt_right">20106010</td>
<td class="gt_row gt_left">Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport</td>
<td class="gt_row gt_right" style="background-color: #BABB00; color: #000000;">1'232.2</td>
<td class="gt_row gt_right">8.6</td>
<td class="gt_row gt_right">17.1</td>
<td class="gt_row gt_right">28.9</td>
<td class="gt_row gt_right">47.4</td>
<td class="gt_row gt_right">32'373.6</td></tr>
    <tr><td class="gt_row gt_left">Stahl</td>
<td class="gt_row gt_right">15104050</td>
<td class="gt_row gt_left">Material: Roh- und Grundstoffe</td>
<td class="gt_row gt_right" style="background-color: #B2C300; color: #000000;">1'108.1</td>
<td class="gt_row gt_right">64.6</td>
<td class="gt_row gt_right">509.9</td>
<td class="gt_row gt_right">596.9</td>
<td class="gt_row gt_right">1'385.2</td>
<td class="gt_row gt_right">4'731.6</td></tr>
    <tr><td class="gt_row gt_left">Fluggesellschaften</td>
<td class="gt_row gt_right">20302010</td>
<td class="gt_row gt_left">Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport</td>
<td class="gt_row gt_right" style="background-color: #AFC500; color: #000000;">1'064.4</td>
<td class="gt_row gt_right">161.2</td>
<td class="gt_row gt_right">663.1</td>
<td class="gt_row gt_right">962.9</td>
<td class="gt_row gt_right">1'436.5</td>
<td class="gt_row gt_right">2'624.3</td></tr>
    <tr><td class="gt_row gt_left">Umwelt- und Anlagendienste</td>
<td class="gt_row gt_right">20201050</td>
<td class="gt_row gt_left">Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport</td>
<td class="gt_row gt_right" style="background-color: #A7CC00; color: #000000;">948.7</td>
<td class="gt_row gt_right">4.3</td>
<td class="gt_row gt_right">17.3</td>
<td class="gt_row gt_right">109.3</td>
<td class="gt_row gt_right">533.8</td>
<td class="gt_row gt_right">14'709.9</td></tr>
    <tr><td class="gt_row gt_left">Elektrische Komponenten u. Geräte [ohne Computer]</td>
<td class="gt_row gt_right">20104010</td>
<td class="gt_row gt_left">Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport</td>
<td class="gt_row gt_right" style="background-color: #A6CC00; color: #000000;">936.4</td>
<td class="gt_row gt_right">12.2</td>
<td class="gt_row gt_right">23.8</td>
<td class="gt_row gt_right">38.4</td>
<td class="gt_row gt_right">57.7</td>
<td class="gt_row gt_right">20'966.2</td></tr>
    <tr><td class="gt_row gt_left">Metalle u. Bergbau: diverse</td>
<td class="gt_row gt_right">15104020</td>
<td class="gt_row gt_left">Material: Roh- und Grundstoffe</td>
<td class="gt_row gt_right" style="background-color: #A4CF00; color: #000000;">897.5</td>
<td class="gt_row gt_right">171.0</td>
<td class="gt_row gt_right">304.0</td>
<td class="gt_row gt_right">442.1</td>
<td class="gt_row gt_right">686.3</td>
<td class="gt_row gt_right">5'148.1</td></tr>
    <tr><td class="gt_row gt_left">Erdöl u. Erdgas: Exploration u. Förderung</td>
<td class="gt_row gt_right">10102020</td>
<td class="gt_row gt_left">Energie</td>
<td class="gt_row gt_right" style="background-color: #A0D100; color: #000000;">851.3</td>
<td class="gt_row gt_right">10.0</td>
<td class="gt_row gt_right">474.4</td>
<td class="gt_row gt_right">730.1</td>
<td class="gt_row gt_right">1'102.0</td>
<td class="gt_row gt_right">3'870.7</td></tr>
    <tr><td class="gt_row gt_left">Schifffahrt [ohne Kreuzfahrtlinien]</td>
<td class="gt_row gt_right">20303010</td>
<td class="gt_row gt_left">Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport</td>
<td class="gt_row gt_right" style="background-color: #9BD400; color: #000000;">791.7</td>
<td class="gt_row gt_right">358.4</td>
<td class="gt_row gt_right">575.1</td>
<td class="gt_row gt_right">791.7</td>
<td class="gt_row gt_right">1'008.4</td>
<td class="gt_row gt_right">1'225.0</td></tr>
    <tr><td class="gt_row gt_left">Integrierte Erdöl- und Erdgasunternehmen</td>
<td class="gt_row gt_right">10102010</td>
<td class="gt_row gt_left">Energie</td>
<td class="gt_row gt_right" style="background-color: #96D800; color: #000000;">734.0</td>
<td class="gt_row gt_right">282.1</td>
<td class="gt_row gt_right">377.5</td>
<td class="gt_row gt_right">643.6</td>
<td class="gt_row gt_right">886.4</td>
<td class="gt_row gt_right">1'683.9</td></tr>
    <tr><td class="gt_row gt_left">Metall- u. Glasverpackungen</td>
<td class="gt_row gt_right">15103010</td>
<td class="gt_row gt_left">Material: Roh- und Grundstoffe</td>
<td class="gt_row gt_right" style="background-color: #96D800; color: #000000;">733.4</td>
<td class="gt_row gt_right">18.7</td>
<td class="gt_row gt_right">80.5</td>
<td class="gt_row gt_right">129.8</td>
<td class="gt_row gt_right">230.8</td>
<td class="gt_row gt_right">5'481.9</td></tr>
    <tr><td class="gt_row gt_left">Grundchemikalien</td>
<td class="gt_row gt_right">15101010</td>
<td class="gt_row gt_left">Material: Roh- und Grundstoffe</td>
<td class="gt_row gt_right" style="background-color: #95D800; color: #000000;">720.5</td>
<td class="gt_row gt_right">0.1</td>
<td class="gt_row gt_right">271.6</td>
<td class="gt_row gt_right">627.4</td>
<td class="gt_row gt_right">1'074.7</td>
<td class="gt_row gt_right">1'873.8</td></tr>
    <tr><td class="gt_row gt_left">Düngemittel u. Agrochemikalien</td>
<td class="gt_row gt_right">15101030</td>
<td class="gt_row gt_left">Material: Roh- und Grundstoffe</td>
<td class="gt_row gt_right" style="background-color: #94D900; color: #000000;">704.7</td>
<td class="gt_row gt_right">26.1</td>
<td class="gt_row gt_right">129.4</td>
<td class="gt_row gt_right">397.4</td>
<td class="gt_row gt_right">553.3</td>
<td class="gt_row gt_right">4'531.8</td></tr>
    <tr><td class="gt_row gt_left">Hotels, Urlaubsanlagen u. Kreuzfahrtlinien</td>
<td class="gt_row gt_right">25301020</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #92DA00; color: #000000;">689.9</td>
<td class="gt_row gt_right">0.4</td>
<td class="gt_row gt_right">45.6</td>
<td class="gt_row gt_right">259.0</td>
<td class="gt_row gt_right">775.5</td>
<td class="gt_row gt_right">3'300.3</td></tr>
    <tr><td class="gt_row gt_left">Diverse Immobilien-Aktivitäten</td>
<td class="gt_row gt_right">60102010</td>
<td class="gt_row gt_left">Immobilien</td>
<td class="gt_row gt_right" style="background-color: #8CDE00; color: #000000;">618.1</td>
<td class="gt_row gt_right">32.1</td>
<td class="gt_row gt_right">33.5</td>
<td class="gt_row gt_right">108.4</td>
<td class="gt_row gt_right">693.0</td>
<td class="gt_row gt_right">2'223.5</td></tr>
    <tr><td class="gt_row gt_left">Edelmetalle u. Minerale</td>
<td class="gt_row gt_right">15104040</td>
<td class="gt_row gt_left">Material: Roh- und Grundstoffe</td>
<td class="gt_row gt_right" style="background-color: #85E200; color: #000000;">550.3</td>
<td class="gt_row gt_right">369.2</td>
<td class="gt_row gt_right">372.3</td>
<td class="gt_row gt_right">375.4</td>
<td class="gt_row gt_right">640.8</td>
<td class="gt_row gt_right">906.2</td></tr>
    <tr><td class="gt_row gt_left">Papier</td>
<td class="gt_row gt_right">15105020</td>
<td class="gt_row gt_left">Material: Roh- und Grundstoffe</td>
<td class="gt_row gt_right" style="background-color: #7BE700; color: #000000;">460.6</td>
<td class="gt_row gt_right">27.0</td>
<td class="gt_row gt_right">374.1</td>
<td class="gt_row gt_right">427.6</td>
<td class="gt_row gt_right">513.4</td>
<td class="gt_row gt_right">1'287.7</td></tr>
    <tr><td class="gt_row gt_left">Erdöl u. Erdgasförderung</td>
<td class="gt_row gt_right">10101010</td>
<td class="gt_row gt_left">Energie</td>
<td class="gt_row gt_right" style="background-color: #7AE700; color: #000000;">451.1</td>
<td class="gt_row gt_right">71.7</td>
<td class="gt_row gt_right">408.3</td>
<td class="gt_row gt_right">522.6</td>
<td class="gt_row gt_right">588.9</td>
<td class="gt_row gt_right">608.3</td></tr>
    <tr><td class="gt_row gt_left">Produkte</td>
<td class="gt_row gt_right">35101020</td>
<td class="gt_row gt_left">Gesundheitswesen</td>
<td class="gt_row gt_right" style="background-color: #6FEC00; color: #000000;">364.7</td>
<td class="gt_row gt_right">5.9</td>
<td class="gt_row gt_right">12.7</td>
<td class="gt_row gt_right">15.3</td>
<td class="gt_row gt_right">20.1</td>
<td class="gt_row gt_right">9'723.1</td></tr>
    <tr><td class="gt_row gt_left">Gold</td>
<td class="gt_row gt_right">15104030</td>
<td class="gt_row gt_left">Material: Roh- und Grundstoffe</td>
<td class="gt_row gt_right" style="background-color: #6DED00; color: #000000;">349.6</td>
<td class="gt_row gt_right">0.0</td>
<td class="gt_row gt_right">178.2</td>
<td class="gt_row gt_right">286.9</td>
<td class="gt_row gt_right">391.3</td>
<td class="gt_row gt_right">1'620.2</td></tr>
    <tr><td class="gt_row gt_left">Schienenverkehr</td>
<td class="gt_row gt_right">20304010</td>
<td class="gt_row gt_left">Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport</td>
<td class="gt_row gt_right" style="background-color: #6BED00; color: #000000;">339.3</td>
<td class="gt_row gt_right">109.2</td>
<td class="gt_row gt_right">338.3</td>
<td class="gt_row gt_right">372.8</td>
<td class="gt_row gt_right">398.2</td>
<td class="gt_row gt_right">420.0</td></tr>
    <tr><td class="gt_row gt_left">Erdöl u. Erdgas: Ausrüstung und Dienste</td>
<td class="gt_row gt_right">10101020</td>
<td class="gt_row gt_left">Energie</td>
<td class="gt_row gt_right" style="background-color: #69EE00; color: #000000;">326.0</td>
<td class="gt_row gt_right">5.1</td>
<td class="gt_row gt_right">62.9</td>
<td class="gt_row gt_right">85.1</td>
<td class="gt_row gt_right">132.9</td>
<td class="gt_row gt_right">6'466.7</td></tr>
    <tr><td class="gt_row gt_left">Luft- u. Raumfahrt u. Verteidigung</td>
<td class="gt_row gt_right">20101010</td>
<td class="gt_row gt_left">Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport</td>
<td class="gt_row gt_right" style="background-color: #69EE00; color: #000000;">321.7</td>
<td class="gt_row gt_right">8.5</td>
<td class="gt_row gt_right">16.9</td>
<td class="gt_row gt_right">22.5</td>
<td class="gt_row gt_right">27.0</td>
<td class="gt_row gt_right">12'120.8</td></tr>
    <tr><td class="gt_row gt_left">Kupfer</td>
<td class="gt_row gt_right">15104025</td>
<td class="gt_row gt_left">Material: Roh- und Grundstoffe</td>
<td class="gt_row gt_right" style="background-color: #68EE00; color: #000000;">320.7</td>
<td class="gt_row gt_right">227.4</td>
<td class="gt_row gt_right">281.5</td>
<td class="gt_row gt_right">281.8</td>
<td class="gt_row gt_right">311.5</td>
<td class="gt_row gt_right">501.0</td></tr>
    <tr><td class="gt_row gt_left">Elektrische Anlagen [Großanlagen, Turbinen usw.]</td>
<td class="gt_row gt_right">20104020</td>
<td class="gt_row gt_left">Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport</td>
<td class="gt_row gt_right" style="background-color: #63F000; color: #000000;">284.7</td>
<td class="gt_row gt_right">20.5</td>
<td class="gt_row gt_right">26.2</td>
<td class="gt_row gt_right">36.3</td>
<td class="gt_row gt_right">56.0</td>
<td class="gt_row gt_right">1'529.8</td></tr>
    <tr><td class="gt_row gt_left">Silber</td>
<td class="gt_row gt_right">15104045</td>
<td class="gt_row gt_left">Material: Roh- und Grundstoffe</td>
<td class="gt_row gt_right" style="background-color: #60F100; color: #000000;">267.4</td>
<td class="gt_row gt_right">124.3</td>
<td class="gt_row gt_right">254.4</td>
<td class="gt_row gt_right">288.6</td>
<td class="gt_row gt_right">331.8</td>
<td class="gt_row gt_right">338.0</td></tr>
    <tr><td class="gt_row gt_left">Spezialchemikalien</td>
<td class="gt_row gt_right">15101050</td>
<td class="gt_row gt_left">Material: Roh- und Grundstoffe</td>
<td class="gt_row gt_right" style="background-color: #60F100; color: #000000;">266.3</td>
<td class="gt_row gt_right">3.6</td>
<td class="gt_row gt_right">45.2</td>
<td class="gt_row gt_right">132.7</td>
<td class="gt_row gt_right">301.9</td>
<td class="gt_row gt_right">3'042.9</td></tr>
    <tr><td class="gt_row gt_left">Halbleiterausrüstung</td>
<td class="gt_row gt_right">45301010</td>
<td class="gt_row gt_left">Informationstechnologie</td>
<td class="gt_row gt_right" style="background-color: #5EF200; color: #000000;">254.3</td>
<td class="gt_row gt_right">0.1</td>
<td class="gt_row gt_right">13.6</td>
<td class="gt_row gt_right">19.6</td>
<td class="gt_row gt_right">29.9</td>
<td class="gt_row gt_right">5'647.3</td></tr>
    <tr><td class="gt_row gt_left">Straßenverkehr</td>
<td class="gt_row gt_right">20304020</td>
<td class="gt_row gt_left">Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport</td>
<td class="gt_row gt_right" style="background-color: #5DF200; color: #000000;">249.4</td>
<td class="gt_row gt_right">11.9</td>
<td class="gt_row gt_right">158.3</td>
<td class="gt_row gt_right">202.4</td>
<td class="gt_row gt_right">234.7</td>
<td class="gt_row gt_right">1'262.8</td></tr>
    <tr><td class="gt_row gt_left">Erdöl u. Erdgas: Raffinierung und Vermarktung</td>
<td class="gt_row gt_right">10102030</td>
<td class="gt_row gt_left">Energie</td>
<td class="gt_row gt_right" style="background-color: #5CF200; color: #000000;">243.8</td>
<td class="gt_row gt_right">2.9</td>
<td class="gt_row gt_right">109.2</td>
<td class="gt_row gt_right">202.3</td>
<td class="gt_row gt_right">300.6</td>
<td class="gt_row gt_right">793.8</td></tr>
    <tr><td class="gt_row gt_left">Industriekonglomerate</td>
<td class="gt_row gt_right">20105010</td>
<td class="gt_row gt_left">Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport</td>
<td class="gt_row gt_right" style="background-color: #5BF200; color: #000000;">238.9</td>
<td class="gt_row gt_right">3.1</td>
<td class="gt_row gt_right">22.1</td>
<td class="gt_row gt_right">47.6</td>
<td class="gt_row gt_right">272.5</td>
<td class="gt_row gt_right">989.5</td></tr>
    <tr><td class="gt_row gt_left">Landwirtschaftliche Produkte</td>
<td class="gt_row gt_right">30202010</td>
<td class="gt_row gt_left">Basiskonsumgüter</td>
<td class="gt_row gt_right" style="background-color: #5AF300; color: #000000;">234.4</td>
<td class="gt_row gt_right">41.4</td>
<td class="gt_row gt_right">48.1</td>
<td class="gt_row gt_right">127.3</td>
<td class="gt_row gt_right">413.2</td>
<td class="gt_row gt_right">580.0</td></tr>
    <tr><td class="gt_row gt_left">Papierverpackungen</td>
<td class="gt_row gt_right">15103020</td>
<td class="gt_row gt_left">Material: Roh- und Grundstoffe</td>
<td class="gt_row gt_right" style="background-color: #5AF300; color: #000000;">232.1</td>
<td class="gt_row gt_right">14.6</td>
<td class="gt_row gt_right">71.3</td>
<td class="gt_row gt_right">178.5</td>
<td class="gt_row gt_right">344.8</td>
<td class="gt_row gt_right">640.4</td></tr>
    <tr><td class="gt_row gt_left">Wasserversorger</td>
<td class="gt_row gt_right">55104010</td>
<td class="gt_row gt_left">Versorgungsunternehmen</td>
<td class="gt_row gt_right" style="background-color: #5AF300; color: #000000;">231.0</td>
<td class="gt_row gt_right">21.6</td>
<td class="gt_row gt_right">131.6</td>
<td class="gt_row gt_right">202.8</td>
<td class="gt_row gt_right">248.8</td>
<td class="gt_row gt_right">864.7</td></tr>
    <tr><td class="gt_row gt_left">Biotechnologie</td>
<td class="gt_row gt_right">35201010</td>
<td class="gt_row gt_left">Gesundheitswesen</td>
<td class="gt_row gt_right" style="background-color: #59F300; color: #000000;">225.4</td>
<td class="gt_row gt_right">0.0</td>
<td class="gt_row gt_right">13.2</td>
<td class="gt_row gt_right">20.6</td>
<td class="gt_row gt_right">48.2</td>
<td class="gt_row gt_right">19'660.0</td></tr>
    <tr><td class="gt_row gt_left">Forstprodukte</td>
<td class="gt_row gt_right">15105010</td>
<td class="gt_row gt_left">Material: Roh- und Grundstoffe</td>
<td class="gt_row gt_right" style="background-color: #55F400; color: #000000;">205.6</td>
<td class="gt_row gt_right">15.9</td>
<td class="gt_row gt_right">135.9</td>
<td class="gt_row gt_right">228.6</td>
<td class="gt_row gt_right">298.3</td>
<td class="gt_row gt_right">349.4</td></tr>
    <tr><td class="gt_row gt_left">NA</td>
<td class="gt_row gt_right">NA</td>
<td class="gt_row gt_left">NA</td>
<td class="gt_row gt_right" style="background-color: #52F500; color: #000000;">192.8</td>
<td class="gt_row gt_right">0.7</td>
<td class="gt_row gt_right">8.0</td>
<td class="gt_row gt_right">19.3</td>
<td class="gt_row gt_right">67.7</td>
<td class="gt_row gt_right">6'216.6</td></tr>
    <tr><td class="gt_row gt_left">Spezialisierte Immobiliengesellschaften</td>
<td class="gt_row gt_right">60101080</td>
<td class="gt_row gt_left">Immobilien</td>
<td class="gt_row gt_right" style="background-color: #51F500; color: #000000;">188.9</td>
<td class="gt_row gt_right">3.8</td>
<td class="gt_row gt_right">61.9</td>
<td class="gt_row gt_right">90.3</td>
<td class="gt_row gt_right">218.2</td>
<td class="gt_row gt_right">1'000.9</td></tr>
    <tr><td class="gt_row gt_left">Gasversorger</td>
<td class="gt_row gt_right">55102010</td>
<td class="gt_row gt_left">Versorgungsunternehmen</td>
<td class="gt_row gt_right" style="background-color: #50F500; color: #000000;">183.8</td>
<td class="gt_row gt_right">26.4</td>
<td class="gt_row gt_right">92.1</td>
<td class="gt_row gt_right">135.1</td>
<td class="gt_row gt_right">170.6</td>
<td class="gt_row gt_right">646.4</td></tr>
    <tr><td class="gt_row gt_left">Reifen u. Gummi</td>
<td class="gt_row gt_right">25101020</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #50F500; color: #000000;">182.1</td>
<td class="gt_row gt_right">182.1</td>
<td class="gt_row gt_right">182.1</td>
<td class="gt_row gt_right">182.1</td>
<td class="gt_row gt_right">182.1</td>
<td class="gt_row gt_right">182.1</td></tr>
    <tr><td class="gt_row gt_left">Hersteller von Kfz</td>
<td class="gt_row gt_right">25102010</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #4AF700; color: #000000;">154.9</td>
<td class="gt_row gt_right">5.1</td>
<td class="gt_row gt_right">8.1</td>
<td class="gt_row gt_right">19.0</td>
<td class="gt_row gt_right">41.1</td>
<td class="gt_row gt_right">1'005.0</td></tr>
    <tr><td class="gt_row gt_left">Motorradhersteller</td>
<td class="gt_row gt_right">25102020</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #4AF700; color: #000000;">154.0</td>
<td class="gt_row gt_right">3.9</td>
<td class="gt_row gt_right">16.9</td>
<td class="gt_row gt_right">30.0</td>
<td class="gt_row gt_right">229.1</td>
<td class="gt_row gt_right">428.2</td></tr>
    <tr><td class="gt_row gt_left">Büro-Immobilien</td>
<td class="gt_row gt_right">60101040</td>
<td class="gt_row gt_left">Immobilien</td>
<td class="gt_row gt_right" style="background-color: #49F700; color: #000000;">151.2</td>
<td class="gt_row gt_right">0.3</td>
<td class="gt_row gt_right">44.1</td>
<td class="gt_row gt_right">77.7</td>
<td class="gt_row gt_right">169.1</td>
<td class="gt_row gt_right">1'054.5</td></tr>
    <tr><td class="gt_row gt_left">Hotels u. Urlaubsanlagen</td>
<td class="gt_row gt_right">60101030</td>
<td class="gt_row gt_left">Immobilien</td>
<td class="gt_row gt_right" style="background-color: #46F800; color: #000000;">139.8</td>
<td class="gt_row gt_right">16.4</td>
<td class="gt_row gt_right">61.9</td>
<td class="gt_row gt_right">129.3</td>
<td class="gt_row gt_right">219.0</td>
<td class="gt_row gt_right">307.0</td></tr>
    <tr><td class="gt_row gt_left">Alternative Carrier [Hochgeschwingkeitsnetze]</td>
<td class="gt_row gt_right">50101010</td>
<td class="gt_row gt_left">Kommunikationsdienstleistungen</td>
<td class="gt_row gt_right" style="background-color: #42F800; color: #000000;">124.5</td>
<td class="gt_row gt_right">9.6</td>
<td class="gt_row gt_right">21.5</td>
<td class="gt_row gt_right">30.7</td>
<td class="gt_row gt_right">73.9</td>
<td class="gt_row gt_right">723.8</td></tr>
    <tr><td class="gt_row gt_left">Hypotheken-REITs</td>
<td class="gt_row gt_right">40204010</td>
<td class="gt_row gt_left">Finanzen</td>
<td class="gt_row gt_right" style="background-color: #41F900; color: #000000;">117.8</td>
<td class="gt_row gt_right">0.0</td>
<td class="gt_row gt_right">21.7</td>
<td class="gt_row gt_right">74.1</td>
<td class="gt_row gt_right">118.3</td>
<td class="gt_row gt_right">638.0</td></tr>
    <tr><td class="gt_row gt_left">Pharmazeutika</td>
<td class="gt_row gt_right">35202010</td>
<td class="gt_row gt_left">Gesundheitswesen</td>
<td class="gt_row gt_right" style="background-color: #3FF900; color: #000000;">111.2</td>
<td class="gt_row gt_right">0.2</td>
<td class="gt_row gt_right">16.8</td>
<td class="gt_row gt_right">27.5</td>
<td class="gt_row gt_right">44.7</td>
<td class="gt_row gt_right">1'899.1</td></tr>
    <tr><td class="gt_row gt_left">Einzelhandel: Automobilindustrie</td>
<td class="gt_row gt_right">25504050</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #3BFA00; color: #000000;">97.1</td>
<td class="gt_row gt_right">2.5</td>
<td class="gt_row gt_right">6.5</td>
<td class="gt_row gt_right">8.1</td>
<td class="gt_row gt_right">18.3</td>
<td class="gt_row gt_right">720.4</td></tr>
    <tr><td class="gt_row gt_left">Halbleiter</td>
<td class="gt_row gt_right">45301020</td>
<td class="gt_row gt_left">Informationstechnologie</td>
<td class="gt_row gt_right" style="background-color: #3AFA00; color: #000000;">93.7</td>
<td class="gt_row gt_right">0.0</td>
<td class="gt_row gt_right">22.0</td>
<td class="gt_row gt_right">64.0</td>
<td class="gt_row gt_right">82.4</td>
<td class="gt_row gt_right">824.5</td></tr>
    <tr><td class="gt_row gt_left">Baumaterialien</td>
<td class="gt_row gt_right">20102010</td>
<td class="gt_row gt_left">Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport</td>
<td class="gt_row gt_right" style="background-color: #39FA00; color: #000000;">93.3</td>
<td class="gt_row gt_right">10.3</td>
<td class="gt_row gt_right">37.1</td>
<td class="gt_row gt_right">44.9</td>
<td class="gt_row gt_right">83.8</td>
<td class="gt_row gt_right">534.4</td></tr>
    <tr><td class="gt_row gt_left">Immobilienbetreiber [Gebäudemanagement]</td>
<td class="gt_row gt_right">60102020</td>
<td class="gt_row gt_left">Immobilien</td>
<td class="gt_row gt_right" style="background-color: #39FA00; color: #000000;">92.4</td>
<td class="gt_row gt_right">19.3</td>
<td class="gt_row gt_right">35.3</td>
<td class="gt_row gt_right">66.0</td>
<td class="gt_row gt_right">98.6</td>
<td class="gt_row gt_right">268.5</td></tr>
    <tr><td class="gt_row gt_left">Erneuerbare Energien</td>
<td class="gt_row gt_right">55105020</td>
<td class="gt_row gt_left">Versorgungsunternehmen</td>
<td class="gt_row gt_right" style="background-color: #38FA00; color: #000000;">90.1</td>
<td class="gt_row gt_right">5.3</td>
<td class="gt_row gt_right">56.8</td>
<td class="gt_row gt_right">108.4</td>
<td class="gt_row gt_right">132.5</td>
<td class="gt_row gt_right">156.7</td></tr>
    <tr><td class="gt_row gt_left">Immobilien f. d. Gesundheitswesen</td>
<td class="gt_row gt_right">60101050</td>
<td class="gt_row gt_left">Immobilien</td>
<td class="gt_row gt_right" style="background-color: #37FB00; color: #000000;">85.1</td>
<td class="gt_row gt_right">45.8</td>
<td class="gt_row gt_right">55.5</td>
<td class="gt_row gt_right">63.4</td>
<td class="gt_row gt_right">97.3</td>
<td class="gt_row gt_right">224.6</td></tr>
    <tr><td class="gt_row gt_left">Computerkomponenten</td>
<td class="gt_row gt_right">45203015</td>
<td class="gt_row gt_left">Informationstechnologie</td>
<td class="gt_row gt_right" style="background-color: #36FB00; color: #000000;">83.4</td>
<td class="gt_row gt_right">11.7</td>
<td class="gt_row gt_right">30.6</td>
<td class="gt_row gt_right">49.3</td>
<td class="gt_row gt_right">85.1</td>
<td class="gt_row gt_right">328.2</td></tr>
    <tr><td class="gt_row gt_left">Bau- u. Ingenieurswesen</td>
<td class="gt_row gt_right">20103010</td>
<td class="gt_row gt_left">Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport</td>
<td class="gt_row gt_right" style="background-color: #36FB00; color: #000000;">82.5</td>
<td class="gt_row gt_right">3.0</td>
<td class="gt_row gt_right">14.0</td>
<td class="gt_row gt_right">21.6</td>
<td class="gt_row gt_right">27.6</td>
<td class="gt_row gt_right">1'088.6</td></tr>
    <tr><td class="gt_row gt_left">Flughafendienste</td>
<td class="gt_row gt_right">20305010</td>
<td class="gt_row gt_left">Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport</td>
<td class="gt_row gt_right" style="background-color: #36FB00; color: #000000;">82.4</td>
<td class="gt_row gt_right">62.6</td>
<td class="gt_row gt_right">72.9</td>
<td class="gt_row gt_right">83.1</td>
<td class="gt_row gt_right">92.3</td>
<td class="gt_row gt_right">101.5</td></tr>
    <tr><td class="gt_row gt_left">Luftfracht u. -logistik</td>
<td class="gt_row gt_right">20301010</td>
<td class="gt_row gt_left">Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport</td>
<td class="gt_row gt_right" style="background-color: #35FB00; color: #000000;">79.4</td>
<td class="gt_row gt_right">1.5</td>
<td class="gt_row gt_right">7.8</td>
<td class="gt_row gt_right">30.4</td>
<td class="gt_row gt_right">156.5</td>
<td class="gt_row gt_right">258.1</td></tr>
    <tr><td class="gt_row gt_left">Freizeiteinrichtungen</td>
<td class="gt_row gt_right">25301030</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #33FB00; color: #000000;">74.0</td>
<td class="gt_row gt_right">29.2</td>
<td class="gt_row gt_right">33.9</td>
<td class="gt_row gt_right">53.5</td>
<td class="gt_row gt_right">113.6</td>
<td class="gt_row gt_right">133.9</td></tr>
    <tr><td class="gt_row gt_left">Privater Wohnungsbau</td>
<td class="gt_row gt_right">60101060</td>
<td class="gt_row gt_left">Immobilien</td>
<td class="gt_row gt_right" style="background-color: #33FB00; color: #000000;">73.3</td>
<td class="gt_row gt_right">4.0</td>
<td class="gt_row gt_right">57.0</td>
<td class="gt_row gt_right">66.7</td>
<td class="gt_row gt_right">104.7</td>
<td class="gt_row gt_right">128.9</td></tr>
    <tr><td class="gt_row gt_left">Abgepackte Produkte u. Fleisch</td>
<td class="gt_row gt_right">30202030</td>
<td class="gt_row gt_left">Basiskonsumgüter</td>
<td class="gt_row gt_right" style="background-color: #32FB00; color: #000000;">71.9</td>
<td class="gt_row gt_right">18.9</td>
<td class="gt_row gt_right">51.6</td>
<td class="gt_row gt_right">61.4</td>
<td class="gt_row gt_right">84.4</td>
<td class="gt_row gt_right">299.7</td></tr>
    <tr><td class="gt_row gt_left">Mobilfunkanbieter</td>
<td class="gt_row gt_right">50102010</td>
<td class="gt_row gt_left">Kommunikationsdienstleistungen</td>
<td class="gt_row gt_right" style="background-color: #32FB00; color: #000000;">70.9</td>
<td class="gt_row gt_right">0.1</td>
<td class="gt_row gt_right">35.1</td>
<td class="gt_row gt_right">57.9</td>
<td class="gt_row gt_right">102.1</td>
<td class="gt_row gt_right">165.4</td></tr>
    <tr><td class="gt_row gt_left">Brauereien</td>
<td class="gt_row gt_right">30201010</td>
<td class="gt_row gt_left">Basiskonsumgüter</td>
<td class="gt_row gt_right" style="background-color: #31FB00; color: #000000;">69.0</td>
<td class="gt_row gt_right">11.7</td>
<td class="gt_row gt_right">47.9</td>
<td class="gt_row gt_right">80.2</td>
<td class="gt_row gt_right">101.3</td>
<td class="gt_row gt_right">103.8</td></tr>
    <tr><td class="gt_row gt_left">Forschungs- u. Beratungsdienstleistungen</td>
<td class="gt_row gt_right">20202020</td>
<td class="gt_row gt_left">Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport</td>
<td class="gt_row gt_right" style="background-color: #30FB00; color: #000000;">67.4</td>
<td class="gt_row gt_right">1.7</td>
<td class="gt_row gt_right">3.8</td>
<td class="gt_row gt_right">4.6</td>
<td class="gt_row gt_right">8.6</td>
<td class="gt_row gt_right">1'995.2</td></tr>
    <tr><td class="gt_row gt_left">Casinos u. Glücksspiel</td>
<td class="gt_row gt_right">25301010</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #30FC00; color: #000000;">66.1</td>
<td class="gt_row gt_right">3.1</td>
<td class="gt_row gt_right">42.9</td>
<td class="gt_row gt_right">51.8</td>
<td class="gt_row gt_right">71.5</td>
<td class="gt_row gt_right">197.0</td></tr>
    <tr><td class="gt_row gt_left">Kfz-Teile u. -Ausrüstung</td>
<td class="gt_row gt_right">25101010</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #2EFC00; color: #000000;">61.5</td>
<td class="gt_row gt_right">12.6</td>
<td class="gt_row gt_right">25.1</td>
<td class="gt_row gt_right">46.4</td>
<td class="gt_row gt_right">70.5</td>
<td class="gt_row gt_right">434.9</td></tr>
    <tr><td class="gt_row gt_left">Industrie-Immobiliengesellschaften</td>
<td class="gt_row gt_right">60101020</td>
<td class="gt_row gt_left">Immobilien</td>
<td class="gt_row gt_right" style="background-color: #2EFC00; color: #000000;">60.0</td>
<td class="gt_row gt_right">1.4</td>
<td class="gt_row gt_right">36.2</td>
<td class="gt_row gt_right">39.1</td>
<td class="gt_row gt_right">56.8</td>
<td class="gt_row gt_right">184.6</td></tr>
    <tr><td class="gt_row gt_left">Integrierte Kommunikationsdienste</td>
<td class="gt_row gt_right">50101020</td>
<td class="gt_row gt_left">Kommunikationsdienstleistungen</td>
<td class="gt_row gt_right" style="background-color: #2DFC00; color: #000000;">58.5</td>
<td class="gt_row gt_right">6.2</td>
<td class="gt_row gt_right">25.6</td>
<td class="gt_row gt_right">32.9</td>
<td class="gt_row gt_right">57.4</td>
<td class="gt_row gt_right">323.2</td></tr>
    <tr><td class="gt_row gt_left">Diverse unterstützende Dienstleistungen</td>
<td class="gt_row gt_right">20201070</td>
<td class="gt_row gt_left">Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport</td>
<td class="gt_row gt_right" style="background-color: #2CFC00; color: #000000;">55.4</td>
<td class="gt_row gt_right">4.3</td>
<td class="gt_row gt_right">11.1</td>
<td class="gt_row gt_right">21.0</td>
<td class="gt_row gt_right">46.5</td>
<td class="gt_row gt_right">467.6</td></tr>
    <tr><td class="gt_row gt_left">Verschiedene [sonstige] Immobiliengesellschaften</td>
<td class="gt_row gt_right">60101010</td>
<td class="gt_row gt_left">Immobilien</td>
<td class="gt_row gt_right" style="background-color: #2BFC00; color: #000000;">54.0</td>
<td class="gt_row gt_right">26.2</td>
<td class="gt_row gt_right">36.0</td>
<td class="gt_row gt_right">42.4</td>
<td class="gt_row gt_right">59.2</td>
<td class="gt_row gt_right">152.7</td></tr>
    <tr><td class="gt_row gt_left">Restaurants</td>
<td class="gt_row gt_right">25301040</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #2BFC00; color: #000000;">53.4</td>
<td class="gt_row gt_right">4.4</td>
<td class="gt_row gt_right">31.2</td>
<td class="gt_row gt_right">43.9</td>
<td class="gt_row gt_right">58.5</td>
<td class="gt_row gt_right">243.2</td></tr>
    <tr><td class="gt_row gt_left">Erfrischungsgetränke [nicht-alkoholische Getränke]</td>
<td class="gt_row gt_right">30201030</td>
<td class="gt_row gt_left">Basiskonsumgüter</td>
<td class="gt_row gt_right" style="background-color: #2AFC00; color: #000000;">52.3</td>
<td class="gt_row gt_right">0.4</td>
<td class="gt_row gt_right">37.5</td>
<td class="gt_row gt_right">44.9</td>
<td class="gt_row gt_right">55.9</td>
<td class="gt_row gt_right">158.7</td></tr>
    <tr><td class="gt_row gt_left">Dienstleistungen f. d. Computer-Fertigung</td>
<td class="gt_row gt_right">45203020</td>
<td class="gt_row gt_left">Informationstechnologie</td>
<td class="gt_row gt_right" style="background-color: #2AFC00; color: #000000;">52.2</td>
<td class="gt_row gt_right">5.3</td>
<td class="gt_row gt_right">27.6</td>
<td class="gt_row gt_right">51.5</td>
<td class="gt_row gt_right">73.5</td>
<td class="gt_row gt_right">94.7</td></tr>
    <tr><td class="gt_row gt_left">Einzelhandelsimmobilien</td>
<td class="gt_row gt_right">60101070</td>
<td class="gt_row gt_left">Immobilien</td>
<td class="gt_row gt_right" style="background-color: #29FC00; color: #000000;">50.6</td>
<td class="gt_row gt_right">0.1</td>
<td class="gt_row gt_right">33.4</td>
<td class="gt_row gt_right">44.3</td>
<td class="gt_row gt_right">55.9</td>
<td class="gt_row gt_right">197.3</td></tr>
    <tr><td class="gt_row gt_left">Systemsoftware</td>
<td class="gt_row gt_right">45103020</td>
<td class="gt_row gt_left">Informationstechnologie</td>
<td class="gt_row gt_right" style="background-color: #27FD00; color: #000000;">45.8</td>
<td class="gt_row gt_right">2.1</td>
<td class="gt_row gt_right">4.0</td>
<td class="gt_row gt_right">4.7</td>
<td class="gt_row gt_right">5.9</td>
<td class="gt_row gt_right">1'356.7</td></tr>
    <tr><td class="gt_row gt_left">Haushaltsartikel</td>
<td class="gt_row gt_right">30301010</td>
<td class="gt_row gt_left">Basiskonsumgüter</td>
<td class="gt_row gt_right" style="background-color: #26FD00; color: #000000;">43.7</td>
<td class="gt_row gt_right">0.0</td>
<td class="gt_row gt_right">25.2</td>
<td class="gt_row gt_right">31.9</td>
<td class="gt_row gt_right">50.6</td>
<td class="gt_row gt_right">195.5</td></tr>
    <tr><td class="gt_row gt_left">Multi-Sektor-Unternehmen [Finanzholdings]</td>
<td class="gt_row gt_right">40201030</td>
<td class="gt_row gt_left">Finanzen</td>
<td class="gt_row gt_right" style="background-color: #25FD00; color: #000000;">40.9</td>
<td class="gt_row gt_right">15.5</td>
<td class="gt_row gt_right">28.3</td>
<td class="gt_row gt_right">41.0</td>
<td class="gt_row gt_right">53.6</td>
<td class="gt_row gt_right">66.1</td></tr>
    <tr><td class="gt_row gt_left">Spezialisierte Finanzdienstleistungen</td>
<td class="gt_row gt_right">40201040</td>
<td class="gt_row gt_left">Finanzen</td>
<td class="gt_row gt_right" style="background-color: #25FD00; color: #000000;">40.9</td>
<td class="gt_row gt_right">1.4</td>
<td class="gt_row gt_right">2.2</td>
<td class="gt_row gt_right">3.1</td>
<td class="gt_row gt_right">3.7</td>
<td class="gt_row gt_right">194.0</td></tr>
    <tr><td class="gt_row gt_left">Lebensmitteleinzelhandel</td>
<td class="gt_row gt_right">30101030</td>
<td class="gt_row gt_left">Basiskonsumgüter</td>
<td class="gt_row gt_right" style="background-color: #23FD00; color: #000000;">36.8</td>
<td class="gt_row gt_right">0.5</td>
<td class="gt_row gt_right">20.9</td>
<td class="gt_row gt_right">31.6</td>
<td class="gt_row gt_right">45.5</td>
<td class="gt_row gt_right">89.3</td></tr>
    <tr><td class="gt_row gt_left">Brennereien u. Winzer</td>
<td class="gt_row gt_right">30201020</td>
<td class="gt_row gt_left">Basiskonsumgüter</td>
<td class="gt_row gt_right" style="background-color: #23FD00; color: #000000;">36.7</td>
<td class="gt_row gt_right">30.1</td>
<td class="gt_row gt_right">31.0</td>
<td class="gt_row gt_right">35.9</td>
<td class="gt_row gt_right">41.6</td>
<td class="gt_row gt_right">45.2</td></tr>
    <tr><td class="gt_row gt_left">Industriemaschinen</td>
<td class="gt_row gt_right">20106020</td>
<td class="gt_row gt_left">Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport</td>
<td class="gt_row gt_right" style="background-color: #21FD00; color: #000000;">34.3</td>
<td class="gt_row gt_right">0.3</td>
<td class="gt_row gt_right">20.4</td>
<td class="gt_row gt_right">27.0</td>
<td class="gt_row gt_right">36.9</td>
<td class="gt_row gt_right">107.0</td></tr>
    <tr><td class="gt_row gt_left">Kaufhäuser</td>
<td class="gt_row gt_right">25503010</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #1FFD00; color: #000000;">31.2</td>
<td class="gt_row gt_right">19.0</td>
<td class="gt_row gt_right">20.3</td>
<td class="gt_row gt_right">23.5</td>
<td class="gt_row gt_right">35.6</td>
<td class="gt_row gt_right">62.3</td></tr>
    <tr><td class="gt_row gt_left">Kommunikationsgeräte</td>
<td class="gt_row gt_right">45201020</td>
<td class="gt_row gt_left">Informationstechnologie</td>
<td class="gt_row gt_right" style="background-color: #1FFD00; color: #000000;">31.2</td>
<td class="gt_row gt_right">1.7</td>
<td class="gt_row gt_right">9.8</td>
<td class="gt_row gt_right">13.2</td>
<td class="gt_row gt_right">18.0</td>
<td class="gt_row gt_right">555.3</td></tr>
    <tr><td class="gt_row gt_left">Immobilienentwicklung</td>
<td class="gt_row gt_right">60102030</td>
<td class="gt_row gt_left">Immobilien</td>
<td class="gt_row gt_right" style="background-color: #1FFD00; color: #000000;">30.9</td>
<td class="gt_row gt_right">9.2</td>
<td class="gt_row gt_right">17.4</td>
<td class="gt_row gt_right">21.9</td>
<td class="gt_row gt_right">29.8</td>
<td class="gt_row gt_right">84.4</td></tr>
    <tr><td class="gt_row gt_left">Haushaltswaren u. Sondergeräte [Hausrat]</td>
<td class="gt_row gt_right">25201050</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #1FFD00; color: #000000;">30.2</td>
<td class="gt_row gt_right">15.8</td>
<td class="gt_row gt_right">25.5</td>
<td class="gt_row gt_right">35.1</td>
<td class="gt_row gt_right">37.4</td>
<td class="gt_row gt_right">39.7</td></tr>
    <tr><td class="gt_row gt_left">Computer-Handel</td>
<td class="gt_row gt_right">45203030</td>
<td class="gt_row gt_left">Informationstechnologie</td>
<td class="gt_row gt_right" style="background-color: #1EFD00; color: #000000;">28.9</td>
<td class="gt_row gt_right">3.3</td>
<td class="gt_row gt_right">9.2</td>
<td class="gt_row gt_right">11.0</td>
<td class="gt_row gt_right">11.7</td>
<td class="gt_row gt_right">159.4</td></tr>
    <tr><td class="gt_row gt_left">Computer-Ausrüstung u. Geräte</td>
<td class="gt_row gt_right">45203010</td>
<td class="gt_row gt_left">Informationstechnologie</td>
<td class="gt_row gt_right" style="background-color: #1DFE00; color: #000000;">27.6</td>
<td class="gt_row gt_right">1.1</td>
<td class="gt_row gt_right">10.0</td>
<td class="gt_row gt_right">21.4</td>
<td class="gt_row gt_right">27.8</td>
<td class="gt_row gt_right">210.3</td></tr>
    <tr><td class="gt_row gt_left">Gesundheitswesen: Einrichtungen</td>
<td class="gt_row gt_right">35102020</td>
<td class="gt_row gt_left">Gesundheitswesen</td>
<td class="gt_row gt_right" style="background-color: #1DFE00; color: #000000;">27.4</td>
<td class="gt_row gt_right">17.5</td>
<td class="gt_row gt_right">21.7</td>
<td class="gt_row gt_right">23.6</td>
<td class="gt_row gt_right">32.6</td>
<td class="gt_row gt_right">42.9</td></tr>
    <tr><td class="gt_row gt_left">[Wohn-] Möbel u. Innenausstattung</td>
<td class="gt_row gt_right">25201020</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #1DFE00; color: #000000;">27.0</td>
<td class="gt_row gt_right">8.4</td>
<td class="gt_row gt_right">11.3</td>
<td class="gt_row gt_right">18.7</td>
<td class="gt_row gt_right">29.1</td>
<td class="gt_row gt_right">98.0</td></tr>
    <tr><td class="gt_row gt_left">[Sonstige] spezialisierte Verbraucherdienstleistungen</td>
<td class="gt_row gt_right">25302020</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #1CFE00; color: #000000;">26.5</td>
<td class="gt_row gt_right">9.4</td>
<td class="gt_row gt_right">14.6</td>
<td class="gt_row gt_right">23.5</td>
<td class="gt_row gt_right">33.3</td>
<td class="gt_row gt_right">58.9</td></tr>
    <tr><td class="gt_row gt_left">Bürodienstleistungen u. -bedarf</td>
<td class="gt_row gt_right">20201060</td>
<td class="gt_row gt_left">Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport</td>
<td class="gt_row gt_right" style="background-color: #1BFE00; color: #000000;">25.2</td>
<td class="gt_row gt_right">4.9</td>
<td class="gt_row gt_right">11.6</td>
<td class="gt_row gt_right">13.7</td>
<td class="gt_row gt_right">25.5</td>
<td class="gt_row gt_right">125.4</td></tr>
    <tr><td class="gt_row gt_left">Lebensmittelvertriebsunternehmen [Lebensmittel-Großhandel]</td>
<td class="gt_row gt_right">30101020</td>
<td class="gt_row gt_left">Basiskonsumgüter</td>
<td class="gt_row gt_right" style="background-color: #1BFE00; color: #000000;">23.8</td>
<td class="gt_row gt_right">16.5</td>
<td class="gt_row gt_right">22.3</td>
<td class="gt_row gt_right">23.1</td>
<td class="gt_row gt_right">24.7</td>
<td class="gt_row gt_right">33.4</td></tr>
    <tr><td class="gt_row gt_left">Sicherheitsdienste u. Alarmdienstleistungen</td>
<td class="gt_row gt_right">20201080</td>
<td class="gt_row gt_left">Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport</td>
<td class="gt_row gt_right" style="background-color: #1AFE00; color: #000000;">23.7</td>
<td class="gt_row gt_right">0.2</td>
<td class="gt_row gt_right">5.9</td>
<td class="gt_row gt_right">14.6</td>
<td class="gt_row gt_right">17.8</td>
<td class="gt_row gt_right">79.9</td></tr>
    <tr><td class="gt_row gt_left">Körperpflegeprodukte</td>
<td class="gt_row gt_right">30302010</td>
<td class="gt_row gt_left">Basiskonsumgüter</td>
<td class="gt_row gt_right" style="background-color: #1AFE00; color: #000000;">23.1</td>
<td class="gt_row gt_right">0.3</td>
<td class="gt_row gt_right">9.8</td>
<td class="gt_row gt_right">17.4</td>
<td class="gt_row gt_right">31.9</td>
<td class="gt_row gt_right">70.5</td></tr>
    <tr><td class="gt_row gt_left">Handels- u. Vertriebsunternehmen</td>
<td class="gt_row gt_right">20107010</td>
<td class="gt_row gt_left">Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport</td>
<td class="gt_row gt_right" style="background-color: #1AFE00; color: #000000;">22.6</td>
<td class="gt_row gt_right">0.3</td>
<td class="gt_row gt_right">7.9</td>
<td class="gt_row gt_right">18.9</td>
<td class="gt_row gt_right">23.8</td>
<td class="gt_row gt_right">104.3</td></tr>
    <tr><td class="gt_row gt_left">Bekleidung, Accessoires u. Luxuswaren</td>
<td class="gt_row gt_right">25203010</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #19FE00; color: #000000;">22.3</td>
<td class="gt_row gt_right">3.7</td>
<td class="gt_row gt_right">11.1</td>
<td class="gt_row gt_right">14.6</td>
<td class="gt_row gt_right">20.1</td>
<td class="gt_row gt_right">145.8</td></tr>
    <tr><td class="gt_row gt_left">Biowissenschaften: Hilfsmittel u. Dienstleistungen</td>
<td class="gt_row gt_right">35203010</td>
<td class="gt_row gt_left">Gesundheitswesen</td>
<td class="gt_row gt_right" style="background-color: #19FE00; color: #000000;">21.5</td>
<td class="gt_row gt_right">2.8</td>
<td class="gt_row gt_right">10.7</td>
<td class="gt_row gt_right">13.8</td>
<td class="gt_row gt_right">21.3</td>
<td class="gt_row gt_right">180.0</td></tr>
    <tr><td class="gt_row gt_left">Gesundheitswesen: Technologie</td>
<td class="gt_row gt_right">35103010</td>
<td class="gt_row gt_left">Gesundheitswesen</td>
<td class="gt_row gt_right" style="background-color: #19FE00; color: #000000;">21.5</td>
<td class="gt_row gt_right">2.9</td>
<td class="gt_row gt_right">5.1</td>
<td class="gt_row gt_right">10.4</td>
<td class="gt_row gt_right">14.9</td>
<td class="gt_row gt_right">274.4</td></tr>
    <tr><td class="gt_row gt_left">Ausstattung</td>
<td class="gt_row gt_right">35101010</td>
<td class="gt_row gt_left">Gesundheitswesen</td>
<td class="gt_row gt_right" style="background-color: #19FE00; color: #000000;">21.4</td>
<td class="gt_row gt_right">0.1</td>
<td class="gt_row gt_right">10.5</td>
<td class="gt_row gt_right">14.0</td>
<td class="gt_row gt_right">19.8</td>
<td class="gt_row gt_right">241.2</td></tr>
    <tr><td class="gt_row gt_left">Landwirtschaftliche Maschinen u. Nutzfahrzeuge</td>
<td class="gt_row gt_right">20106015</td>
<td class="gt_row gt_left">Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport</td>
<td class="gt_row gt_right" style="background-color: #19FE00; color: #000000;">21.3</td>
<td class="gt_row gt_right">6.4</td>
<td class="gt_row gt_right">14.9</td>
<td class="gt_row gt_right">17.4</td>
<td class="gt_row gt_right">20.9</td>
<td class="gt_row gt_right">53.3</td></tr>
    <tr><td class="gt_row gt_left">Tabak</td>
<td class="gt_row gt_right">30203010</td>
<td class="gt_row gt_left">Basiskonsumgüter</td>
<td class="gt_row gt_right" style="background-color: #18FE00; color: #000000;">20.3</td>
<td class="gt_row gt_right">2.1</td>
<td class="gt_row gt_right">11.5</td>
<td class="gt_row gt_right">14.8</td>
<td class="gt_row gt_right">17.9</td>
<td class="gt_row gt_right">82.1</td></tr>
    <tr><td class="gt_row gt_left">Gesundheitswesen: Dienstleistungen</td>
<td class="gt_row gt_right">35102015</td>
<td class="gt_row gt_left">Gesundheitswesen</td>
<td class="gt_row gt_right" style="background-color: #17FE00; color: #000000;">18.8</td>
<td class="gt_row gt_right">0.7</td>
<td class="gt_row gt_right">14.4</td>
<td class="gt_row gt_right">18.3</td>
<td class="gt_row gt_right">25.1</td>
<td class="gt_row gt_right">52.3</td></tr>
    <tr><td class="gt_row gt_left">Kabel- u. Satellitenübertragung</td>
<td class="gt_row gt_right">50201030</td>
<td class="gt_row gt_left">Kommunikationsdienstleistungen</td>
<td class="gt_row gt_right" style="background-color: #17FE00; color: #000000;">18.6</td>
<td class="gt_row gt_right">5.3</td>
<td class="gt_row gt_right">8.0</td>
<td class="gt_row gt_right">18.5</td>
<td class="gt_row gt_right">23.7</td>
<td class="gt_row gt_right">45.3</td></tr>
    <tr><td class="gt_row gt_left">Immobiliendienstleistungen [Makler usw.]</td>
<td class="gt_row gt_right">60102040</td>
<td class="gt_row gt_left">Immobilien</td>
<td class="gt_row gt_right" style="background-color: #15FE00; color: #000000;">17.1</td>
<td class="gt_row gt_right">0.3</td>
<td class="gt_row gt_right">3.9</td>
<td class="gt_row gt_right">4.8</td>
<td class="gt_row gt_right">6.0</td>
<td class="gt_row gt_right">142.4</td></tr>
    <tr><td class="gt_row gt_left">Mode</td>
<td class="gt_row gt_right">25504010</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #15FE00; color: #000000;">17.1</td>
<td class="gt_row gt_right">6.7</td>
<td class="gt_row gt_right">8.3</td>
<td class="gt_row gt_right">10.9</td>
<td class="gt_row gt_right">20.1</td>
<td class="gt_row gt_right">57.0</td></tr>
    <tr><td class="gt_row gt_left">Hardwarekomponenten, Speicher- u. Peripheriegeräte</td>
<td class="gt_row gt_right">45202030</td>
<td class="gt_row gt_left">Informationstechnologie</td>
<td class="gt_row gt_right" style="background-color: #15FE00; color: #000000;">17.1</td>
<td class="gt_row gt_right">2.6</td>
<td class="gt_row gt_right">5.6</td>
<td class="gt_row gt_right">12.3</td>
<td class="gt_row gt_right">24.2</td>
<td class="gt_row gt_right">63.4</td></tr>
    <tr><td class="gt_row gt_left">Gemischtwarenhandel</td>
<td class="gt_row gt_right">25503020</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #15FE00; color: #000000;">17.1</td>
<td class="gt_row gt_right">0.1</td>
<td class="gt_row gt_right">14.3</td>
<td class="gt_row gt_right">18.1</td>
<td class="gt_row gt_right">22.3</td>
<td class="gt_row gt_right">28.9</td></tr>
    <tr><td class="gt_row gt_left">Bildungsdienste</td>
<td class="gt_row gt_right">25302010</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #15FE00; color: #000000;">16.5</td>
<td class="gt_row gt_right">1.2</td>
<td class="gt_row gt_right">4.4</td>
<td class="gt_row gt_right">11.1</td>
<td class="gt_row gt_right">26.0</td>
<td class="gt_row gt_right">78.9</td></tr>
    <tr><td class="gt_row gt_left">[Sonstige] Textilien</td>
<td class="gt_row gt_right">25203030</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #14FE00; color: #000000;">15.3</td>
<td class="gt_row gt_right">14.9</td>
<td class="gt_row gt_right">15.1</td>
<td class="gt_row gt_right">15.3</td>
<td class="gt_row gt_right">15.6</td>
<td class="gt_row gt_right">15.8</td></tr>
    <tr><td class="gt_row gt_left">Vertriebsunternehmen</td>
<td class="gt_row gt_right">25501010</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #14FE00; color: #000000;">15.3</td>
<td class="gt_row gt_right">6.1</td>
<td class="gt_row gt_right">8.7</td>
<td class="gt_row gt_right">10.7</td>
<td class="gt_row gt_right">15.7</td>
<td class="gt_row gt_right">38.6</td></tr>
    <tr><td class="gt_row gt_left">[Sonstige] Fachgeschäfte</td>
<td class="gt_row gt_right">25504040</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #13FE00; color: #000000;">14.9</td>
<td class="gt_row gt_right">7.0</td>
<td class="gt_row gt_right">11.0</td>
<td class="gt_row gt_right">15.4</td>
<td class="gt_row gt_right">18.4</td>
<td class="gt_row gt_right">22.9</td></tr>
    <tr><td class="gt_row gt_left">Haus- u. Wohnungsbau [Eigenheime]</td>
<td class="gt_row gt_right">25201030</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #13FE00; color: #000000;">14.6</td>
<td class="gt_row gt_right">3.2</td>
<td class="gt_row gt_right">3.6</td>
<td class="gt_row gt_right">4.2</td>
<td class="gt_row gt_right">18.1</td>
<td class="gt_row gt_right">86.5</td></tr>
    <tr><td class="gt_row gt_left">Fußbekleidung</td>
<td class="gt_row gt_right">25203020</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #13FE00; color: #000000;">14.2</td>
<td class="gt_row gt_right">6.0</td>
<td class="gt_row gt_right">7.1</td>
<td class="gt_row gt_right">10.3</td>
<td class="gt_row gt_right">14.5</td>
<td class="gt_row gt_right">39.7</td></tr>
    <tr><td class="gt_row gt_left">Verbraucherelektronik</td>
<td class="gt_row gt_right">25201010</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #12FE00; color: #000000;">13.3</td>
<td class="gt_row gt_right">1.4</td>
<td class="gt_row gt_right">6.8</td>
<td class="gt_row gt_right">11.6</td>
<td class="gt_row gt_right">17.1</td>
<td class="gt_row gt_right">32.2</td></tr>
    <tr><td class="gt_row gt_left">Bau- u. Heimwerkerbedarf (inkl. Gartenbedarf)</td>
<td class="gt_row gt_right">25504030</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #12FE00; color: #000000;">13.2</td>
<td class="gt_row gt_right">9.5</td>
<td class="gt_row gt_right">10.3</td>
<td class="gt_row gt_right">12.0</td>
<td class="gt_row gt_right">13.4</td>
<td class="gt_row gt_right">22.0</td></tr>
    <tr><td class="gt_row gt_left">Einzelhandel: Drogerien u. Apotheken</td>
<td class="gt_row gt_right">30101010</td>
<td class="gt_row gt_left">Basiskonsumgüter</td>
<td class="gt_row gt_right" style="background-color: #12FE00; color: #000000;">13.1</td>
<td class="gt_row gt_right">0.6</td>
<td class="gt_row gt_right">8.9</td>
<td class="gt_row gt_right">12.9</td>
<td class="gt_row gt_right">17.1</td>
<td class="gt_row gt_right">25.9</td></tr>
    <tr><td class="gt_row gt_left">Weitere diverzierte Finanzdienstleister</td>
<td class="gt_row gt_right">40201020</td>
<td class="gt_row gt_left">Finanzen</td>
<td class="gt_row gt_right" style="background-color: #11FE00; color: #000000;">12.8</td>
<td class="gt_row gt_right">1.7</td>
<td class="gt_row gt_right">2.0</td>
<td class="gt_row gt_right">2.1</td>
<td class="gt_row gt_right">5.5</td>
<td class="gt_row gt_right">52.7</td></tr>
    <tr><td class="gt_row gt_left">Freizeitartikel</td>
<td class="gt_row gt_right">25202010</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #10FE00; color: #000000;">11.9</td>
<td class="gt_row gt_right">0.7</td>
<td class="gt_row gt_right">7.8</td>
<td class="gt_row gt_right">11.4</td>
<td class="gt_row gt_right">14.9</td>
<td class="gt_row gt_right">29.1</td></tr>
    <tr><td class="gt_row gt_left">Hausgeräte</td>
<td class="gt_row gt_right">25201040</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #10FE00; color: #000000;">11.8</td>
<td class="gt_row gt_right">2.3</td>
<td class="gt_row gt_right">5.4</td>
<td class="gt_row gt_right">8.4</td>
<td class="gt_row gt_right">13.0</td>
<td class="gt_row gt_right">30.2</td></tr>
    <tr><td class="gt_row gt_left">Verbrauchermärkte und Supercenter</td>
<td class="gt_row gt_right">30101040</td>
<td class="gt_row gt_left">Basiskonsumgüter</td>
<td class="gt_row gt_right" style="background-color: #10FE00; color: #000000;">11.7</td>
<td class="gt_row gt_right">0.2</td>
<td class="gt_row gt_right">4.5</td>
<td class="gt_row gt_right">9.2</td>
<td class="gt_row gt_right">14.3</td>
<td class="gt_row gt_right">32.8</td></tr>
    <tr><td class="gt_row gt_left">Computer u. Elektronik</td>
<td class="gt_row gt_right">25504020</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #0FFE00; color: #000000;">11.0</td>
<td class="gt_row gt_right">8.5</td>
<td class="gt_row gt_right">10.0</td>
<td class="gt_row gt_right">11.0</td>
<td class="gt_row gt_right">12.0</td>
<td class="gt_row gt_right">13.8</td></tr>
    <tr><td class="gt_row gt_left">Filme und Entertainment</td>
<td class="gt_row gt_right">50202010</td>
<td class="gt_row gt_left">Kommunikationsdienstleistungen</td>
<td class="gt_row gt_right" style="background-color: #0FFE00; color: #000000;">10.7</td>
<td class="gt_row gt_right">0.4</td>
<td class="gt_row gt_right">2.8</td>
<td class="gt_row gt_right">3.7</td>
<td class="gt_row gt_right">20.5</td>
<td class="gt_row gt_right">40.0</td></tr>
    <tr><td class="gt_row gt_left">Einzelhandel: Einrichtungsgegenständel</td>
<td class="gt_row gt_right">25504060</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #0FFE00; color: #000000;">10.7</td>
<td class="gt_row gt_right">4.0</td>
<td class="gt_row gt_right">9.2</td>
<td class="gt_row gt_right">9.7</td>
<td class="gt_row gt_right">11.8</td>
<td class="gt_row gt_right">19.0</td></tr>
    <tr><td class="gt_row gt_left">Kommerzielle Druckereien</td>
<td class="gt_row gt_right">20201010</td>
<td class="gt_row gt_left">Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport</td>
<td class="gt_row gt_right" style="background-color: #0EFE00; color: #000000;">10.0</td>
<td class="gt_row gt_right">5.1</td>
<td class="gt_row gt_right">6.1</td>
<td class="gt_row gt_right">9.6</td>
<td class="gt_row gt_right">12.1</td>
<td class="gt_row gt_right">18.0</td></tr>
    <tr><td class="gt_row gt_left">Internetdienste u. -infrastruktur</td>
<td class="gt_row gt_right">45102030</td>
<td class="gt_row gt_left">Informationstechnologie</td>
<td class="gt_row gt_right" style="background-color: #0EFE00; color: #000000;">9.7</td>
<td class="gt_row gt_right">0.7</td>
<td class="gt_row gt_right">3.1</td>
<td class="gt_row gt_right">4.0</td>
<td class="gt_row gt_right">4.7</td>
<td class="gt_row gt_right">80.8</td></tr>
    <tr><td class="gt_row gt_left">Gesundheitswesen: Vertriebshandel [Großhandel]</td>
<td class="gt_row gt_right">35102010</td>
<td class="gt_row gt_left">Gesundheitswesen</td>
<td class="gt_row gt_right" style="background-color: #0EFE00; color: #000000;">9.7</td>
<td class="gt_row gt_right">0.5</td>
<td class="gt_row gt_right">2.7</td>
<td class="gt_row gt_right">9.5</td>
<td class="gt_row gt_right">10.4</td>
<td class="gt_row gt_right">25.7</td></tr>
    <tr><td class="gt_row gt_left">Datenverarbeitungs- u. Outsourcing-Dienste</td>
<td class="gt_row gt_right">45102020</td>
<td class="gt_row gt_left">Informationstechnologie</td>
<td class="gt_row gt_right" style="background-color: #0EFF00; color: #000000;">9.5</td>
<td class="gt_row gt_right">0.1</td>
<td class="gt_row gt_right">4.7</td>
<td class="gt_row gt_right">7.1</td>
<td class="gt_row gt_right">9.3</td>
<td class="gt_row gt_right">47.4</td></tr>
    <tr><td class="gt_row gt_left">IT-Beratung u. weitere Dienste</td>
<td class="gt_row gt_right">45102010</td>
<td class="gt_row gt_left">Informationstechnologie</td>
<td class="gt_row gt_right" style="background-color: #0DFF00; color: #000000;">9.3</td>
<td class="gt_row gt_right">1.0</td>
<td class="gt_row gt_right">4.8</td>
<td class="gt_row gt_right">6.0</td>
<td class="gt_row gt_right">9.9</td>
<td class="gt_row gt_right">39.9</td></tr>
    <tr><td class="gt_row gt_left">[Konventionelle] Fernseh- u. Rundfunkübertragung</td>
<td class="gt_row gt_right">50201020</td>
<td class="gt_row gt_left">Kommunikationsdienstleistungen</td>
<td class="gt_row gt_right" style="background-color: #0DFF00; color: #000000;">8.6</td>
<td class="gt_row gt_right">3.0</td>
<td class="gt_row gt_right">6.5</td>
<td class="gt_row gt_right">8.7</td>
<td class="gt_row gt_right">10.1</td>
<td class="gt_row gt_right">16.8</td></tr>
    <tr><td class="gt_row gt_left">Diversifizierte [allgemeine] Banken</td>
<td class="gt_row gt_right">40101010</td>
<td class="gt_row gt_left">Finanzen</td>
<td class="gt_row gt_right" style="background-color: #0BFF00; color: #000000;">7.6</td>
<td class="gt_row gt_right">0.0</td>
<td class="gt_row gt_right">3.8</td>
<td class="gt_row gt_right">6.9</td>
<td class="gt_row gt_right">8.5</td>
<td class="gt_row gt_right">31.7</td></tr>
    <tr><td class="gt_row gt_left">Einzelhandel: Internet- u. Direktverkauf</td>
<td class="gt_row gt_right">25502020</td>
<td class="gt_row gt_left">Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter</td>
<td class="gt_row gt_right" style="background-color: #0BFF00; color: #000000;">7.6</td>
<td class="gt_row gt_right">0.4</td>
<td class="gt_row gt_right">2.2</td>
<td class="gt_row gt_right">4.4</td>
<td class="gt_row gt_right">9.3</td>
<td class="gt_row gt_right">50.7</td></tr>
    <tr><td class="gt_row gt_left">Personalwesen u. Arbeitsvermittlungen</td>
<td class="gt_row gt_right">20202010</td>
<td class="gt_row gt_left">Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport</td>
<td class="gt_row gt_right" style="background-color: #0BFF00; color: #000000;">7.3</td>
<td class="gt_row gt_right">0.1</td>
<td class="gt_row gt_right">1.1</td>
<td class="gt_row gt_right">1.7</td>
<td class="gt_row gt_right">3.9</td>
<td class="gt_row gt_right">54.9</td></tr>
    <tr><td class="gt_row gt_left">Anwendungssoftware</td>
<td class="gt_row gt_right">45103010</td>
<td class="gt_row gt_left">Informationstechnologie</td>
<td class="gt_row gt_right" style="background-color: #0AFF00; color: #000000;">6.5</td>
<td class="gt_row gt_right">0.8</td>
<td class="gt_row gt_right">3.5</td>
<td class="gt_row gt_right">4.7</td>
<td class="gt_row gt_right">6.3</td>
<td class="gt_row gt_right">52.0</td></tr>
    <tr><td class="gt_row gt_left">Regionale Banken</td>
<td class="gt_row gt_right">40101015</td>
<td class="gt_row gt_left">Finanzen</td>
<td class="gt_row gt_right" style="background-color: #09FF00; color: #000000;">6.0</td>
<td class="gt_row gt_right">1.4</td>
<td class="gt_row gt_right">4.9</td>
<td class="gt_row gt_right">5.6</td>
<td class="gt_row gt_right">6.3</td>
<td class="gt_row gt_right">27.5</td></tr>
    <tr><td class="gt_row gt_left">Verlagswesen</td>
<td class="gt_row gt_right">50201040</td>
<td class="gt_row gt_left">Kommunikationsdienstleistungen</td>
<td class="gt_row gt_right" style="background-color: #09FF00; color: #000000;">5.9</td>
<td class="gt_row gt_right">3.1</td>
<td class="gt_row gt_right">4.1</td>
<td class="gt_row gt_right">4.7</td>
<td class="gt_row gt_right">7.2</td>
<td class="gt_row gt_right">9.7</td></tr>
    <tr><td class="gt_row gt_left">Sparkassen u. Hypothekenfinanzierung</td>
<td class="gt_row gt_right">40102010</td>
<td class="gt_row gt_left">Finanzen</td>
<td class="gt_row gt_right" style="background-color: #08FF00; color: #000000;">5.5</td>
<td class="gt_row gt_right">0.0</td>
<td class="gt_row gt_right">3.2</td>
<td class="gt_row gt_right">5.4</td>
<td class="gt_row gt_right">6.4</td>
<td class="gt_row gt_right">37.9</td></tr>
    <tr><td class="gt_row gt_left">Interaktive Medien u. Dienste</td>
<td class="gt_row gt_right">50203010</td>
<td class="gt_row gt_left">Kommunikationsdienstleistungen</td>
<td class="gt_row gt_right" style="background-color: #08FF00; color: #000000;">5.4</td>
<td class="gt_row gt_right">0.5</td>
<td class="gt_row gt_right">2.1</td>
<td class="gt_row gt_right">3.3</td>
<td class="gt_row gt_right">4.5</td>
<td class="gt_row gt_right">23.3</td></tr>
    <tr><td class="gt_row gt_left">Interaktives Home-Entertainment</td>
<td class="gt_row gt_right">50202020</td>
<td class="gt_row gt_left">Kommunikationsdienstleistungen</td>
<td class="gt_row gt_right" style="background-color: #07FF00; color: #000000;">4.9</td>
<td class="gt_row gt_right">0.0</td>
<td class="gt_row gt_right">0.7</td>
<td class="gt_row gt_right">2.7</td>
<td class="gt_row gt_right">3.9</td>
<td class="gt_row gt_right">30.4</td></tr>
    <tr><td class="gt_row gt_left">Managed Health Care [HMOs]</td>
<td class="gt_row gt_right">35102030</td>
<td class="gt_row gt_left">Gesundheitswesen</td>
<td class="gt_row gt_right" style="background-color: #07FF00; color: #000000;">4.8</td>
<td class="gt_row gt_right">0.6</td>
<td class="gt_row gt_right">0.7</td>
<td class="gt_row gt_right">1.2</td>
<td class="gt_row gt_right">10.8</td>
<td class="gt_row gt_right">12.4</td></tr>
    <tr><td class="gt_row gt_left">Börsen u. [Finanz-] Daten</td>
<td class="gt_row gt_right">40203040</td>
<td class="gt_row gt_left">Finanzen</td>
<td class="gt_row gt_right" style="background-color: #06FF00; color: #000000;">4.4</td>
<td class="gt_row gt_right">1.5</td>
<td class="gt_row gt_right">3.7</td>
<td class="gt_row gt_right">4.1</td>
<td class="gt_row gt_right">5.0</td>
<td class="gt_row gt_right">7.4</td></tr>
    <tr><td class="gt_row gt_left">Versicherungsmakler</td>
<td class="gt_row gt_right">40301010</td>
<td class="gt_row gt_left">Finanzen</td>
<td class="gt_row gt_right" style="background-color: #06FF00; color: #000000;">4.3</td>
<td class="gt_row gt_right">0.5</td>
<td class="gt_row gt_right">2.5</td>
<td class="gt_row gt_right">3.9</td>
<td class="gt_row gt_right">5.2</td>
<td class="gt_row gt_right">13.8</td></tr>
    <tr><td class="gt_row gt_left">Werbung</td>
<td class="gt_row gt_right">50201010</td>
<td class="gt_row gt_left">Kommunikationsdienstleistungen</td>
<td class="gt_row gt_right" style="background-color: #06FF00; color: #000000;">3.8</td>
<td class="gt_row gt_right">2.1</td>
<td class="gt_row gt_right">2.9</td>
<td class="gt_row gt_right">3.4</td>
<td class="gt_row gt_right">3.9</td>
<td class="gt_row gt_right">8.1</td></tr>
    <tr><td class="gt_row gt_left">Private Finanzdienstleistungen</td>
<td class="gt_row gt_right">40202010</td>
<td class="gt_row gt_left">Finanzen</td>
<td class="gt_row gt_right" style="background-color: #05FF00; color: #000000;">3.4</td>
<td class="gt_row gt_right">0.2</td>
<td class="gt_row gt_right">1.6</td>
<td class="gt_row gt_right">2.4</td>
<td class="gt_row gt_right">3.6</td>
<td class="gt_row gt_right">17.3</td></tr>
    <tr><td class="gt_row gt_left">Investmentbanken u. Broker</td>
<td class="gt_row gt_right">40203020</td>
<td class="gt_row gt_left">Finanzen</td>
<td class="gt_row gt_right" style="background-color: #05FF00; color: #000000;">3.2</td>
<td class="gt_row gt_right">0.9</td>
<td class="gt_row gt_right">1.9</td>
<td class="gt_row gt_right">2.9</td>
<td class="gt_row gt_right">3.4</td>
<td class="gt_row gt_right">12.8</td></tr>
    <tr><td class="gt_row gt_left">Lebens- und Krankenversicherungen</td>
<td class="gt_row gt_right">40301020</td>
<td class="gt_row gt_left">Finanzen</td>
<td class="gt_row gt_right" style="background-color: #03FF00; color: #000000;">2.2</td>
<td class="gt_row gt_right">0.4</td>
<td class="gt_row gt_right">0.9</td>
<td class="gt_row gt_right">1.2</td>
<td class="gt_row gt_right">1.8</td>
<td class="gt_row gt_right">13.0</td></tr>
    <tr><td class="gt_row gt_left">Vermögensverwaltungs- u. Depotbanken</td>
<td class="gt_row gt_right">40203010</td>
<td class="gt_row gt_left">Finanzen</td>
<td class="gt_row gt_right" style="background-color: #03FF00; color: #000000;">1.8</td>
<td class="gt_row gt_right">0.0</td>
<td class="gt_row gt_right">0.9</td>
<td class="gt_row gt_right">1.2</td>
<td class="gt_row gt_right">1.9</td>
<td class="gt_row gt_right">7.2</td></tr>
    <tr><td class="gt_row gt_left">Schaden- und Unfallversicherungen</td>
<td class="gt_row gt_right">40301040</td>
<td class="gt_row gt_left">Finanzen</td>
<td class="gt_row gt_right" style="background-color: #02FF00; color: #000000;">1.6</td>
<td class="gt_row gt_right">0.2</td>
<td class="gt_row gt_right">0.9</td>
<td class="gt_row gt_right">1.0</td>
<td class="gt_row gt_right">1.3</td>
<td class="gt_row gt_right">21.8</td></tr>
    <tr><td class="gt_row gt_left">Diversifizierte Versicherungen</td>
<td class="gt_row gt_right">40301030</td>
<td class="gt_row gt_left">Finanzen</td>
<td class="gt_row gt_right" style="background-color: #02FF00; color: #000000;">1.3</td>
<td class="gt_row gt_right">0.4</td>
<td class="gt_row gt_right">1.0</td>
<td class="gt_row gt_right">1.4</td>
<td class="gt_row gt_right">1.8</td>
<td class="gt_row gt_right">1.8</td></tr>
    <tr><td class="gt_row gt_left">Rückversicherungen</td>
<td class="gt_row gt_right">40301050</td>
<td class="gt_row gt_left">Finanzen</td>
<td class="gt_row gt_right" style="background-color: #01FF00; color: #000000;">0.4</td>
<td class="gt_row gt_right">0.1</td>
<td class="gt_row gt_right">0.2</td>
<td class="gt_row gt_right">0.4</td>
<td class="gt_row gt_right">0.5</td>
<td class="gt_row gt_right">0.7</td></tr>
    <tr><td class="gt_row gt_left">Diversifizierte Kapitalmärkte</td>
<td class="gt_row gt_right">40203030</td>
<td class="gt_row gt_left">Finanzen</td>
<td class="gt_row gt_right" style="background-color: #808080; color: #000000;">NaN</td>
<td class="gt_row gt_right">NA</td>
<td class="gt_row gt_right">NA</td>
<td class="gt_row gt_right">NA</td>
<td class="gt_row gt_right">NA</td>
<td class="gt_row gt_right">NA</td></tr>
    <tr><td class="gt_row gt_left">Seehäfen u. Dienstleistungen</td>
<td class="gt_row gt_right">20305030</td>
<td class="gt_row gt_left">Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport</td>
<td class="gt_row gt_right" style="background-color: #808080; color: #000000;">NaN</td>
<td class="gt_row gt_right">NA</td>
<td class="gt_row gt_right">NA</td>
<td class="gt_row gt_right">NA</td>
<td class="gt_row gt_right">NA</td>
<td class="gt_row gt_right">NA</td></tr>
  </tbody>
  <tfoot class="gt_sourcenotes">
    <tr>
      <td class="gt_sourcenote" colspan="9">Daten: Refinitiv (Industrie- und GHG-Daten). Q4/2021, US Securities and Exchange Commission (13F-Filings)</td>
    </tr>
  </tfoot>
  <tfoot class="gt_footnotes">
    <tr>
      <td class="gt_footnote" colspan="9"><sup class="gt_footnote_marks">1</sup> Emissionen auf eine Dezimalstelle gerundet.</td>
    </tr>
  </tfoot>
</table>
</div>
```

-   grosse Unterschiede innerhalb der Industrien selbst (s. z.B. [Belkhir & Elmeligi (2019)](https://doi.org/10.1016/j.jclepro.2018.11.204)) (Roche steht z.B. viel besser da als Procter & Gamble), bzw. P&G generierte etwa gleich viel Umsatz wie Johnson & Johnson, stösst aber viel mehr CO2 aus.

<!-- Als Boxplot (noch reordern): -->

<!-- ```{r, fig.height=15} -->

<!-- filings %>% -->

<!--   distinct( -->

<!--     company_common_name, gics_industry_name, -->

<!--     co2_equivalent_emissions_indirect_scope_3_to_revenues_usd_in_million -->

<!--   ) %>% -->

<!--   ggplot( -->

<!--     aes( -->

<!--       co2_equivalent_emissions_indirect_scope_3_to_revenues_usd_in_million, -->

<!--       forcats::fct_reorder( -->

<!--         gics_industry_name, -->

<!--         co2_equivalent_emissions_indirect_scope_3_to_revenues_usd_in_million, -->

<!--         median -->

<!--       ) -->

<!--     ) -->

<!--   ) + -->

<!--   geom_boxplot() -->

<!-- ``` -->

<!-- Boxplot der Industriedurchschnitte (mean_scope_3_by_revenue) -->

<!-- ```{r} -->

<!-- mean_industry_co2_eqs %>% -->

<!--   ggplot(aes(mean_scope_3_by_revenue)) + -->

<!--   geom_boxplot(notch = TRUE) + -->

<!--   scale_x_continuous(breaks = scales::breaks_pretty(n = 10)) -->

<!-- ``` -->

<!-- Boxplot aller Firmen: -->

<!-- ```{r} -->

<!-- eikon %>% -->

<!--   distinct(cusip, co2_equivalent_emissions_indirect_scope_3_to_revenues_usd_in_million) %>% -->

<!--   ggplot(aes(co2_equivalent_emissions_indirect_scope_3_to_revenues_usd_in_million)) + -->

<!--   geom_boxplot(notch = TRUE) + -->

<!--   scale_x_log10() -->

<!-- ``` -->

Durchschnittliche CO2-Equivalente der relativ zum Umsatz CO2-intensivsten Industrien (Median der Industrien grösser als 85 %-Quantil der totalen Emissionen aller Unternehmen):


```{=html}
<div id="htmlwidget-8213f1534dd0b1d0bd61" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-8213f1534dd0b1d0bd61">{"x":{"filter":"none","vertical":false,"data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26"],["Energie-Agenturen","Kohle und nicht-erneuerbare Brennstoffe","Industriegase","Multi-Versorger","Stromversorgungsunternehmen","Aluminium","Baustoffe","diverse Chemikalien","Erdöl u. Erdgas: Lagerung u. Transport","Bau- und Schwerlastfahrzeuge [inkl. ziviler Schiffbau]","Stahl","Fluggesellschaften","Umwelt- und Anlagendienste","Elektrische Komponenten u. Geräte [ohne Computer]","Metalle u. Bergbau: diverse","Erdöl u. Erdgas: Exploration u. Förderung","Schifffahrt [ohne Kreuzfahrtlinien]","Integrierte Erdöl- und Erdgasunternehmen","Metall- u. Glasverpackungen","Grundchemikalien","Düngemittel u. Agrochemikalien","Hotels, Urlaubsanlagen u. Kreuzfahrtlinien","Diverse Immobilien-Aktivitäten","Edelmetalle u. Minerale","Papier","Erdöl u. Erdgasförderung"],[55105010,10102050,15101040,55103010,55101010,15104010,15102010,15101020,10102040,20106010,15104050,20302010,20201050,20104010,15104020,10102020,20303010,10102010,15103010,15101010,15101030,25301020,60102010,15104040,15105020,10101010],["Versorgungsunternehmen","Energie","Material: Roh- und Grundstoffe","Versorgungsunternehmen","Versorgungsunternehmen","Material: Roh- und Grundstoffe","Material: Roh- und Grundstoffe","Material: Roh- und Grundstoffe","Energie","Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport","Material: Roh- und Grundstoffe","Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport","Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport","Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport","Material: Roh- und Grundstoffe","Energie","Industriegüter: Produktion von Investitionsgütern für den gewerblichen und öffentlichen Sektor, dazugehörige Dienstleistungen, Transport","Energie","Material: Roh- und Grundstoffe","Material: Roh- und Grundstoffe","Material: Roh- und Grundstoffe","Nicht-Basiskonsumgüter, private Investitionsgüter, zyklische Konsumgüter","Immobilien","Material: Roh- und Grundstoffe","Material: Roh- und Grundstoffe","Energie"],[7126.06137335697,2974.70691136225,2344.2797636346,2213.32585282591,2053.86308755163,1803.13023414971,1567.74637842707,1370.21961528787,1282.76983556982,1232.17665646321,1108.0525138677,1064.43032216637,948.732867384654,936.414639971157,897.518402563844,851.315558952338,791.729467306994,733.988077266072,733.432172455189,720.497676517125,704.673708562156,689.867720342857,618.093652873348,550.266849802336,460.631200737113,451.068366474183]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>gics_sub_industry_name<\/th>\n      <th>gics_sub_industry_code<\/th>\n      <th>gics_sector_name<\/th>\n      <th>mean_tot_co2_by_revenue<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"pageLength":10,"scrollX":"800px","columnDefs":[{"className":"dt-right","targets":[2,4]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false}},"evals":[],"jsHooks":[]}</script>
```

(Welche [GICS-Industrien](https://en.wikipedia.org/wiki/Global_Industry_Classification_Standard) könnten gemäss Green/Brown Share Approach als brown zählen?

-   Gas Utilities

-   Oil, Gas & Consumable Fuels

    -   Integrated Oil & Gas
    -   Oil & Gas Exploration & Production
    -   Oil & Gas Refining & Marketing
    -   Oil & Gas Storage & Transportation
    -   Coal & Consumable Fuels

-   Energy Equipment & Services

    -   Oil & Gas Drilling
    -   Oil & Gas Equipment & Services

-   Airlines

unter Umständen wohl auch

-   Metals & Mining

    -   Aluminum
    -   Diversified Metals & Mining
    -   Copper
    -   Gold
    -   Precious Metals & Minerals
    -   Silver
    -   Steel

-   Electric Utilities )

Gemäss [Measuring Climate-Related Risks in Investment Portfolios von Swiss Sustainable Finance p.5:](https://s3.us-west-2.amazonaws.com/secure.notion-static.com/f48b38e4-8853-4e00-ade6-73d0ccb6e6ea/2019_03_04_SSF_Focus_Measuring_Climate_related_Risks_in_Portfolios_final.pdf?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT73L2G45O3KS52Y5%2F20210506%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20210506T144212Z&X-Amz-Expires=86400&X-Amz-Signature=88292fc8126d9a118bd22d61506f56d553c8aa7af118767964c42b5057798b5a&X-Amz-SignedHeaders=host&response-content-disposition=filename%20%3D%222019_03_04_SSF_Focus_Measuring_Climate_related_Risks_in_Portfolios_final.pdf%22)

> Green/brown shares can either be calculated at a technological level, where different industry-specific metrics are relevant (e.g. average gCO₂e/km of the fleet in the logistics sector), or by using industry classification data, such as GICS15, ICB16 or ISIC17. According to UNEP FI et al. (2015), these classifications can be useful when considering the high-carbon sector. However, they offer less detail regarding new "green" activities, such as energy efficiency measures or low-carbon technology.



In welchen Industrien sind die CO2-intensivsten Unternehmen?

CO2-intensivste Investitionen (absolut) in den Portfolios:




<div class="figure">

```{=html}
<div id="htmlwidget-607857b157013db7241f" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-607857b157013db7241f">{"x":{"filter":"none","vertical":false,"data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30"],[32373.5789473684,20966.2376422309,19660.0416666667,14709.9074074074,14489.7959183673,14475.3920386007,13721.5614971975,12120.756302521,9723.07005136604,8587.05178344249,8269.14270733199,8224.93296847236,7796.33507853403,6466.71883923095,6363.43195525302,6304.80635469215,6264.09024649553,6216.64832730227,6074.63665501629,5880.09366418581,5647.30418204801,5509.99309630652,5481.92415038581,5148.11288176838,4889.85848681874,4874.77103431432,4798.31350442257,4731.59821607538,4670.18421052632,4567.75426097258],["Nikola Corp","Abb Ltd","Beam Therapeutics Inc","Aqua Metals Inc","Dynegy Inc","Energy Fuels Inc","Huaneng Power International Inc","Virgin Galactic Holdings Inc","VolitionRX Ltd","OGE Energy Corp","Vistra Corp","SFL Corporation Ltd","TransAlta Corp","SEACOR Marine Holdings Inc","CONSOL Energy Inc","Blink Charging Co","Pampa Energia SA","Great Plains Energy Inc","Black Hills Corp","Transportadora de Gas del Sur SA","Daqo New Energy Corp","Ameren Corp","O-I Glass Inc","Vedanta Ltd","Sasol Ltd","Evergy Inc","NACCO Industries Inc","Companhia Siderurgica Nacional","TG Therapeutics Inc","NorthWestern Corp"],["Bau- und Schwerlastfahrzeuge [inkl. ziviler Schiffbau]","Elektrische Komponenten u. Geräte [ohne Computer]","Biotechnologie","Umwelt- und Anlagendienste","Energie-Agenturen","Kohle und nicht-erneuerbare Brennstoffe","Energie-Agenturen","Luft- u. Raumfahrt u. Verteidigung","Produkte","Stromversorgungsunternehmen","Energie-Agenturen","Erdöl u. Erdgas: Lagerung u. Transport","Energie-Agenturen","Erdöl u. Erdgas: Ausrüstung und Dienste","Kohle und nicht-erneuerbare Brennstoffe","Elektrische Komponenten u. Geräte [ohne Computer]","Stromversorgungsunternehmen",null,"Multi-Versorger","Erdöl u. Erdgas: Lagerung u. Transport","Halbleiterausrüstung","Multi-Versorger","Metall- u. Glasverpackungen","Metalle u. Bergbau: diverse","diverse Chemikalien","Stromversorgungsunternehmen","Kohle und nicht-erneuerbare Brennstoffe","Stahl","Biotechnologie","Multi-Versorger"]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>tot_co2_by_revenue<\/th>\n      <th>company_common_name<\/th>\n      <th>gics_sub_industry_name<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"pageLength":10,"scrollX":"800px","columnDefs":[{"className":"dt-right","targets":1},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false}},"evals":[],"jsHooks":[]}</script>
```

<p class="caption">(\#fig:unnamed-chunk-38)CO2-intensivste Unternehmen, Top 30, portfolioübergreifend</p>
</div>

- hier möglicherweise gewisse False Positives (Unternehmen mit keinem/tiefem Umsatz)


<!-- #### Portfolioanteil «brauner» Industrien -->

<!-- Einzeln (relativ): -->



<!-- Wert am neusten Datum (Q2), CO2-intensivste **Subindustrien** (nicht Firmen!): -->

<!-- ```{r, fig.cap="Anteil der Aktien aus den CO2-intensivsten Subindustrien per Q2/2021"} -->
<!-- perc_by_report_date %>%  -->
<!--   filter(period_of_report == "2021-06-30") %>% -->
<!--   ggplot(aes(forcats::fct_reorder(bank_name, -perc_dirty), perc_dirty)) + -->
<!--   geom_point() + -->
<!--   geom_segment(aes( -->
<!--     x = forcats::fct_reorder(bank_name, -perc_dirty), -->
<!--     y = 0, -->
<!--     xend = forcats::fct_reorder(bank_name, -perc_dirty), -->
<!--     yend = perc_dirty -->
<!--   )) + -->
<!--   geom_text(aes(y = (perc_dirty + 3), label = paste(round(perc_dirty, 1), "%"))) + -->
<!--   coord_flip() + -->
<!--   scale_y_continuous(labels = scales::label_number(suffix = " %", accuracy = 1)) + -->
<!--   labs( -->
<!--     x = "", -->
<!--     y = "Anteil am Portfolio" -->
<!--   ) -->
<!-- ``` -->

<!-- Absoluter Wert «brauner» Industrien an Portfolios: -->

<!-- ```{r, fig.cap="Wert der Aktien aus den CO2-intensivsten Industrien"} -->
<!-- investments_by_bank_and_gics_industry_name %>% -->
<!--   filter(gics_sub_industry_name %in% top_dirtiest_industries) %>% -->
<!--   group_by(bank_name, period_of_report) %>% -->
<!--   summarize(sum_dirty = sum(investment_sum, na.rm = TRUE)) %>% -->
<!--   ggplot(aes(period_of_report, sum_dirty, bank_name)) + -->
<!--   geom_line() + -->
<!--   facet_wrap(~bank_name, ncol = 3, scales = "free_y") + -->
<!--     scale_y_continuous( -->
<!--     # labels = scales::label_number(accuracy = 1, scale = 1e-3, suffix = " Mia.") -->
<!--     labels = scales::label_number(accuracy = 1, suffix = " Mio.") -->
<!--   ) + -->
<!--   labs(y = "Mio. US-Dollar", x = "") -->
<!-- ``` -->

<!-- -   relativ sinken die investierten Beträge in «schmutzige industrien», aber in absoluten Zahlen steigen sie? -->



<!-- Nach allen Subindustrien: -->

<!-- ```{r, fig.height=15} -->
<!-- investments_by_bank_and_gics_industry_name %>% -->
<!--   # mean_industry_co2_eqs_2() %>% -->
<!--   ggplot(aes(period_of_report, perc, color = gics_sub_industry_name)) + -->
<!--   geom_line() + -->
<!--   facet_wrap(~bank_name, ncol = 3) + -->
<!--   theme(plot.title.position = "plot", legend.position = "top") + -->
<!--   guides(color = guide_legend(nrow = 4, byrow = TRUE)) -->
<!-- ``` -->

<!-- absolutes Investment in letzter Periode (Q2 2021): -->

<!-- ```{r} -->
<!-- investments_by_bank_and_gics_industry_name %>% -->
<!--   filter(period_of_report == "2021-06-30") %>%  -->
<!--   filter(gics_sub_industry_name %in% top_dirtiest_industries) %>% -->
<!--   group_by(bank_name) %>%  -->
<!--   summarize(sum_dirty = sum(investment_sum, na.rm = TRUE)) %>% -->
<!--   ggplot(aes(forcats::fct_reorder(bank_name, sum_dirty), sum_dirty)) + -->
<!--   geom_point() + -->
<!--   geom_segment(aes( -->
<!--     y = 0, -->
<!--     xend = forcats::fct_reorder(bank_name, sum_dirty), -->
<!--     yend = sum_dirty -->
<!--   )) + -->
<!--   geom_text(aes( -->
<!--     y = sum_dirty + 6e3, -->
<!--     label = if_else( -->
<!--       sum_dirty > 1000, -->
<!--       paste(round(sum_dirty / 1000, digits = 2), "Mia."), -->
<!--       paste(round(sum_dirty, digits = 2), "Mio.") -->
<!--     ) -->
<!--   )) + -->
<!--   coord_flip() + -->
<!--   scale_y_continuous( -->
<!--     labels = scales::label_number(accuracy = 1, scale = 1e-3, suffix = " Mia.") -->
<!--   ) + -->
<!--   labs(y = "Anlagewert", x = "") -->
<!-- ``` -->

<!-- #### nach Bank und Industrie -->

<!-- ##### absolut -->

<!-- ```{r} -->
<!-- investments_by_bank_and_gics_industry_name %>% -->
<!--   filter(period_of_report == "2021-06-30") %>% -->
<!--   filter(gics_sub_industry_name %in% top_dirtiest_industries) %>% -->
<!--   group_by(bank_name, gics_sub_industry_name) %>% -->
<!--   summarize( -->
<!--     sum_dirty = sum(investment_sum, na.rm = TRUE) -->
<!--   ) %>% -->
<!--   ungroup() %>% -->
<!--   group_by(bank_name) %>% -->
<!--   mutate(sum_dirty_bank = sum(sum_dirty)) %>% -->
<!--   ungroup() %>% -->
<!--   ggplot( -->
<!--     aes( -->
<!--       forcats::fct_reorder(bank_name, sum_dirty_bank), -->
<!--       sum_dirty, -->
<!--       fill = gics_sub_industry_name -->
<!--     ) -->
<!--   ) + -->
<!--   geom_col() + -->
<!--   coord_flip() + -->
<!--   scale_y_continuous( -->
<!--     labels = scales::label_number(accuracy = 1, scale = 1e-3, suffix = " Mia.") -->
<!--   ) + -->
<!--   labs(y = "Anlagewert", x = "", fill = "Industrie (GICS)") + -->
<!--   theme(plot.title.position = "plot", legend.position = "top") + -->
<!--   guides(fill = guide_legend(ncol = 2)) -->
<!-- ``` -->

<!-- -   Unterschiedliche Akzente innerhalb den 'schmutzigen Industrien' -->

<!-- ##### relativ (zum Gesamtportfolio) -->

<!-- ```{r} -->

<!-- investments_by_bank_and_gics_industry_name %>% -->

<!--       filter(period_of_report == "2021-06-30") %>%  -->

<!--    mutate(dirty_industry_indicator =  -->

<!--    gics_industry_name %in% top_dirtiest_industries) %>% -->

<!--    group_by(bank_name) %>%  -->

<!--    mutate( -->

<!--       perc_of_portfolio =  -->

<!--          investment_sum/sum(investment_sum, na.rm = TRUE) -->

<!--    ) %>% -->

<!--    group_by(bank_name, dirty_industry_indicator) %>%  -->

<!--    summarize( -->

<!--       perc_of_portfolio_sum = sum(perc_of_portfolio) * 100 -->

<!--       ) %>%  -->

<!--    ungroup() %>%  -->

<!--    ggplot( -->

<!--       aes( -->

<!--          forcats::fct_reorder(bank_name, perc_of_portfolio_sum, min),  -->

<!--          perc_of_portfolio_sum,  -->

<!--          fill = dirty_industry_indicator) -->

<!--       ) + -->

<!--    geom_col() + -->

<!--    coord_flip() + -->

<!--    scale_y_continuous(limits = c(0, 10) -->

<!--       ) +  -->

<!--    labs(y = "Anlagewert", x = "", fill = "Schmutzige Industrie") -->

<!-- ``` -->

Portfoliovergleich innerhalb der 15 % emissionsintensivsten Unternehmen, per Q2/2021:

<div class="figure">
<p class="caption">(\#fig:unnamed-chunk-41)Industrie-Exposure innerhalb der 15 % emissionsintensivsten Unternehmen, nach Bank, Q2/2021</p><img src="03-filings-mit-meta-ts_files/figure-html/unnamed-chunk-41-1.png" alt="Industrie-Exposure innerhalb der 15 % emissionsintensivsten Unternehmen, nach Bank, Q2/2021" width="672" /></div>

dasselbe als Text (je Top 3):


* Credit Suisse
   * Integrierte Erdöl- und Erdgasunternehmen: 1.573 Mia. USD
   * Stromversorgungsunternehmen: 1.55 Mia. USD
   * Erdöl u. Erdgas: Exploration u. Förderung: 1.119 Mia. USD 
* Lombard Odier
   * Fluggesellschaften: 0.369 Mia. USD
   * Stahl: 0.206 Mia. USD
   * Umwelt- und Anlagendienste: 0.066 Mia. USD 
* Pictet
   * Stromversorgungsunternehmen: 3.22 Mia. USD
   * Multi-Versorger: 2.325 Mia. USD
   * Umwelt- und Anlagendienste: 2.103 Mia. USD 
* UBS
   * Stromversorgungsunternehmen: 4.363 Mia. USD
   * Integrierte Erdöl- und Erdgasunternehmen: 4.115 Mia. USD
   * Erdöl u. Erdgas: Exploration u. Förderung: 2.814 Mia. USD 
* ZKB
   * Integrierte Erdöl- und Erdgasunternehmen: 0.198 Mia. USD
   * Stromversorgungsunternehmen: 0.131 Mia. USD
   * Halbleiter: 0.108 Mia. USD

Je Top 3 Investitionen aus jeder dieser Kategorien nach Bank:


* Credit Suisse

   * Erdöl u. Erdgas: Exploration u. Förderung

      * Conocophillips: 211.19 Mio. USD (353.5T. CO2-Äq./Mio. USD)

      * EOG Resources Inc: 147.26 Mio. USD (493.25T. CO2-Äq./Mio. USD)

      * Pioneer Natural Resources Co: 109.78 Mio. USD (247.66T. CO2-Äq./Mio. USD)

   * Integrierte Erdöl- und Erdgasunternehmen

      * Exxon Mobil Corp: 610.37 Mio. USD (420.11T. CO2-Äq./Mio. USD)

      * Chevron Corp: 567.72 Mio. USD (366.31T. CO2-Äq./Mio. USD)

      * Occidental Petroleum Corp: 132.12 Mio. USD (1008.53T. CO2-Äq./Mio. USD)

   * Stromversorgungsunternehmen

      * Nextera Energy Inc: 355.28 Mio. USD (2538.29T. CO2-Äq./Mio. USD)

      * Evergy Inc: 273.99 Mio. USD (4874.77T. CO2-Äq./Mio. USD)

      * PG&E Corp: 255.6 Mio. USD (278.16T. CO2-Äq./Mio. USD)

* Lombard Odier

   * Fluggesellschaften

      * American Airlines Group Inc: 273.73 Mio. USD (672.28T. CO2-Äq./Mio. USD)

      * United Airlines Holdings Inc: 71.16 Mio. USD (635.73T. CO2-Äq./Mio. USD)

      * Delta Air Lines Inc: 22.43 Mio. USD (1286.08T. CO2-Äq./Mio. USD)

   * Stahl

      * United States Steel Corp: 123.84 Mio. USD (1367.2T. CO2-Äq./Mio. USD)

      * Cleveland-Cliffs Inc: 47.43 Mio. USD (1905.44T. CO2-Äq./Mio. USD)

      * Steel Dynamics Inc: 33.69 Mio. USD (316.8T. CO2-Äq./Mio. USD)

   * Umwelt- und Anlagendienste

      * Waste Management Inc: 37.62 Mio. USD (910.12T. CO2-Äq./Mio. USD)

      * Republic Services Inc: 17.75 Mio. USD (1324.86T. CO2-Äq./Mio. USD)

      * Waste Connections Inc: 11.02 Mio. USD (1039.57T. CO2-Äq./Mio. USD)

* Pictet

   * Multi-Versorger

      * Sempra Energy: 459.89 Mio. USD (607.83T. CO2-Äq./Mio. USD)

      * Dominion Energy Inc: 409.07 Mio. USD (2649.34T. CO2-Äq./Mio. USD)

      * WEC Energy Group Inc: 396.88 Mio. USD (2674T. CO2-Äq./Mio. USD)

   * Stromversorgungsunternehmen

      * Nextera Energy Inc: 827.47 Mio. USD (2538.29T. CO2-Äq./Mio. USD)

      * Xcel Energy Inc: 319.68 Mio. USD (2925.97T. CO2-Äq./Mio. USD)

      * Alliant Energy Corp: 306.9 Mio. USD (3696.87T. CO2-Äq./Mio. USD)

   * Umwelt- und Anlagendienste

      * Republic Services Inc: 847.45 Mio. USD (1324.86T. CO2-Äq./Mio. USD)

      * Waste Connections Inc: 604.91 Mio. USD (1039.57T. CO2-Äq./Mio. USD)

      * Waste Management Inc: 499.3 Mio. USD (910.12T. CO2-Äq./Mio. USD)

* UBS

   * Erdöl u. Erdgas: Exploration u. Förderung

      * Conocophillips: 693.02 Mio. USD (353.5T. CO2-Äq./Mio. USD)

      * Hess Corp: 534.51 Mio. USD (495.12T. CO2-Äq./Mio. USD)

      * EOG Resources Inc: 415.93 Mio. USD (493.25T. CO2-Äq./Mio. USD)

   * Integrierte Erdöl- und Erdgasunternehmen

      * Chevron Corp: 1575.77 Mio. USD (366.31T. CO2-Äq./Mio. USD)

      * Exxon Mobil Corp: 1541.65 Mio. USD (420.11T. CO2-Äq./Mio. USD)

      * Petroleo Brasileiro SA Petrobras: 407.87 Mio. USD (1060.62T. CO2-Äq./Mio. USD)

   * Stromversorgungsunternehmen

      * Nextera Energy Inc: 1858.01 Mio. USD (2538.29T. CO2-Äq./Mio. USD)

      * Duke Energy Corp: 524.12 Mio. USD (3134.22T. CO2-Äq./Mio. USD)

      * American Electric Power Company Inc: 420.15 Mio. USD (3330.13T. CO2-Äq./Mio. USD)

* ZKB

   * Halbleiter

      * Micron Technology Inc: 95.16 Mio. USD (257.76T. CO2-Äq./Mio. USD)

      * ON Semiconductor Corp: 11.74 Mio. USD (437.45T. CO2-Äq./Mio. USD)

      * Wolfspeed Inc: 1.26 Mio. USD (824.53T. CO2-Äq./Mio. USD)

   * Integrierte Erdöl- und Erdgasunternehmen

      * Exxon Mobil Corp: 100.97 Mio. USD (420.11T. CO2-Äq./Mio. USD)

      * Chevron Corp: 59.84 Mio. USD (366.31T. CO2-Äq./Mio. USD)

      * Suncor Energy Inc: 19.77 Mio. USD (845.67T. CO2-Äq./Mio. USD)

   * Stromversorgungsunternehmen

      * Nextera Energy Inc: 31 Mio. USD (2538.29T. CO2-Äq./Mio. USD)

      * Exelon Corp: 14.25 Mio. USD (415.3T. CO2-Äq./Mio. USD)

      * Duke Energy Corp: 12.57 Mio. USD (3134.22T. CO2-Äq./Mio. USD)


<!-- ```{r, fig.height=30} -->
<!-- investments_by_bank_and_gics_industry_name %>% -->
<!--   filter(period_of_report == "2021-06-30") %>% -->
<!--   filter(gics_sub_industry_name %in% top_dirtiest_industries) %>% -->
<!--   group_by(bank_name, gics_sub_industry_name) %>% -->
<!--   summarize(sum_dirty = sum(investment_sum, na.rm = TRUE)) %>% -->
<!--   ungroup() %>% -->
<!--   mutate( -->
<!--     bank_name = as.factor(bank_name), -->
<!--     gics_sub_industry_name = -->
<!--       tidytext::reorder_within(gics_sub_industry_name, sum_dirty, bank_name) -->
<!--   ) %>% -->
<!--   ggplot( -->
<!--     aes(gics_sub_industry_name, -->
<!--       sum_dirty, -->
<!--       fill = sum_dirty -->
<!--     ) -->
<!--   ) + -->
<!--   geom_col() + -->
<!--   coord_flip() + -->
<!--   tidytext::scale_x_reordered() + -->
<!--   scale_y_continuous() + -->
<!--   # scale_fill_gradient(trans = "log10") + -->
<!--   labs( -->
<!--     y = "Anlagewert in Mio. US-Dollar", x = "", -->
<!--     fill = "Wert in Mio. USD" -->
<!--   ) + -->
<!--   facet_wrap(~bank_name, ncol = 1, scales = "free") + -->
<!--   labs() -->
<!-- ``` -->

<!-- Selbe Information in Text: -->

<!-- ```{r, results="asis"} -->
<!-- investments_by_bank_and_gics_industry_name %>% -->
<!--   filter(period_of_report == "2021-06-30") %>% -->
<!--   filter(gics_sub_industry_name %in% top_dirtiest_industries) %>% -->
<!--   group_by(bank_name, gics_sub_industry_name) %>% -->
<!--   summarize(sum_dirty = sum(investment_sum, na.rm = TRUE)) %>% -->
<!--   ungroup() %>%  -->
<!--   group_by(bank_name) %>%  -->
<!--   slice_max(order_by = sum_dirty, n = 3) %>%  -->
<!--   mutate_at(vars(sum_dirty), ~.*1e-3) %>% -->
<!--   summarize(chr_lst = paste(" *", gics_sub_industry_name, ":", round(sum_dirty, 3), "Mia. USD", collapse = "\n")) %>%  -->
<!--   mutate(text = paste("\n*", bank_name, "\n", chr_lst)) %>% -->
<!--   pull(text) %>%  -->
<!--   cat() -->
<!-- ``` -->

<!-- #### Portfolios nach geschätztem CO2-Ausstoss und Wert -->








<!-- Reinzoomen bei Investments unter 200 Mio. CO2-Äquivalenten: -->




<!-- #### Total -->

<!-- Summe aller Anlagen in C02-intensive Industrien aller Banken (Q2) -->

<!-- ```{r} -->
<!-- investments_by_bank_and_gics_industry_name %>% -->
<!--   filter(period_of_report == "2021-06-30") %>% -->
<!--   filter(gics_sub_industry_name %in% top_dirtiest_industries) %>% -->
<!--   summarize(sum_dirty = sum(investment_sum, na.rm = TRUE) / 1e3) %>% -->
<!--   DT::datatable(colnames = c("Summe aller Investitionen in C02-intensive Industrien")) -->
<!-- ``` -->

<!-- Summe aller Investitionen in C02-intensive Industrien nach Bank (Q2 2021) -->

<!-- ```{r} -->
<!-- investments_by_bank_and_gics_industry_name %>% -->
<!--    filter(period_of_report == "2021-06-30") %>%  -->
<!--    filter(gics_sub_industry_name %in% top_dirtiest_industries) %>%  -->
<!--    group_by(bank_name) %>%  -->
<!--    summarize(sum_dirty = sum(investment_sum, na.rm = TRUE)/1e3) %>% -->
<!--    arrange(desc(sum_dirty)) %>%  -->
<!--    DT::datatable(colnames = c("Bank", "Summe in Mia.")) -->
<!-- ``` -->

<!-- ## Portfolios gewichtet nach CO2-Intensität {#portfoliosnachausstossintensitaet} -->

<!-- ```{r} -->
<!-- portfolio_intensity <- filings %>%  -->
<!--   group_by(bank_name, period_of_report) %>%  -->
<!--   summarize( -->
<!--     weighted_mean_portfolio = sum(value_usd_thousands * estimated_co2_equivalents_emission_total,  na.rm = TRUE)/sum(value_usd_thousands, na.rm = TRUE), -->
<!--   )  -->
<!-- ``` -->

<!-- ```{r, fig.cap="CO2-Intensität der Portfolios über Zeit, gewichtet nach Anlagewert"} -->
<!-- portfolio_intensity %>% -->
<!--   ggplot(aes(period_of_report, weighted_mean_portfolio)) + -->
<!--   geom_line() + -->
<!--   facet_wrap(~bank_name) + -->
<!--   scale_y_continuous( -->
<!--     labels = scales::label_number(scale = 1e-6) -->
<!--     ) + -->
<!--   labs( -->
<!--     y = "Mio. Tonnen CO2-Äquivalente", -->
<!--     x = "" -->
<!--   ) -->
<!-- ``` -->

<!-- -   dieser Approach ist streng genommen nur korrekt, wenn wir für jedes Quartal die geschätzten CO2-Emissionen hätten -->

<!-- ```{r, fig.cap="CO2-Intensität der Portfolios gewichtet nach Anlagewert, Q2/2021"} -->
<!-- portfolio_intensity %>% -->
<!--   filter(period_of_report == "2021-06-30") %>% -->
<!--   ggplot(aes( -->
<!--     forcats::fct_reorder(bank_name, weighted_mean_portfolio), -->
<!--     weighted_mean_portfolio -->
<!--   )) + -->
<!--   geom_point() + -->
<!--   geom_segment(aes( -->
<!--     y = 0, -->
<!--     xend = forcats::fct_reorder(bank_name, weighted_mean_portfolio), -->
<!--     yend = weighted_mean_portfolio -->
<!--   )) + -->
<!--   geom_text(aes( -->
<!--     y = weighted_mean_portfolio + 0.5e6, -->
<!--     label = -->
<!--       round(weighted_mean_portfolio * 1e-6, 1) %>% -->
<!--         paste(., "Mio.") -->
<!--   )) + -->
<!--   coord_flip() + -->
<!--   scale_y_continuous( -->
<!--     labels = scales::label_number(accuracy = 1, scale = 1e-6) -->
<!--   ) + -->
<!--   labs(y = "Mio. Tonnen CO2-Äquivalente", x = "") -->

<!-- ``` -->









Kombinierte Grafik mit weiteren Indikatoren:

<div class="figure">
<p class="caption">(\#fig:unnamed-chunk-52)Anteil US-Aktienanlagen, nach Bank und Metrik, Q2/2014 - Q2/2021, pro Quartal, (in % und Unterschied in Prozentpunkten (Pp.))</p><img src="03-filings-mit-meta-ts_files/figure-html/unnamed-chunk-52-1.png" alt="Anteil US-Aktienanlagen, nach Bank und Metrik, Q2/2014 - Q2/2021, pro Quartal, (in % und Unterschied in Prozentpunkten (Pp.))" width="672" /></div>

<img src="03-filings-mit-meta-ts_files/figure-html/unnamed-chunk-53-1.png" width="672" />

Absoluter Wert per Q2/2021

<img src="03-filings-mit-meta-ts_files/figure-html/unnamed-chunk-54-1.png" width="672" />

<div class="figure">
<p class="caption">(\#fig:unnamed-chunk-55)Wert CO2-intensivste Aktienanlagen, Q2/2021, nach Bank</p><img src="03-filings-mit-meta-ts_files/figure-html/unnamed-chunk-55-1.png" alt="Wert CO2-intensivste Aktienanlagen, Q2/2021, nach Bank" width="672" /></div>

Kombiniert mit Prozentwerten:

<img src="03-filings-mit-meta-ts_files/figure-html/unnamed-chunk-56-1.png" width="672" />

Wie stark ähneln sich die Portfolios (nach Sektor, per Q2/2021)?

<div class="figure">
<p class="caption">(\#fig:unnamed-chunk-57)Portfolioanteile, nach Sektor und Bank, Q2/2021</p><img src="03-filings-mit-meta-ts_files/figure-html/unnamed-chunk-57-1.png" alt="Portfolioanteile, nach Sektor und Bank, Q2/2021" width="672" /></div>

Anteil der Versorgerunternehmen:

<div class="figure">
<p class="caption">(\#fig:unnamed-chunk-58)Pictet investiert stärker als alle anderen Banken in Versorgungsunternehmen</p><img src="03-filings-mit-meta-ts_files/figure-html/unnamed-chunk-58-1.png" alt="Pictet investiert stärker als alle anderen Banken in Versorgungsunternehmen" width="672" /></div>

<div class="figure">
<p class="caption">(\#fig:unnamed-chunk-59)Anteil der Aktien aus den CO2-intensivsten Subindustrien per Q2/2021</p><img src="03-filings-mit-meta-ts_files/figure-html/unnamed-chunk-59-1.png" alt="Anteil der Aktien aus den CO2-intensivsten Subindustrien per Q2/2021" width="672" /></div>


<div class="figure">
<p class="caption">(\#fig:unnamed-chunk-60)Metriken über Zeit, kapital-gewichtete Durchschnitte, Q2/2014 - Q2/2021</p><img src="03-filings-mit-meta-ts_files/figure-html/unnamed-chunk-60-1.png" alt="Metriken über Zeit, kapital-gewichtete Durchschnitte, Q2/2014 - Q2/2021" width="672" /></div>

-   [Pictet](https://www.group.pictet/de/eine-verantwortungsbewusste-vision/unser-beitrag-gegen-den-klimawandel): «Seit Ende 2020 ist unsere Bilanz frei von fossilen Brennstoffen.»

Pictets fossile Positionen (raw, nicht aggregiert):




```{=html}
<div id="htmlwidget-4cbb72119e02d3987754" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-4cbb72119e02d3987754">{"x":{"filter":"none","vertical":false,"data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89","90","91","92","93","94","95","96","97","98","99","100","101","102","103","104","105","106","107","108","109","110","111"],[1123274,1123274,1123274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1291274,1535631,1535631,1535631,1535631,1599576],["Pictet &amp; Cie (Europe) SA","Pictet &amp; Cie (Europe) SA","Pictet &amp; Cie (Europe) SA","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET BANK &amp; TRUST Ltd","PICTET BANK &amp; TRUST Ltd","PICTET BANK &amp; TRUST Ltd","PICTET BANK &amp; TRUST Ltd","Pictet North America Advisors SA"],["000112327421000005","000112327421000005","000112327421000005","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000095012321011040","000153563121000004","000153563121000004","000153563121000004","000153563121000004","000159957621000004"],["13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR"],["2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30","2021-06-30"],[2864030,2864030,2864030,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,91797087,178345,178345,178345,178345,967975],[null,null,null,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],["13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR","13F-HR"],[2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021,2021],[3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3],["https://www.sec.gov/Archives/edgar/data/1123274/000112327421000005/US13F_PEUSA_2021_Q2.xml","https://www.sec.gov/Archives/edgar/data/1123274/000112327421000005/US13F_PEUSA_2021_Q2.xml","https://www.sec.gov/Archives/edgar/data/1123274/000112327421000005/US13F_PEUSA_2021_Q2.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/0000950123-21-011040-5664.xml","https://www.sec.gov/Archives/edgar/data/1535631/000153563121000004/PBT_2021_Q2.xml","https://www.sec.gov/Archives/edgar/data/1535631/000153563121000004/PBT_2021_Q2.xml","https://www.sec.gov/Archives/edgar/data/1535631/000153563121000004/PBT_2021_Q2.xml","https://www.sec.gov/Archives/edgar/data/1535631/000153563121000004/PBT_2021_Q2.xml","https://www.sec.gov/Archives/edgar/data/1599576/000159957621000004/US13F_PNA_2021_Q2.xml"],["CHEVRON CORP NEW","ENBRIDGE INC","EOG RES INC","ANTERO MIDSTREAM CORP","ANTERO RESOURCES CORP","APA CORPORATION","ARCHROCK INC","BAKER HUGHES COMPANY","BAKER HUGHES COMPANY","CABOT OIL &amp; GAS CORP","CALIFORNIA RES CORP","CALIFORNIA RES CORP","CALIFORNIA RES CORP","CALIFORNIA RES CORP","CAMECO CORP","CAMECO CORP","CANADIAN NAT RES LTD","CANADIAN NAT RES LTD","CENOVUS ENERGY INC","CENOVUS ENERGY INC","CENTENNIAL RESOURCE DEV INC","CHAMPIONX CORPORATION","CHENIERE ENERGY INC","CHENIERE ENERGY INC","CHESAPEAKE ENERGY CORP","CHESAPEAKE ENERGY CORP","CHESAPEAKE ENERGY CORP","CHESAPEAKE ENERGY CORP","CHEVRON CORP NEW","CHEVRON CORP NEW","CIMAREX ENERGY CO","CNX RES CORP","CONOCOPHILLIPS","CONOCOPHILLIPS","CRESCENT PT ENERGY CORP","DENBURY INC","DENISON MINES CORP","DEVON ENERGY CORP NEW","DEVON ENERGY CORP NEW","DIAMONDBACK ENERGY INC","ENBRIDGE INC","ENBRIDGE INC","ENERGY FUELS INC","ENERPLUS CORP","ENLINK MIDSTREAM LLC","EOG RES INC","EOG RES INC","EQT CORP","EQUITRANS MIDSTREAM CORP","EXXON MOBIL CORP","EXXON MOBIL CORP","GEVO INC","GULFPORT ENERGY CORP","GULFPORT ENERGY CORP","HALLIBURTON CO","HALLIBURTON CO","HELIX ENERGY SOLUTIONS GRP I","HELMERICH &amp; PAYNE INC","HESS CORP","HESS CORP","HOLLYFRONTIER CORP","IMPERIAL OIL LTD","IMPERIAL OIL LTD","KINDER MORGAN INC DEL","KINDER MORGAN INC DEL","KOSMOS ENERGY LTD","MARATHON OIL CORP","MARATHON PETE CORP","MARATHON PETE CORP","MATADOR RES CO","MURPHY OIL CORP","NEXGEN ENERGY LTD","NEXTIER OILFIELD SOLUTIONS","NOV INC","OCCIDENTAL PETE CORP","OCCIDENTAL PETE CORP","ONEOK INC NEW","ONEOK INC NEW","OVINTIV INC","PATTERSON-UTI ENERGY INC","PDC ENERGY INC","PEMBINA PIPELINE CORP","PEMBINA PIPELINE CORP","PHILLIPS 66","PHILLIPS 66","PIONEER NAT RES CO","PIONEER NAT RES CO","PLAINS GP HLDGS L P","RANGE RES CORP","SCHLUMBERGER LTD","SCHLUMBERGER LTD","SOUTHWESTERN ENERGY CO","SUNCOR ENERGY INC NEW","SUNCOR ENERGY INC NEW","TARGA RES CORP","TC ENERGY CORP","TC ENERGY CORP","TELLURIAN INC NEW","TEXAS PACIFIC LAND CORPORATI","TRANSPORTADORA DE GAS SUR","VALERO ENERGY CORP","VALERO ENERGY CORP","VERMILION ENERGY INC","WILLIAMS COS INC","WILLIAMS COS INC","YPF SOCIEDAD ANONIMA","BP PLC","CHEVRON CORP NEW","EOG RES INC","ROYAL DUTCH SHELL PLC","EOG RES INC"],["COM","COM","COM","COM","COM","COM","COM","CL A","CL A","COM","COM STOCK","COM STOCK","COM STOCK","COM STOCK","COM","COM","COM","COM","COM","COM","CL A","COM","COM NEW","COM NEW","COM","COM","COM","COM","COM","COM","COM","COM","COM","COM","COM","COM","COM","COM","COM","COM","COM","COM","COM NEW","COM","COM UNIT REP LTD","COM","COM","COM","COM","COM","COM","COM PAR","COMMON SHARES","COMMON SHARES","COM","COM","COM","COM","COM","COM","COM","COM NEW","COM NEW","COM","COM","COM","COM","COM","COM","COM","COM","COM","COM","COM","COM","COM","COM","COM","COM","COM","COM","COM","COM","COM","COM","COM","COM","LTD PARTNR INT A","COM","COM STK","COM STK","COM","COM","COM","COM","COM","COM","COM","COM","SPONSORED ADS B","COM","COM","COM","COM","COM","SPON ADR CL D","SPONSORED ADR","COM","COM","SPONS ADR A","COM"],["166764100","29250N105","26875P101","03676B102","03674X106","03743Q108","03957W106","05722G100","05722G100","127097103","13057Q305","13057Q305","13057Q305","13057Q305","13321L108","13321L108","136385101","136385101","15135U109","15135U109","15136A102","15872M104","16411R208","16411R208","165167735","165167735","165167735","165167735","166764100","166764100","171798101","12653C108","20825C104","20825C104","22576C101","24790A101","248356107","25179M103","25179M103","25278X109","29250N105","29250N105","292671708","292766102","29336T100","26875P101","26875P101","26884L109","294600101","30231G102","30231G102","374396406","402635502","402635502","406216101","406216101","42330P107","423452101","42809H107","42809H107","436106108","453038408","453038408","49456B101","49456B101","500688106","565849106","56585A102","56585A102","576485205","626717102","65340P106","65290C105","62955J103","674599105","674599105","682680103","682680103","69047Q102","703481101","69327R101","706327103","706327103","718546104","718546104","723787107","723787107","72651A207","75281A109","806857108","806857108","845467109","867224107","867224107","87612G101","87807B107","87807B107","87968A104","88262P102","893870204","91913Y100","91913Y100","923725105","969457100","969457100","984245100","055622104","166764100","26875P101","780259206","26875P101"],[15211,689,6159,225,285,1069,90,7532,1425,997,8639,10954,2606,3616,2541,438,12049,2798,3497,859,109,331,6859,1378,3952,4708,2111,2702,71501,24252,500,238,32727,18176,169,231,81,6147,1189,2750,28650,5072,78,162,146,16876,3120,414,225,93334,18524,79,17169,21968,7151,1287,62,258,10552,2337,316,2271,457,15419,2316,147,1392,15811,2605,248,228,59,71,740,14957,7482,10192,1786,532,135,273,5915,1029,15382,2453,12406,2109,127,292,17954,2742,279,10262,2273,612,14562,3021,100,675,58,13351,2046,94,15331,2070,1802,137,801,567,214,2842],[145230,17200,73816,21700,18958,49434,10100,329334,62328,57099,286631,363434,86469,119956,132405,22850,331561,76996,365098,89744,16027,12900,79078,15882,76121,90674,40667,52037,682657,231540,6900,17400,537385,298457,37300,3007,66081,210579,40727,29289,714849,126538,12915,22500,22800,202249,37398,18600,26399,1479607,293658,10824,265357,339543,309308,55646,10800,7900,120844,26764,9600,74440,14981,845814,127057,42600,102172,261686,43107,6900,9790,14422,14834,48285,478329,239262,183176,32109,16900,13600,5957,185943,32357,179236,28581,76333,12980,10600,17400,560877,85666,49200,428032,94791,13773,293961,60988,21503,422,35300,170985,26210,10700,577424,77966,218306,31480,7650,6800,10600,34064],["SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH","SH"],["SOLE","SOLE","SOLE","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","DFND","SOLE","SOLE","SOLE","SOLE","SOLE"],[0,0,0,0,0,0,0,0,1,0,0,1,0,1,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,1,0,0,0,1,0,0,0,0,1,0,0,1,0,0,0,0,1,0,0,0,1,0,0,1,0,1,0,0,0,1,0,0,1,0,1,0,0,0,1,0,0,0,0,0,0,1,0,1,0,0,0,0,1,0,1,0,1,0,0,0,1,0,0,1,0,0,1,0,0,0,0,1,0,0,1,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[145230,17200,73816,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,31480,7650,6800,10600,34064],["Pictet &amp; Cie (Europe) SA","Pictet &amp; Cie (Europe) SA","Pictet &amp; Cie (Europe) SA","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET BANK &amp; TRUST Ltd","PICTET BANK &amp; TRUST Ltd","PICTET BANK &amp; TRUST Ltd","PICTET BANK &amp; TRUST Ltd","Pictet North America Advisors SA"],["13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT","13F HOLDINGS REPORT"],["06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021"],["028-15846","028-15846","028-15846","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-11664","028-15848","028-15848","028-15848","028-15848","028-15820"],["N4","N4","N4","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","C5","C5","C5","C5","V8"],["08-11-2021","08-11-2021","08-11-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-13-2021","08-11-2021","08-11-2021","08-11-2021","08-11-2021","08-11-2021"],[171,171,171,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,2737,68,68,68,68,128],[0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,0,0,0,0,0],["06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021","06-30-2021"],["https://www.sec.gov/Archives/edgar/data/1123274/000112327421000005/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1123274/000112327421000005/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1123274/000112327421000005/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1291274/000095012321011040/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1535631/000153563121000004/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1535631/000153563121000004/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1535631/000153563121000004/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1535631/000153563121000004/primary_doc.xml","https://www.sec.gov/Archives/edgar/data/1599576/000159957621000004/primary_doc.xml"],["Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet","Pictet"],["Pictet &amp; Cie (Europe) SA","Pictet &amp; Cie (Europe) SA","Pictet &amp; Cie (Europe) SA","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET ASSET MANAGEMENT LTD","PICTET BANK &amp; TRUST Ltd","PICTET BANK &amp; TRUST Ltd","PICTET BANK &amp; TRUST Ltd","PICTET BANK &amp; TRUST Ltd","Pictet North America Advisors SA"],[1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,1231,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],["N4","N4","N4","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","C5","C5","C5","C5","V8"],["N4","N4","N4","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","C5","C5","C5","C5",null],["LUXEMBOURG","LUXEMBOURG","LUXEMBOURG","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","NASSAU","NASSAU","NASSAU","NASSAU","GENEVA 73"],["N4","N4","N4","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","C5","C5","C5","C5","V8"],["1855","1855","1855","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","N-4837","N-4837","N-4837","N-4837","1211"],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,"120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","WEST BAY STREET AND BLAKE ROAD","WEST BAY STREET AND BLAKE ROAD","WEST BAY STREET AND BLAKE ROAD","WEST BAY STREET AND BLAKE ROAD",null],["LUXEMBOURG","LUXEMBOURG","LUXEMBOURG","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","LONDON","NASSAU","NASSAU","NASSAU","NASSAU","GENEVA 73"],["N4","N4","N4","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","X0","C5","C5","C5","C5","V8"],["1855","1855","1855","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","EC2Y 5ET","N-4837","N-4837","N-4837","N-4837","1211"],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,"120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","120 LONDON WALL","WEST BAY STREET AND BLAKE ROAD","WEST BAY STREET AND BLAKE ROAD","WEST BAY STREET AND BLAKE ROAD","WEST BAY STREET AND BLAKE ROAD",null],["00352467171","00352467171","00352467171","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","44 207 847 5000","0012423022222","0012423022222","0012423022222","0012423022222","01141223079000"],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],["15A, AVENUE J. F. KENNEDY","15A, AVENUE J. F. KENNEDY","15A, AVENUE J. F. KENNEDY","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","BAYSIDE EXECUTIVE PARK","BAYSIDE EXECUTIVE PARK","BAYSIDE EXECUTIVE PARK","BAYSIDE EXECUTIVE PARK","ROUTE DES ACACIAS 48"],["15A, AVENUE J. F. KENNEDY","15A, AVENUE J. F. KENNEDY","15A, AVENUE J. F. KENNEDY","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","MOOR HOUSE, LEVEL 11","BAYSIDE EXECUTIVE PARK","BAYSIDE EXECUTIVE PARK","BAYSIDE EXECUTIVE PARK","BAYSIDE EXECUTIVE PARK","ROUTE DES ACACIAS 48"]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>cik<\/th>\n      <th>company_name<\/th>\n      <th>filing_number<\/th>\n      <th>submission_type<\/th>\n      <th>period_of_report<\/th>\n      <th>table_value_total<\/th>\n      <th>is_amendment<\/th>\n      <th>amendment_no<\/th>\n      <th>amendment_type<\/th>\n      <th>form_type<\/th>\n      <th>date_filed<\/th>\n      <th>quarter<\/th>\n      <th>link_to_filing<\/th>\n      <th>issuer<\/th>\n      <th>class<\/th>\n      <th>cusip<\/th>\n      <th>value<\/th>\n      <th>shrsorprnamt<\/th>\n      <th>sshprnamttype<\/th>\n      <th>investment_discretion<\/th>\n      <th>voting_authority_sole<\/th>\n      <th>voting_authority_shared<\/th>\n      <th>voting_authority_none<\/th>\n      <th>filing_manager<\/th>\n      <th>report_type<\/th>\n      <th>report_calendar_or_quarter<\/th>\n      <th>form13f_file_number<\/th>\n      <th>state_or_country<\/th>\n      <th>signature_date<\/th>\n      <th>table_entry_total<\/th>\n      <th>other_included_managers_count<\/th>\n      <th>cover_page<\/th>\n      <th>link_to_primary_doc<\/th>\n      <th>bank_name<\/th>\n      <th>name<\/th>\n      <th>fiscal_year_end<\/th>\n      <th>company_href<\/th>\n      <th>sic<\/th>\n      <th>sic_description<\/th>\n      <th>state_location<\/th>\n      <th>state_incorporation<\/th>\n      <th>mailing_city<\/th>\n      <th>mailing_state<\/th>\n      <th>mailing_zip<\/th>\n      <th>mailing_street<\/th>\n      <th>mailing_street2<\/th>\n      <th>business_city<\/th>\n      <th>business_state<\/th>\n      <th>business_zip<\/th>\n      <th>business_street<\/th>\n      <th>business_street2<\/th>\n      <th>business_phone<\/th>\n      <th>formerly<\/th>\n      <th>mailing_street1<\/th>\n      <th>business_street1<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"pageLength":10,"scrollX":"800px","columnDefs":[{"className":"dt-right","targets":[1,6,8,11,12,17,18,21,22,23,30,31,36]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false}},"evals":[],"jsHooks":[]}</script>
```



<!-- Summe Investitionen Fossile Pictet: -->

<!-- ```{r} -->
<!-- pictet_oil_and_gas_raw %>% -->
<!--   group_by(investment_discretion) %>%  -->
<!--   summarize(sum_value = sum(value)) -->
<!-- ``` -->

<!-- - Eventuell aufgrund von `DFND`? Oder doch noch nicht auf dem Gruppenlevel? -->

<!-- ## Vergleich der Werte, wenn anderer Threshold gewählt wird -->

<!-- ```{r} -->
<!-- anteil_intensiver_als_threshold <- filings %>%  -->
<!--   group_by(period_of_report, bank_name, which_quantile) %>% -->
<!--   summarize(sum_by_quantile = sum(value_usd_thousands)) %>%  -->
<!--   group_by(period_of_report, bank_name) %>% -->
<!--   arrange(desc(which_quantile)) %>%  -->
<!--   mutate( -->
<!--     perc = sum_by_quantile/sum(sum_by_quantile) * 100, -->
<!--     sum_of_perc = cumsum(perc), -->
<!--     ) %>%  -->
<!--   ungroup() %>% -->
<!--   filter(which_quantile >= 80) %>% -->
<!--   arrange(period_of_report, desc(which_quantile)) -->
<!-- ``` -->

<!-- ```{r} -->
<!-- anteil_intensiver_als_threshold %>% -->
<!--   mutate(which_quantile = as.character(which_quantile)) %>%  -->
<!--   ggplot(aes(period_of_report, sum_of_perc, color = which_quantile)) + -->
<!--   geom_line() + -->
<!--   facet_wrap(~bank_name) -->
<!-- ``` -->

## Zahlen für den Text {#textnumbers}



-   Anzahl Firmen im Sample: (6993)
-   Anzahl Firmen, die geschätzte CO2-Werte aufweisen: (3958)
-   Summe aller Investitionen über gesamten Zeitraum: 11.94 Billionen
-   Durchschnittliche Summe pro Filing: 411.77 Milliarden
-   Emissionsintensivste 15 %
    -   Durchschnittliche Reduktion des Anteils der emissionsintensivsten 15 %, nicht kapitalgewichtet: -5.4
    -   Mediane Reduktion des Anteils der emissionsintensivsten 15 %, nicht kapitalgewichtet: -6.9
-   Öl, Gas und Kohle
    -   Medianer Anteil Öl, Gas und Kohle 2014: 11.8
    -   Medianer Anteil Öl, Gas und Kohle 2021: 2.8
    -   Durchschnittliche Reduktion Anteil Öl, Gas und Kohle (nicht kapital-gewichtet): -9.7
    -   Mediane Reduktion Anteil Öl, Gas und Kohle (nicht kapital-gewichtet): -10.6
    -   Gesamtwerte Investments: 704 Mia. US-Dollar

Summen aller untersuchten Banken im Sample per Q2/2021:


* 15 % emissionsintensivste Unternehmen 63.156 Mia. USD 
* CO2-intensivste Industrien 59.258 Mia. USD 
* Erdöl, Erdgas und Kohle 18.969 Mia. USD

Summen aller untersuchten Banken im Sample per Q2/2014:


* 15 % emissionsintensivste Unternehmen 47.911 Mia. USD 
* CO2-intensivste Industrien 59.414 Mia. USD 
* Erdöl, Erdgas und Kohle 41.145 Mia. USD

### Nextera Energy


```{=html}
<div id="ctrtnqrqba" style="overflow-x:auto;overflow-y:auto;width:auto;height:auto;">
<style>html {
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Helvetica Neue', 'Fira Sans', 'Droid Sans', Arial, sans-serif;
}

#ctrtnqrqba .gt_table {
  display: table;
  border-collapse: collapse;
  margin-left: auto;
  margin-right: auto;
  color: #333333;
  font-size: 16px;
  font-weight: normal;
  font-style: normal;
  background-color: #FFFFFF;
  width: auto;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #A8A8A8;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #A8A8A8;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
}

#ctrtnqrqba .gt_heading {
  background-color: #FFFFFF;
  text-align: center;
  border-bottom-color: #FFFFFF;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#ctrtnqrqba .gt_title {
  color: #333333;
  font-size: 125%;
  font-weight: initial;
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 5px;
  padding-right: 5px;
  border-bottom-color: #FFFFFF;
  border-bottom-width: 0;
}

#ctrtnqrqba .gt_subtitle {
  color: #333333;
  font-size: 85%;
  font-weight: initial;
  padding-top: 0;
  padding-bottom: 6px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-color: #FFFFFF;
  border-top-width: 0;
}

#ctrtnqrqba .gt_bottom_border {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#ctrtnqrqba .gt_col_headings {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#ctrtnqrqba .gt_col_heading {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 6px;
  padding-left: 5px;
  padding-right: 5px;
  overflow-x: hidden;
}

#ctrtnqrqba .gt_column_spanner_outer {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  padding-top: 0;
  padding-bottom: 0;
  padding-left: 4px;
  padding-right: 4px;
}

#ctrtnqrqba .gt_column_spanner_outer:first-child {
  padding-left: 0;
}

#ctrtnqrqba .gt_column_spanner_outer:last-child {
  padding-right: 0;
}

#ctrtnqrqba .gt_column_spanner {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 5px;
  overflow-x: hidden;
  display: inline-block;
  width: 100%;
}

#ctrtnqrqba .gt_group_heading {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
}

#ctrtnqrqba .gt_empty_group_heading {
  padding: 0.5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: middle;
}

#ctrtnqrqba .gt_from_md > :first-child {
  margin-top: 0;
}

#ctrtnqrqba .gt_from_md > :last-child {
  margin-bottom: 0;
}

#ctrtnqrqba .gt_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  margin: 10px;
  border-top-style: solid;
  border-top-width: 1px;
  border-top-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
  overflow-x: hidden;
}

#ctrtnqrqba .gt_stub {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 5px;
  padding-right: 5px;
}

#ctrtnqrqba .gt_stub_row_group {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 5px;
  padding-right: 5px;
  vertical-align: top;
}

#ctrtnqrqba .gt_row_group_first td {
  border-top-width: 2px;
}

#ctrtnqrqba .gt_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#ctrtnqrqba .gt_first_summary_row {
  border-top-style: solid;
  border-top-color: #D3D3D3;
}

#ctrtnqrqba .gt_first_summary_row.thick {
  border-top-width: 2px;
}

#ctrtnqrqba .gt_last_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#ctrtnqrqba .gt_grand_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#ctrtnqrqba .gt_first_grand_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-style: double;
  border-top-width: 6px;
  border-top-color: #D3D3D3;
}

#ctrtnqrqba .gt_striped {
  background-color: rgba(128, 128, 128, 0.05);
}

#ctrtnqrqba .gt_table_body {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#ctrtnqrqba .gt_footnotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#ctrtnqrqba .gt_footnote {
  margin: 0px;
  font-size: 90%;
  padding-left: 4px;
  padding-right: 4px;
  padding-left: 5px;
  padding-right: 5px;
}

#ctrtnqrqba .gt_sourcenotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#ctrtnqrqba .gt_sourcenote {
  font-size: 90%;
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 5px;
  padding-right: 5px;
}

#ctrtnqrqba .gt_left {
  text-align: left;
}

#ctrtnqrqba .gt_center {
  text-align: center;
}

#ctrtnqrqba .gt_right {
  text-align: right;
  font-variant-numeric: tabular-nums;
}

#ctrtnqrqba .gt_font_normal {
  font-weight: normal;
}

#ctrtnqrqba .gt_font_bold {
  font-weight: bold;
}

#ctrtnqrqba .gt_font_italic {
  font-style: italic;
}

#ctrtnqrqba .gt_super {
  font-size: 65%;
}

#ctrtnqrqba .gt_footnote_marks {
  font-style: italic;
  font-weight: normal;
  font-size: 75%;
  vertical-align: 0.4em;
}

#ctrtnqrqba .gt_asterisk {
  font-size: 100%;
  vertical-align: 0;
}

#ctrtnqrqba .gt_slash_mark {
  font-size: 0.7em;
  line-height: 0.7em;
  vertical-align: 0.15em;
}

#ctrtnqrqba .gt_fraction_numerator {
  font-size: 0.6em;
  line-height: 0.6em;
  vertical-align: 0.45em;
}

#ctrtnqrqba .gt_fraction_denominator {
  font-size: 0.6em;
  line-height: 0.6em;
  vertical-align: -0.05em;
}
</style>
<table class="gt_table">
  <caption>(#tab:unnamed-chunk-67)Investitionen in NextEra Energy, nach Bank, Q2/2021</caption>
  
  <thead class="gt_col_headings">
    <tr>
      <th class="gt_col_heading gt_columns_bottom_border gt_left" rowspan="1" colspan="1">bank_name</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1">value_usd_mns</th>
    </tr>
  </thead>
  <tbody class="gt_table_body">
    <tr><td class="gt_row gt_left">ZKB</td>
<td class="gt_row gt_right">30.998</td></tr>
    <tr><td class="gt_row gt_left">Lombard Odier</td>
<td class="gt_row gt_right">47.739</td></tr>
    <tr><td class="gt_row gt_left">Credit Suisse</td>
<td class="gt_row gt_right">360.205</td></tr>
    <tr><td class="gt_row gt_left">Pictet</td>
<td class="gt_row gt_right">827.470</td></tr>
    <tr><td class="gt_row gt_left">UBS</td>
<td class="gt_row gt_right">1858.143</td></tr>
  </tbody>
  
  
</table>
</div>
```

- Summe der Investments in Nextera: 3124.554854 Mio. USD


```{=html}
<div id="yxrxbxemsr" style="overflow-x:auto;overflow-y:auto;width:auto;height:auto;">
<style>html {
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Helvetica Neue', 'Fira Sans', 'Droid Sans', Arial, sans-serif;
}

#yxrxbxemsr .gt_table {
  display: table;
  border-collapse: collapse;
  margin-left: auto;
  margin-right: auto;
  color: #333333;
  font-size: 16px;
  font-weight: normal;
  font-style: normal;
  background-color: #FFFFFF;
  width: auto;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #A8A8A8;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #A8A8A8;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
}

#yxrxbxemsr .gt_heading {
  background-color: #FFFFFF;
  text-align: center;
  border-bottom-color: #FFFFFF;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#yxrxbxemsr .gt_title {
  color: #333333;
  font-size: 125%;
  font-weight: initial;
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 5px;
  padding-right: 5px;
  border-bottom-color: #FFFFFF;
  border-bottom-width: 0;
}

#yxrxbxemsr .gt_subtitle {
  color: #333333;
  font-size: 85%;
  font-weight: initial;
  padding-top: 0;
  padding-bottom: 6px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-color: #FFFFFF;
  border-top-width: 0;
}

#yxrxbxemsr .gt_bottom_border {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#yxrxbxemsr .gt_col_headings {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#yxrxbxemsr .gt_col_heading {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 6px;
  padding-left: 5px;
  padding-right: 5px;
  overflow-x: hidden;
}

#yxrxbxemsr .gt_column_spanner_outer {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  padding-top: 0;
  padding-bottom: 0;
  padding-left: 4px;
  padding-right: 4px;
}

#yxrxbxemsr .gt_column_spanner_outer:first-child {
  padding-left: 0;
}

#yxrxbxemsr .gt_column_spanner_outer:last-child {
  padding-right: 0;
}

#yxrxbxemsr .gt_column_spanner {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 5px;
  overflow-x: hidden;
  display: inline-block;
  width: 100%;
}

#yxrxbxemsr .gt_group_heading {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
}

#yxrxbxemsr .gt_empty_group_heading {
  padding: 0.5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: middle;
}

#yxrxbxemsr .gt_from_md > :first-child {
  margin-top: 0;
}

#yxrxbxemsr .gt_from_md > :last-child {
  margin-bottom: 0;
}

#yxrxbxemsr .gt_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  margin: 10px;
  border-top-style: solid;
  border-top-width: 1px;
  border-top-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
  overflow-x: hidden;
}

#yxrxbxemsr .gt_stub {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 5px;
  padding-right: 5px;
}

#yxrxbxemsr .gt_stub_row_group {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 5px;
  padding-right: 5px;
  vertical-align: top;
}

#yxrxbxemsr .gt_row_group_first td {
  border-top-width: 2px;
}

#yxrxbxemsr .gt_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#yxrxbxemsr .gt_first_summary_row {
  border-top-style: solid;
  border-top-color: #D3D3D3;
}

#yxrxbxemsr .gt_first_summary_row.thick {
  border-top-width: 2px;
}

#yxrxbxemsr .gt_last_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#yxrxbxemsr .gt_grand_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#yxrxbxemsr .gt_first_grand_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-style: double;
  border-top-width: 6px;
  border-top-color: #D3D3D3;
}

#yxrxbxemsr .gt_striped {
  background-color: rgba(128, 128, 128, 0.05);
}

#yxrxbxemsr .gt_table_body {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#yxrxbxemsr .gt_footnotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#yxrxbxemsr .gt_footnote {
  margin: 0px;
  font-size: 90%;
  padding-left: 4px;
  padding-right: 4px;
  padding-left: 5px;
  padding-right: 5px;
}

#yxrxbxemsr .gt_sourcenotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#yxrxbxemsr .gt_sourcenote {
  font-size: 90%;
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 5px;
  padding-right: 5px;
}

#yxrxbxemsr .gt_left {
  text-align: left;
}

#yxrxbxemsr .gt_center {
  text-align: center;
}

#yxrxbxemsr .gt_right {
  text-align: right;
  font-variant-numeric: tabular-nums;
}

#yxrxbxemsr .gt_font_normal {
  font-weight: normal;
}

#yxrxbxemsr .gt_font_bold {
  font-weight: bold;
}

#yxrxbxemsr .gt_font_italic {
  font-style: italic;
}

#yxrxbxemsr .gt_super {
  font-size: 65%;
}

#yxrxbxemsr .gt_footnote_marks {
  font-style: italic;
  font-weight: normal;
  font-size: 75%;
  vertical-align: 0.4em;
}

#yxrxbxemsr .gt_asterisk {
  font-size: 100%;
  vertical-align: 0;
}

#yxrxbxemsr .gt_slash_mark {
  font-size: 0.7em;
  line-height: 0.7em;
  vertical-align: 0.15em;
}

#yxrxbxemsr .gt_fraction_numerator {
  font-size: 0.6em;
  line-height: 0.6em;
  vertical-align: 0.45em;
}

#yxrxbxemsr .gt_fraction_denominator {
  font-size: 0.6em;
  line-height: 0.6em;
  vertical-align: -0.05em;
}
</style>
<table class="gt_table">
  <caption>(#tab:unnamed-chunk-68)Emissionsintensität NextEra vs. andere emissionsintensive Firmen</caption>
  
  <thead class="gt_col_headings">
    <tr>
      <th class="gt_col_heading gt_columns_bottom_border gt_left" rowspan="1" colspan="1">company_common_name</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1">tot_co2_by_revenue</th>
    </tr>
  </thead>
  <tbody class="gt_table_body">
    <tr><td class="gt_row gt_left">Exxon Mobil Corp</td>
<td class="gt_row gt_right">420.1092</td></tr>
    <tr><td class="gt_row gt_left">Nextera Energy Inc</td>
<td class="gt_row gt_right">2538.2916</td></tr>
    <tr><td class="gt_row gt_left">Ryanair Holdings PLC</td>
<td class="gt_row gt_right">2624.2550</td></tr>
  </tbody>
  
  
</table>
</div>
```

### Grösste Positionen (fossile Brennstoffe)


```{=html}
<div id="ctyxupnwbu" style="overflow-x:auto;overflow-y:auto;width:auto;height:auto;">
<style>html {
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Helvetica Neue', 'Fira Sans', 'Droid Sans', Arial, sans-serif;
}

#ctyxupnwbu .gt_table {
  display: table;
  border-collapse: collapse;
  margin-left: auto;
  margin-right: auto;
  color: #333333;
  font-size: 16px;
  font-weight: normal;
  font-style: normal;
  background-color: #FFFFFF;
  width: auto;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #A8A8A8;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #A8A8A8;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
}

#ctyxupnwbu .gt_heading {
  background-color: #FFFFFF;
  text-align: center;
  border-bottom-color: #FFFFFF;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#ctyxupnwbu .gt_title {
  color: #333333;
  font-size: 125%;
  font-weight: initial;
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 5px;
  padding-right: 5px;
  border-bottom-color: #FFFFFF;
  border-bottom-width: 0;
}

#ctyxupnwbu .gt_subtitle {
  color: #333333;
  font-size: 85%;
  font-weight: initial;
  padding-top: 0;
  padding-bottom: 6px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-color: #FFFFFF;
  border-top-width: 0;
}

#ctyxupnwbu .gt_bottom_border {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#ctyxupnwbu .gt_col_headings {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#ctyxupnwbu .gt_col_heading {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 6px;
  padding-left: 5px;
  padding-right: 5px;
  overflow-x: hidden;
}

#ctyxupnwbu .gt_column_spanner_outer {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  padding-top: 0;
  padding-bottom: 0;
  padding-left: 4px;
  padding-right: 4px;
}

#ctyxupnwbu .gt_column_spanner_outer:first-child {
  padding-left: 0;
}

#ctyxupnwbu .gt_column_spanner_outer:last-child {
  padding-right: 0;
}

#ctyxupnwbu .gt_column_spanner {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 5px;
  overflow-x: hidden;
  display: inline-block;
  width: 100%;
}

#ctyxupnwbu .gt_group_heading {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
}

#ctyxupnwbu .gt_empty_group_heading {
  padding: 0.5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: middle;
}

#ctyxupnwbu .gt_from_md > :first-child {
  margin-top: 0;
}

#ctyxupnwbu .gt_from_md > :last-child {
  margin-bottom: 0;
}

#ctyxupnwbu .gt_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  margin: 10px;
  border-top-style: solid;
  border-top-width: 1px;
  border-top-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
  overflow-x: hidden;
}

#ctyxupnwbu .gt_stub {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 5px;
  padding-right: 5px;
}

#ctyxupnwbu .gt_stub_row_group {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 5px;
  padding-right: 5px;
  vertical-align: top;
}

#ctyxupnwbu .gt_row_group_first td {
  border-top-width: 2px;
}

#ctyxupnwbu .gt_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#ctyxupnwbu .gt_first_summary_row {
  border-top-style: solid;
  border-top-color: #D3D3D3;
}

#ctyxupnwbu .gt_first_summary_row.thick {
  border-top-width: 2px;
}

#ctyxupnwbu .gt_last_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#ctyxupnwbu .gt_grand_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#ctyxupnwbu .gt_first_grand_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-style: double;
  border-top-width: 6px;
  border-top-color: #D3D3D3;
}

#ctyxupnwbu .gt_striped {
  background-color: rgba(128, 128, 128, 0.05);
}

#ctyxupnwbu .gt_table_body {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#ctyxupnwbu .gt_footnotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#ctyxupnwbu .gt_footnote {
  margin: 0px;
  font-size: 90%;
  padding-left: 4px;
  padding-right: 4px;
  padding-left: 5px;
  padding-right: 5px;
}

#ctyxupnwbu .gt_sourcenotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#ctyxupnwbu .gt_sourcenote {
  font-size: 90%;
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 5px;
  padding-right: 5px;
}

#ctyxupnwbu .gt_left {
  text-align: left;
}

#ctyxupnwbu .gt_center {
  text-align: center;
}

#ctyxupnwbu .gt_right {
  text-align: right;
  font-variant-numeric: tabular-nums;
}

#ctyxupnwbu .gt_font_normal {
  font-weight: normal;
}

#ctyxupnwbu .gt_font_bold {
  font-weight: bold;
}

#ctyxupnwbu .gt_font_italic {
  font-style: italic;
}

#ctyxupnwbu .gt_super {
  font-size: 65%;
}

#ctyxupnwbu .gt_footnote_marks {
  font-style: italic;
  font-weight: normal;
  font-size: 75%;
  vertical-align: 0.4em;
}

#ctyxupnwbu .gt_asterisk {
  font-size: 100%;
  vertical-align: 0;
}

#ctyxupnwbu .gt_slash_mark {
  font-size: 0.7em;
  line-height: 0.7em;
  vertical-align: 0.15em;
}

#ctyxupnwbu .gt_fraction_numerator {
  font-size: 0.6em;
  line-height: 0.6em;
  vertical-align: 0.45em;
}

#ctyxupnwbu .gt_fraction_denominator {
  font-size: 0.6em;
  line-height: 0.6em;
  vertical-align: -0.05em;
}
</style>
<table class="gt_table">
  <caption>(#tab:unnamed-chunk-69)10 Grösste Investitionen in fossile Brennstoffe: Credit Suisse , Q2/2021</caption>
  
  <thead class="gt_col_headings">
    <tr>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1">Mio. USD</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_left" rowspan="1" colspan="1">Unternehmen</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_left" rowspan="1" colspan="1">GICS Subindustrie</th>
    </tr>
  </thead>
  <tbody class="gt_table_body">
    <tr><td class="gt_row gt_right">610.373</td>
<td class="gt_row gt_left">Exxon Mobil Corp</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">567.717</td>
<td class="gt_row gt_left">Chevron Corp</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">211.194</td>
<td class="gt_row gt_left">Conocophillips</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">152.363</td>
<td class="gt_row gt_left">Schlumberger NV</td>
<td class="gt_row gt_left">Energiezubehör und Dienste</td></tr>
    <tr><td class="gt_row gt_right">150.456</td>
<td class="gt_row gt_left">Enbridge Inc</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">147.255</td>
<td class="gt_row gt_left">EOG Resources Inc</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">132.123</td>
<td class="gt_row gt_left">Occidental Petroleum Corp</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">121.986</td>
<td class="gt_row gt_left">TC Energy Corp</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">121.759</td>
<td class="gt_row gt_left">Suncor Energy Inc</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">109.779</td>
<td class="gt_row gt_left">Pioneer Natural Resources Co</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
  </tbody>
  
  
</table>
</div>
<div id="kcnshhnyca" style="overflow-x:auto;overflow-y:auto;width:auto;height:auto;">
<style>html {
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Helvetica Neue', 'Fira Sans', 'Droid Sans', Arial, sans-serif;
}

#kcnshhnyca .gt_table {
  display: table;
  border-collapse: collapse;
  margin-left: auto;
  margin-right: auto;
  color: #333333;
  font-size: 16px;
  font-weight: normal;
  font-style: normal;
  background-color: #FFFFFF;
  width: auto;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #A8A8A8;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #A8A8A8;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
}

#kcnshhnyca .gt_heading {
  background-color: #FFFFFF;
  text-align: center;
  border-bottom-color: #FFFFFF;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#kcnshhnyca .gt_title {
  color: #333333;
  font-size: 125%;
  font-weight: initial;
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 5px;
  padding-right: 5px;
  border-bottom-color: #FFFFFF;
  border-bottom-width: 0;
}

#kcnshhnyca .gt_subtitle {
  color: #333333;
  font-size: 85%;
  font-weight: initial;
  padding-top: 0;
  padding-bottom: 6px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-color: #FFFFFF;
  border-top-width: 0;
}

#kcnshhnyca .gt_bottom_border {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#kcnshhnyca .gt_col_headings {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#kcnshhnyca .gt_col_heading {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 6px;
  padding-left: 5px;
  padding-right: 5px;
  overflow-x: hidden;
}

#kcnshhnyca .gt_column_spanner_outer {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  padding-top: 0;
  padding-bottom: 0;
  padding-left: 4px;
  padding-right: 4px;
}

#kcnshhnyca .gt_column_spanner_outer:first-child {
  padding-left: 0;
}

#kcnshhnyca .gt_column_spanner_outer:last-child {
  padding-right: 0;
}

#kcnshhnyca .gt_column_spanner {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 5px;
  overflow-x: hidden;
  display: inline-block;
  width: 100%;
}

#kcnshhnyca .gt_group_heading {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
}

#kcnshhnyca .gt_empty_group_heading {
  padding: 0.5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: middle;
}

#kcnshhnyca .gt_from_md > :first-child {
  margin-top: 0;
}

#kcnshhnyca .gt_from_md > :last-child {
  margin-bottom: 0;
}

#kcnshhnyca .gt_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  margin: 10px;
  border-top-style: solid;
  border-top-width: 1px;
  border-top-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
  overflow-x: hidden;
}

#kcnshhnyca .gt_stub {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 5px;
  padding-right: 5px;
}

#kcnshhnyca .gt_stub_row_group {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 5px;
  padding-right: 5px;
  vertical-align: top;
}

#kcnshhnyca .gt_row_group_first td {
  border-top-width: 2px;
}

#kcnshhnyca .gt_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#kcnshhnyca .gt_first_summary_row {
  border-top-style: solid;
  border-top-color: #D3D3D3;
}

#kcnshhnyca .gt_first_summary_row.thick {
  border-top-width: 2px;
}

#kcnshhnyca .gt_last_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#kcnshhnyca .gt_grand_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#kcnshhnyca .gt_first_grand_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-style: double;
  border-top-width: 6px;
  border-top-color: #D3D3D3;
}

#kcnshhnyca .gt_striped {
  background-color: rgba(128, 128, 128, 0.05);
}

#kcnshhnyca .gt_table_body {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#kcnshhnyca .gt_footnotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#kcnshhnyca .gt_footnote {
  margin: 0px;
  font-size: 90%;
  padding-left: 4px;
  padding-right: 4px;
  padding-left: 5px;
  padding-right: 5px;
}

#kcnshhnyca .gt_sourcenotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#kcnshhnyca .gt_sourcenote {
  font-size: 90%;
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 5px;
  padding-right: 5px;
}

#kcnshhnyca .gt_left {
  text-align: left;
}

#kcnshhnyca .gt_center {
  text-align: center;
}

#kcnshhnyca .gt_right {
  text-align: right;
  font-variant-numeric: tabular-nums;
}

#kcnshhnyca .gt_font_normal {
  font-weight: normal;
}

#kcnshhnyca .gt_font_bold {
  font-weight: bold;
}

#kcnshhnyca .gt_font_italic {
  font-style: italic;
}

#kcnshhnyca .gt_super {
  font-size: 65%;
}

#kcnshhnyca .gt_footnote_marks {
  font-style: italic;
  font-weight: normal;
  font-size: 75%;
  vertical-align: 0.4em;
}

#kcnshhnyca .gt_asterisk {
  font-size: 100%;
  vertical-align: 0;
}

#kcnshhnyca .gt_slash_mark {
  font-size: 0.7em;
  line-height: 0.7em;
  vertical-align: 0.15em;
}

#kcnshhnyca .gt_fraction_numerator {
  font-size: 0.6em;
  line-height: 0.6em;
  vertical-align: 0.45em;
}

#kcnshhnyca .gt_fraction_denominator {
  font-size: 0.6em;
  line-height: 0.6em;
  vertical-align: -0.05em;
}
</style>
<table class="gt_table">
  <caption>(#tab:unnamed-chunk-69)10 Grösste Investitionen in fossile Brennstoffe: Lombard Odier , Q2/2021</caption>
  
  <thead class="gt_col_headings">
    <tr>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1">Mio. USD</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_left" rowspan="1" colspan="1">Unternehmen</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_left" rowspan="1" colspan="1">GICS Subindustrie</th>
    </tr>
  </thead>
  <tbody class="gt_table_body">
    <tr><td class="gt_row gt_right">22.373</td>
<td class="gt_row gt_left">Occidental Petroleum Corp</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">16.707</td>
<td class="gt_row gt_left">Exxon Mobil Corp</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">8.283</td>
<td class="gt_row gt_left">Renewable Energy Group Inc</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">5.898</td>
<td class="gt_row gt_left">Chevron Corp</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">5.191</td>
<td class="gt_row gt_left">APA Corp (US)</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">3.436</td>
<td class="gt_row gt_left">Enbridge Inc</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">3.435</td>
<td class="gt_row gt_left">Halliburton Co</td>
<td class="gt_row gt_left">Energiezubehör und Dienste</td></tr>
    <tr><td class="gt_row gt_right">2.146</td>
<td class="gt_row gt_left">Phillips 66</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">1.898</td>
<td class="gt_row gt_left">Baker Hughes Co</td>
<td class="gt_row gt_left">Energiezubehör und Dienste</td></tr>
    <tr><td class="gt_row gt_right">1.667</td>
<td class="gt_row gt_left">Kinder Morgan Inc</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
  </tbody>
  
  
</table>
</div>
<div id="wopiweowjg" style="overflow-x:auto;overflow-y:auto;width:auto;height:auto;">
<style>html {
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Helvetica Neue', 'Fira Sans', 'Droid Sans', Arial, sans-serif;
}

#wopiweowjg .gt_table {
  display: table;
  border-collapse: collapse;
  margin-left: auto;
  margin-right: auto;
  color: #333333;
  font-size: 16px;
  font-weight: normal;
  font-style: normal;
  background-color: #FFFFFF;
  width: auto;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #A8A8A8;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #A8A8A8;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
}

#wopiweowjg .gt_heading {
  background-color: #FFFFFF;
  text-align: center;
  border-bottom-color: #FFFFFF;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#wopiweowjg .gt_title {
  color: #333333;
  font-size: 125%;
  font-weight: initial;
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 5px;
  padding-right: 5px;
  border-bottom-color: #FFFFFF;
  border-bottom-width: 0;
}

#wopiweowjg .gt_subtitle {
  color: #333333;
  font-size: 85%;
  font-weight: initial;
  padding-top: 0;
  padding-bottom: 6px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-color: #FFFFFF;
  border-top-width: 0;
}

#wopiweowjg .gt_bottom_border {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#wopiweowjg .gt_col_headings {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#wopiweowjg .gt_col_heading {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 6px;
  padding-left: 5px;
  padding-right: 5px;
  overflow-x: hidden;
}

#wopiweowjg .gt_column_spanner_outer {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  padding-top: 0;
  padding-bottom: 0;
  padding-left: 4px;
  padding-right: 4px;
}

#wopiweowjg .gt_column_spanner_outer:first-child {
  padding-left: 0;
}

#wopiweowjg .gt_column_spanner_outer:last-child {
  padding-right: 0;
}

#wopiweowjg .gt_column_spanner {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 5px;
  overflow-x: hidden;
  display: inline-block;
  width: 100%;
}

#wopiweowjg .gt_group_heading {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
}

#wopiweowjg .gt_empty_group_heading {
  padding: 0.5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: middle;
}

#wopiweowjg .gt_from_md > :first-child {
  margin-top: 0;
}

#wopiweowjg .gt_from_md > :last-child {
  margin-bottom: 0;
}

#wopiweowjg .gt_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  margin: 10px;
  border-top-style: solid;
  border-top-width: 1px;
  border-top-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
  overflow-x: hidden;
}

#wopiweowjg .gt_stub {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 5px;
  padding-right: 5px;
}

#wopiweowjg .gt_stub_row_group {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 5px;
  padding-right: 5px;
  vertical-align: top;
}

#wopiweowjg .gt_row_group_first td {
  border-top-width: 2px;
}

#wopiweowjg .gt_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#wopiweowjg .gt_first_summary_row {
  border-top-style: solid;
  border-top-color: #D3D3D3;
}

#wopiweowjg .gt_first_summary_row.thick {
  border-top-width: 2px;
}

#wopiweowjg .gt_last_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#wopiweowjg .gt_grand_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#wopiweowjg .gt_first_grand_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-style: double;
  border-top-width: 6px;
  border-top-color: #D3D3D3;
}

#wopiweowjg .gt_striped {
  background-color: rgba(128, 128, 128, 0.05);
}

#wopiweowjg .gt_table_body {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#wopiweowjg .gt_footnotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#wopiweowjg .gt_footnote {
  margin: 0px;
  font-size: 90%;
  padding-left: 4px;
  padding-right: 4px;
  padding-left: 5px;
  padding-right: 5px;
}

#wopiweowjg .gt_sourcenotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#wopiweowjg .gt_sourcenote {
  font-size: 90%;
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 5px;
  padding-right: 5px;
}

#wopiweowjg .gt_left {
  text-align: left;
}

#wopiweowjg .gt_center {
  text-align: center;
}

#wopiweowjg .gt_right {
  text-align: right;
  font-variant-numeric: tabular-nums;
}

#wopiweowjg .gt_font_normal {
  font-weight: normal;
}

#wopiweowjg .gt_font_bold {
  font-weight: bold;
}

#wopiweowjg .gt_font_italic {
  font-style: italic;
}

#wopiweowjg .gt_super {
  font-size: 65%;
}

#wopiweowjg .gt_footnote_marks {
  font-style: italic;
  font-weight: normal;
  font-size: 75%;
  vertical-align: 0.4em;
}

#wopiweowjg .gt_asterisk {
  font-size: 100%;
  vertical-align: 0;
}

#wopiweowjg .gt_slash_mark {
  font-size: 0.7em;
  line-height: 0.7em;
  vertical-align: 0.15em;
}

#wopiweowjg .gt_fraction_numerator {
  font-size: 0.6em;
  line-height: 0.6em;
  vertical-align: 0.45em;
}

#wopiweowjg .gt_fraction_denominator {
  font-size: 0.6em;
  line-height: 0.6em;
  vertical-align: -0.05em;
}
</style>
<table class="gt_table">
  <caption>(#tab:unnamed-chunk-69)10 Grösste Investitionen in fossile Brennstoffe: Pictet , Q2/2021</caption>
  
  <thead class="gt_col_headings">
    <tr>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1">Mio. USD</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_left" rowspan="1" colspan="1">Unternehmen</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_left" rowspan="1" colspan="1">GICS Subindustrie</th>
    </tr>
  </thead>
  <tbody class="gt_table_body">
    <tr><td class="gt_row gt_right">111.858</td>
<td class="gt_row gt_left">Exxon Mobil Corp</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">111.765</td>
<td class="gt_row gt_left">Chevron Corp</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">50.903</td>
<td class="gt_row gt_left">Conocophillips</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">39.137</td>
<td class="gt_row gt_left">Gulfport Energy Corp</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">34.411</td>
<td class="gt_row gt_left">Enbridge Inc</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">29.564</td>
<td class="gt_row gt_left">EOG Resources Inc</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">25.815</td>
<td class="gt_row gt_left">California Resources Corp</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">22.439</td>
<td class="gt_row gt_left">Occidental Petroleum Corp</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">20.696</td>
<td class="gt_row gt_left">Schlumberger NV</td>
<td class="gt_row gt_left">Energiezubehör und Dienste</td></tr>
    <tr><td class="gt_row gt_right">18.416</td>
<td class="gt_row gt_left">Marathon Petroleum Corp</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
  </tbody>
  
  
</table>
</div>
<div id="mhzkfyxuys" style="overflow-x:auto;overflow-y:auto;width:auto;height:auto;">
<style>html {
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Helvetica Neue', 'Fira Sans', 'Droid Sans', Arial, sans-serif;
}

#mhzkfyxuys .gt_table {
  display: table;
  border-collapse: collapse;
  margin-left: auto;
  margin-right: auto;
  color: #333333;
  font-size: 16px;
  font-weight: normal;
  font-style: normal;
  background-color: #FFFFFF;
  width: auto;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #A8A8A8;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #A8A8A8;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
}

#mhzkfyxuys .gt_heading {
  background-color: #FFFFFF;
  text-align: center;
  border-bottom-color: #FFFFFF;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#mhzkfyxuys .gt_title {
  color: #333333;
  font-size: 125%;
  font-weight: initial;
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 5px;
  padding-right: 5px;
  border-bottom-color: #FFFFFF;
  border-bottom-width: 0;
}

#mhzkfyxuys .gt_subtitle {
  color: #333333;
  font-size: 85%;
  font-weight: initial;
  padding-top: 0;
  padding-bottom: 6px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-color: #FFFFFF;
  border-top-width: 0;
}

#mhzkfyxuys .gt_bottom_border {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#mhzkfyxuys .gt_col_headings {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#mhzkfyxuys .gt_col_heading {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 6px;
  padding-left: 5px;
  padding-right: 5px;
  overflow-x: hidden;
}

#mhzkfyxuys .gt_column_spanner_outer {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  padding-top: 0;
  padding-bottom: 0;
  padding-left: 4px;
  padding-right: 4px;
}

#mhzkfyxuys .gt_column_spanner_outer:first-child {
  padding-left: 0;
}

#mhzkfyxuys .gt_column_spanner_outer:last-child {
  padding-right: 0;
}

#mhzkfyxuys .gt_column_spanner {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 5px;
  overflow-x: hidden;
  display: inline-block;
  width: 100%;
}

#mhzkfyxuys .gt_group_heading {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
}

#mhzkfyxuys .gt_empty_group_heading {
  padding: 0.5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: middle;
}

#mhzkfyxuys .gt_from_md > :first-child {
  margin-top: 0;
}

#mhzkfyxuys .gt_from_md > :last-child {
  margin-bottom: 0;
}

#mhzkfyxuys .gt_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  margin: 10px;
  border-top-style: solid;
  border-top-width: 1px;
  border-top-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
  overflow-x: hidden;
}

#mhzkfyxuys .gt_stub {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 5px;
  padding-right: 5px;
}

#mhzkfyxuys .gt_stub_row_group {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 5px;
  padding-right: 5px;
  vertical-align: top;
}

#mhzkfyxuys .gt_row_group_first td {
  border-top-width: 2px;
}

#mhzkfyxuys .gt_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#mhzkfyxuys .gt_first_summary_row {
  border-top-style: solid;
  border-top-color: #D3D3D3;
}

#mhzkfyxuys .gt_first_summary_row.thick {
  border-top-width: 2px;
}

#mhzkfyxuys .gt_last_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#mhzkfyxuys .gt_grand_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#mhzkfyxuys .gt_first_grand_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-style: double;
  border-top-width: 6px;
  border-top-color: #D3D3D3;
}

#mhzkfyxuys .gt_striped {
  background-color: rgba(128, 128, 128, 0.05);
}

#mhzkfyxuys .gt_table_body {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#mhzkfyxuys .gt_footnotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#mhzkfyxuys .gt_footnote {
  margin: 0px;
  font-size: 90%;
  padding-left: 4px;
  padding-right: 4px;
  padding-left: 5px;
  padding-right: 5px;
}

#mhzkfyxuys .gt_sourcenotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#mhzkfyxuys .gt_sourcenote {
  font-size: 90%;
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 5px;
  padding-right: 5px;
}

#mhzkfyxuys .gt_left {
  text-align: left;
}

#mhzkfyxuys .gt_center {
  text-align: center;
}

#mhzkfyxuys .gt_right {
  text-align: right;
  font-variant-numeric: tabular-nums;
}

#mhzkfyxuys .gt_font_normal {
  font-weight: normal;
}

#mhzkfyxuys .gt_font_bold {
  font-weight: bold;
}

#mhzkfyxuys .gt_font_italic {
  font-style: italic;
}

#mhzkfyxuys .gt_super {
  font-size: 65%;
}

#mhzkfyxuys .gt_footnote_marks {
  font-style: italic;
  font-weight: normal;
  font-size: 75%;
  vertical-align: 0.4em;
}

#mhzkfyxuys .gt_asterisk {
  font-size: 100%;
  vertical-align: 0;
}

#mhzkfyxuys .gt_slash_mark {
  font-size: 0.7em;
  line-height: 0.7em;
  vertical-align: 0.15em;
}

#mhzkfyxuys .gt_fraction_numerator {
  font-size: 0.6em;
  line-height: 0.6em;
  vertical-align: 0.45em;
}

#mhzkfyxuys .gt_fraction_denominator {
  font-size: 0.6em;
  line-height: 0.6em;
  vertical-align: -0.05em;
}
</style>
<table class="gt_table">
  <caption>(#tab:unnamed-chunk-69)10 Grösste Investitionen in fossile Brennstoffe: UBS , Q2/2021</caption>
  
  <thead class="gt_col_headings">
    <tr>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1">Mio. USD</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_left" rowspan="1" colspan="1">Unternehmen</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_left" rowspan="1" colspan="1">GICS Subindustrie</th>
    </tr>
  </thead>
  <tbody class="gt_table_body">
    <tr><td class="gt_row gt_right">1575.7702</td>
<td class="gt_row gt_left">Chevron Corp</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">1541.6549</td>
<td class="gt_row gt_left">Exxon Mobil Corp</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">693.0175</td>
<td class="gt_row gt_left">Conocophillips</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">544.3998</td>
<td class="gt_row gt_left">Phillips 66</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">534.5123</td>
<td class="gt_row gt_left">Hess Corp</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">436.9249</td>
<td class="gt_row gt_left">Schlumberger NV</td>
<td class="gt_row gt_left">Energiezubehör und Dienste</td></tr>
    <tr><td class="gt_row gt_right">424.6301</td>
<td class="gt_row gt_left">Marathon Petroleum Corp</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">423.2789</td>
<td class="gt_row gt_left">Williams Companies Inc</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">415.9255</td>
<td class="gt_row gt_left">EOG Resources Inc</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">407.8740</td>
<td class="gt_row gt_left">Petroleo Brasileiro SA Petrobras</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
  </tbody>
  
  
</table>
</div>
<div id="jitjzyytwl" style="overflow-x:auto;overflow-y:auto;width:auto;height:auto;">
<style>html {
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Helvetica Neue', 'Fira Sans', 'Droid Sans', Arial, sans-serif;
}

#jitjzyytwl .gt_table {
  display: table;
  border-collapse: collapse;
  margin-left: auto;
  margin-right: auto;
  color: #333333;
  font-size: 16px;
  font-weight: normal;
  font-style: normal;
  background-color: #FFFFFF;
  width: auto;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #A8A8A8;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #A8A8A8;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
}

#jitjzyytwl .gt_heading {
  background-color: #FFFFFF;
  text-align: center;
  border-bottom-color: #FFFFFF;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#jitjzyytwl .gt_title {
  color: #333333;
  font-size: 125%;
  font-weight: initial;
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 5px;
  padding-right: 5px;
  border-bottom-color: #FFFFFF;
  border-bottom-width: 0;
}

#jitjzyytwl .gt_subtitle {
  color: #333333;
  font-size: 85%;
  font-weight: initial;
  padding-top: 0;
  padding-bottom: 6px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-color: #FFFFFF;
  border-top-width: 0;
}

#jitjzyytwl .gt_bottom_border {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#jitjzyytwl .gt_col_headings {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#jitjzyytwl .gt_col_heading {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 6px;
  padding-left: 5px;
  padding-right: 5px;
  overflow-x: hidden;
}

#jitjzyytwl .gt_column_spanner_outer {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  padding-top: 0;
  padding-bottom: 0;
  padding-left: 4px;
  padding-right: 4px;
}

#jitjzyytwl .gt_column_spanner_outer:first-child {
  padding-left: 0;
}

#jitjzyytwl .gt_column_spanner_outer:last-child {
  padding-right: 0;
}

#jitjzyytwl .gt_column_spanner {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 5px;
  overflow-x: hidden;
  display: inline-block;
  width: 100%;
}

#jitjzyytwl .gt_group_heading {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
}

#jitjzyytwl .gt_empty_group_heading {
  padding: 0.5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: middle;
}

#jitjzyytwl .gt_from_md > :first-child {
  margin-top: 0;
}

#jitjzyytwl .gt_from_md > :last-child {
  margin-bottom: 0;
}

#jitjzyytwl .gt_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  margin: 10px;
  border-top-style: solid;
  border-top-width: 1px;
  border-top-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
  overflow-x: hidden;
}

#jitjzyytwl .gt_stub {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 5px;
  padding-right: 5px;
}

#jitjzyytwl .gt_stub_row_group {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 5px;
  padding-right: 5px;
  vertical-align: top;
}

#jitjzyytwl .gt_row_group_first td {
  border-top-width: 2px;
}

#jitjzyytwl .gt_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#jitjzyytwl .gt_first_summary_row {
  border-top-style: solid;
  border-top-color: #D3D3D3;
}

#jitjzyytwl .gt_first_summary_row.thick {
  border-top-width: 2px;
}

#jitjzyytwl .gt_last_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#jitjzyytwl .gt_grand_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#jitjzyytwl .gt_first_grand_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-style: double;
  border-top-width: 6px;
  border-top-color: #D3D3D3;
}

#jitjzyytwl .gt_striped {
  background-color: rgba(128, 128, 128, 0.05);
}

#jitjzyytwl .gt_table_body {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#jitjzyytwl .gt_footnotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#jitjzyytwl .gt_footnote {
  margin: 0px;
  font-size: 90%;
  padding-left: 4px;
  padding-right: 4px;
  padding-left: 5px;
  padding-right: 5px;
}

#jitjzyytwl .gt_sourcenotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#jitjzyytwl .gt_sourcenote {
  font-size: 90%;
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 5px;
  padding-right: 5px;
}

#jitjzyytwl .gt_left {
  text-align: left;
}

#jitjzyytwl .gt_center {
  text-align: center;
}

#jitjzyytwl .gt_right {
  text-align: right;
  font-variant-numeric: tabular-nums;
}

#jitjzyytwl .gt_font_normal {
  font-weight: normal;
}

#jitjzyytwl .gt_font_bold {
  font-weight: bold;
}

#jitjzyytwl .gt_font_italic {
  font-style: italic;
}

#jitjzyytwl .gt_super {
  font-size: 65%;
}

#jitjzyytwl .gt_footnote_marks {
  font-style: italic;
  font-weight: normal;
  font-size: 75%;
  vertical-align: 0.4em;
}

#jitjzyytwl .gt_asterisk {
  font-size: 100%;
  vertical-align: 0;
}

#jitjzyytwl .gt_slash_mark {
  font-size: 0.7em;
  line-height: 0.7em;
  vertical-align: 0.15em;
}

#jitjzyytwl .gt_fraction_numerator {
  font-size: 0.6em;
  line-height: 0.6em;
  vertical-align: 0.45em;
}

#jitjzyytwl .gt_fraction_denominator {
  font-size: 0.6em;
  line-height: 0.6em;
  vertical-align: -0.05em;
}
</style>
<table class="gt_table">
  <caption>(#tab:unnamed-chunk-69)10 Grösste Investitionen in fossile Brennstoffe: ZKB , Q2/2021</caption>
  
  <thead class="gt_col_headings">
    <tr>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1">Mio. USD</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_left" rowspan="1" colspan="1">Unternehmen</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_left" rowspan="1" colspan="1">GICS Subindustrie</th>
    </tr>
  </thead>
  <tbody class="gt_table_body">
    <tr><td class="gt_row gt_right">100.972</td>
<td class="gt_row gt_left">Exxon Mobil Corp</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">59.841</td>
<td class="gt_row gt_left">Chevron Corp</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">32.914</td>
<td class="gt_row gt_left">Enbridge Inc</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">24.017</td>
<td class="gt_row gt_left">Conocophillips</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">20.239</td>
<td class="gt_row gt_left">Kinder Morgan Inc</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">19.768</td>
<td class="gt_row gt_left">Suncor Energy Inc</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">17.220</td>
<td class="gt_row gt_left">Hess Corp</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">12.716</td>
<td class="gt_row gt_left">EOG Resources Inc</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">11.476</td>
<td class="gt_row gt_left">Pioneer Natural Resources Co</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
    <tr><td class="gt_row gt_right">10.526</td>
<td class="gt_row gt_left">Devon Energy Corp</td>
<td class="gt_row gt_left">Erdöl, Erdgas u. nicht erneuerbare Brennstoffe</td></tr>
  </tbody>
  
  
</table>
</div>
```

<!-- ## Investitionen nach durchschnittlichem CO2-Ausstoss -->

<!-- ### ungewichtet -->

<!-- ```{r} -->

<!-- filings %>%  -->

<!--    group_by(bank_name) %>% -->

<!--    summarize(mean_co2 = mean(estimated_co2_equivalents_emission_total, na.rm = TRUE)) %>% -->

<!--    arrange(desc(mean_co2)) -->

<!-- ``` -->

<!-- #### gewichtet nach Investitionsgrösse -->

<!-- -   noch überlegen, wie am besten ausrechnen -->

<!-- ```{r} -->

<!-- filings %>%  -->

<!--    group_by(bank_name) %>% -->

<!--    summarize(mean_co2 = mean((estimated_co2_equivalents_emission_total * value_usd_thousands), na.rm = TRUE)) %>% -->

<!--    arrange(desc(mean_co2)) -->

<!-- ``` -->

<!-- ## Top 20 Investments aus «braunen» Industrien nach Bank -->

<!-- ```{r} -->

<!-- # Funktion definieren für Top 10 Output -->

<!-- portfolio_filter_and_top_ten <- function(bank_name_arg, dirty_industries_arg = TRUE) { -->

<!--   filings %>% -->

<!--     filter(bank_name == bank_name_arg) %>% -->

<!--     filter( -->

<!--       if (dirty_industries_arg) { -->

<!--         gics_industry_name %in% dirty_industries -->

<!--       } else TRUE -->

<!--     ) %>% -->

<!--       slice_max(order_by = value_usd_thousands, n = 20) %>% -->

<!--       transmute( -->

<!--          value_millions = value_usd_thousands/1e3,  -->

<!--          company_common_name, gics_industry_name, -->

<!--          asset_category_description -->

<!--          ) %>% -->

<!--       DT::datatable( -->

<!--          colnames = c( -->

<!--             "Wert in Millionen",  -->

<!--             "Unternehmen",  -->

<!--             "Industrie nach GICS",  -->

<!--             "Wertpapierkategorie")  -->

<!--          ) -->

<!-- } -->

<!-- ``` -->

<!-- ```{r, include=FALSE} -->

<!-- generate_chunk <- function(bank_name) { -->

<!--   paste0( -->

<!--     "\n\n### ", bank_name, "\n\n", -->

<!--     "```{r}\n", -->

<!--     "portfolio_filter_and_top_ten(bank_name_arg = '", bank_name, "', dirty_industries_arg = TRUE)", "\n", -->

<!--     "```" -->

<!--   ) -->

<!-- } -->

<!-- unique(filings$bank_name) %>%  -->

<!--    purrr::map_chr(generate_chunk) %>%  -->

<!--    cat() -->

<!-- ``` -->

<!-- ### Banque Cantonale Vaudoise -->

<!-- ```{r} -->

<!-- portfolio_filter_and_top_ten(bank_name_arg = 'Banque Cantonale Vaudoise', dirty_industries_arg = TRUE) -->

<!-- ``` -->

<!-- ### Credit Suisse -->

<!-- ```{r} -->

<!-- portfolio_filter_and_top_ten(bank_name_arg = 'Credit Suisse', dirty_industries_arg = TRUE) -->

<!-- ``` -->

<!-- ### Edmond de Rothschild -->

<!-- ```{r} -->

<!-- portfolio_filter_and_top_ten(bank_name_arg = 'Edmond de Rothschild', dirty_industries_arg = TRUE) -->

<!-- ``` -->

<!-- ### Julius Bär -->

<!-- ```{r} -->

<!-- portfolio_filter_and_top_ten(bank_name_arg = 'Julius Bär', dirty_industries_arg = TRUE) -->

<!-- ``` -->

<!-- ### LGT -->

<!-- ```{r} -->

<!-- portfolio_filter_and_top_ten(bank_name_arg = 'LGT', dirty_industries_arg = TRUE) -->

<!-- ``` -->

<!-- -   LGT verzeichnet hier nur Werte, wenn Metals & Mining in den Industrien drin ist, sonst ist diese Tabelle leer -->

<!-- ### Lombard Odier -->

<!-- ```{r} -->

<!-- portfolio_filter_and_top_ten(bank_name_arg = 'Lombard Odier', dirty_industries_arg = TRUE) -->

<!-- ``` -->

<!-- ### Pictet -->

<!-- ```{r} -->

<!-- portfolio_filter_and_top_ten(bank_name_arg = 'Pictet', dirty_industries_arg = TRUE) -->

<!-- ``` -->

<!-- ### UBS -->

<!-- ```{r} -->

<!-- portfolio_filter_and_top_ten(bank_name_arg = 'UBS', dirty_industries_arg = TRUE) -->

<!-- ``` -->

<!-- ### Union Bancaire Privee -->

<!-- ```{r} -->

<!-- portfolio_filter_and_top_ten(bank_name_arg = 'Union Bancaire Privee', dirty_industries_arg = TRUE) -->

<!-- ``` -->

<!-- ### Vontobel -->

<!-- ```{r} -->

<!-- portfolio_filter_and_top_ten(bank_name_arg = 'Vontobel', dirty_industries_arg = TRUE) -->

<!-- ``` -->

<!-- ### ZKB -->

<!-- ```{r} -->

<!-- portfolio_filter_and_top_ten(bank_name_arg = 'ZKB', dirty_industries_arg = TRUE) -->

<!-- ``` -->
