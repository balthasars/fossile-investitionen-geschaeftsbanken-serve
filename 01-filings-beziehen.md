---
output:
  html_document:
     df_print: paged
  pdf_document: default
editor_options:
  chunk_output_type: inline
site: bookdown::bookdown_site
---



# Bezug 13F-Filings


```r
# devtools::install_github("hadley/emo")
# devtools::install_github("balthasars/tidysec")
library(dplyr)
library(tidysec)
library(ggplot2)
theme_set(theme_minimal())
library(tidyverse)
```

Central Index Keys der zu analysierenden Geschäftsbanken einlesen:


```r
banken_nach_cik <- vroom::vroom(here::here("data", "bank_ciks.csv")) %>% 
   # remove leading zeroes in CIK
   mutate(cik = stringr::str_remove_all(cik, "^(0)+"))
ciks_vec <- banken_nach_cik %>% pull(cik)
```

Dann die 13F-Filings mit dem [`{tidysec}` Package](https://github.com/balthasars/tidysec) herunterladen — 
Zeitreihe für alle Filings zwischen 2013 und 2021:


```r
if(!file.exists(here::here("raw", "13f", "13f_filings_2014_to_2021.rds"))){
  
alle_raw_ts <- purrr::map_df(c(2014:2021), get_13f, cik = ciks_vec, amendments = TRUE)
alle_raw_ts %>%
  unnest(filing) %>%
  # schönere Spaltennamen
  janitor::clean_names() %>%
  # speichern
  readr::write_rds(file = here::here("raw", "13F", "13f_filings_2014_to_2021.rds"))
}
```
