---
title: "Identifizieren der Finanzinstitute"
author: "Balthasar Sager"
date: "1/23/2022"
output: html_document
---



# Identifizieren der Filings der Banken und ihrer Tochterfirmen

Ausgangslage: 13F-Filings der folgenden in der Schweiz domizilierten Banken:

- Banque Cantonale Vaudoise
- Credit Suisse
- Edmond de Rothschild
- Julius Bär
- LGT
- Lombard Odier
- Pictet
- UBS
- Union Bancaire Privée
- Vontobel
- ZKB

Das sind alle _Privatbanken_, die mehr als 100 Millionen US-Dollar an den US-Börsen für ihre Kundschaft verwalten.
Weitere Investmentfirmen (z.B. GAM, Bellevue Group etc.), sowie die SNB lassen wir vor.

## CIK identifizieren


```r
# funktionierende Version von`edgarWebR` installieren
# remotes::install_github("https://github.com/balthasars/edgarWebR")
library(dplyr)
```

```
## 
## Attaching package: 'dplyr'
```

```
## The following objects are masked from 'package:stats':
## 
##     filter, lag
```

```
## The following objects are masked from 'package:base':
## 
##     intersect, setdiff, setequal, union
```

```r
library(tidyr)
library(edgarWebR)
```


```r
bank_terms <- tibble::tribble(
   ~bank, ~search_terms,
   "BCV", "Banque Cantonale Vaudoise",
   "Credit Suisse", "Credit Suisse",
   "Edmond de Rotschild", c("Edmond de Rothschild"), 
   "Julius Baer", c("Julius Baer", "Bank Julius Baer"),
   "LGT", "LGT",
   "Lombard Odier", c("Lombard Odier","Compagnie Lombard Odier", "Compagnie Lombard"),
   "Pictet", c("Pictet", "Bank Pictet"),
   "UBS", c("UBS", "UBS AG"),
   "Union Bancaire Privée", c("UBP", "Union Bancaire Privee"),
   "Vontobel", "Vontobel",
   "ZKB", "Zurcher Kantonalbank"
   ) %>%
   unnest(search_terms)

library(edgarWebR)

edgar_agent <- Sys.getenv("EDGARWEBR_USER_AGENT",
unset = "edgarWebR edgarpackage@examplemail.com"
)
ua <- httr::user_agent(edgar_agent)
options(HTTPUserAgent=ua$options$useragent)
edgar_agent <- Sys.getenv("EDGARWEBR_USER_AGENT",
unset = "edgarWebR edgarpackage@examplemail.com"
)

ua <- httr::user_agent(edgar_agent)
options(HTTPUserAgent=ua$options$useragent)

bank_search_results <- bank_terms %>%
  mutate(
    results = purrr::map(
      search_terms,
      edgarWebR::company_search,
      type = "13F-HR"
    )
  ) %>% 
   unnest(results)
bank_search_results
```

```
## # A tibble: 55 × 24
##    bank              search_terms name  cik   fiscal_year_end company_href sic  
##    <chr>             <chr>        <chr> <chr> <chr>           <chr>        <chr>
##  1 BCV               Banque Cant… Banq… 0001… 1231            https://www… <NA> 
##  2 Credit Suisse     Credit Suis… CRED… 0001… 1231            <NA>         <NA> 
##  3 Credit Suisse     Credit Suis… CRED… 0000… <NA>            <NA>         <NA> 
##  4 Credit Suisse     Credit Suis… CRED… 0000… 1231            <NA>         <NA> 
##  5 Credit Suisse     Credit Suis… CRED… 0001… <NA>            <NA>         <NA> 
##  6 Credit Suisse     Credit Suis… CRED… 0000… 1231            <NA>         6211 
##  7 Edmond de Rotsch… Edmond de R… Edmo… 0001… 1231            <NA>         <NA> 
##  8 Edmond de Rotsch… Edmond de R… EDMO… 0001… 1231            <NA>         <NA> 
##  9 Edmond de Rotsch… Edmond de R… EDMO… 0001… 1231            <NA>         <NA> 
## 10 Edmond de Rotsch… Edmond de R… EDMO… 0001… 1231            <NA>         <NA> 
## # … with 45 more rows, and 17 more variables: sic_description <chr>,
## #   state_location <chr>, state_incorporation <chr>, mailing_city <chr>,
## #   mailing_state <chr>, mailing_zip <chr>, mailing_street <chr>,
## #   mailing_street2 <chr>, business_city <chr>, business_state <chr>,
## #   business_zip <chr>, business_street <chr>, business_street2 <chr>,
## #   business_phone <chr>, formerly <chr>, mailing_street1 <chr>,
## #   business_street1 <chr>
```
- Suchbegriffe mit «Rothschild» geben Falschergebnisse (nicht Edmond de Rothschild zugehörig)
   - [ROTHSCHILD INVESTMENT CORP /IL](https://www.rothschildinv.com/)
      - unabhängig
   - [Rothschild & Co](https://www.rothschildandco.com/en/)
      - [gehört nicht zu Edmond de Rothschild](https://www.wsj.com/articles/rothschilds-end-family-feud-over-use-of-name-in-banking-1530284793)
- Invesco gehört nicht zu LGT
- GAM und [Artio](https://www.businesswire.com/news/home/20130213006599/en/Artio-Global-Investors-Inc.-Enters-into-Agreement-to-be-Acquired-by-Aberdeen-Asset-Management-PLC) gehören nicht mehr zu Julius Bär


Falschergebnisse entfernen

```r
incorrect_entities <- c("INVESCO /NY/ ASSET MANAGEMENT INC", "GAM Holding AG", "ARTIO GLOBAL MANAGEMENT LLC")
banks_details <- bank_search_results %>% 
   filter(!name %in% incorrect_entities)
```


```r
vroom::vroom_write(banks_details, here::here("data", "bank_ciks.csv"))
```

